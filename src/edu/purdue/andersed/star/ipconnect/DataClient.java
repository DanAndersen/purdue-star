package edu.purdue.andersed.star.ipconnect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.commands.Command;
import edu.purdue.andersed.star.commands.CreateAnnotationCommand;
import edu.purdue.andersed.star.commands.DeleteAnnotationCommand;
import edu.purdue.andersed.star.commands.SendAnnotationsCommand;
import edu.purdue.andersed.star.commands.UpdateAnnotationCommand;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class DataClient {

	// used by the trainee system, establishes a connection to the mentor system and supports receiving from the mentor server
	
	private static final String TAG = "DataClient";
	
	private DataClientAsyncTask mDataClientAsyncTask;
		
	public DataClient() {
		Log.d(TAG, "constructing DataClient");
				
		mDataClientAsyncTask = new DataClientAsyncTask();
	}
	
	public void init() {
		Log.d(TAG, "initializing DataClient");
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// this is needed, or else doInBackground() doesn't get called
			mDataClientAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);	
		} else {
			mDataClientAsyncTask.execute();
		}
	}
	
	public void destroy() {
		Log.d(TAG, "destroying DataClient");
	}
	
	private class DataClientAsyncTask extends AsyncTask<Void, Void, Void> {

		private Socket mSocket;
		
		private boolean mRunning;
		
		@Override
	    protected void onPreExecute() {
			mRunning = true;
	    }
		
		@Override
		protected Void doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			
			String host = PeerConnection.getInstance().getMentorIP();
			int port = Pref.getInstance().getDataServerSocketPort();
			
			Log.d(TAG, "going to connect to " + host + ":" + port);
			try {
				mSocket = new Socket(host, port);
				
				InputStream inputStream = mSocket.getInputStream();
	            
	            InputStreamReader in = new InputStreamReader(inputStream);
	            
	            BufferedReader bf = new BufferedReader(in);
	            
				Log.d(TAG, "started socket");
				
				while (mRunning) {
					
		            StringBuilder incomingCommandStringBuilder = new StringBuilder();
		            
		            String inputLine = bf.readLine();
		            
		            if (inputLine != null) {
		            	Log.d(TAG, "received inputLine: " + inputLine);
		            	incomingCommandStringBuilder.append(inputLine);
		            	
		            	Log.d(TAG, "received incoming command string");
			            STARUtils.saveStringToFile(incomingCommandStringBuilder.toString(), "star_received_command_" + System.currentTimeMillis() + ".log");
			            
			            JSONObject incomingCommandJsonObject = new JSONObject(incomingCommandStringBuilder.toString());
			            
			            Log.d(TAG, "got incoming json object: " + incomingCommandJsonObject.toString());
			            
			            Command incomingCommand = Command.commandFromJSON(incomingCommandJsonObject);
			            	            
			            processCommand(incomingCommand);
		            }
		            
		            
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
	    protected void onPostExecute(Void result) {
	    }
		
	}
	
	private void processCommand(Command command) {
    	Log.d(TAG, "processCommand()");
    	
		if (command instanceof SendAnnotationsCommand) {
			SendAnnotationsCommand sendAnnotationsCommand = (SendAnnotationsCommand) command;
			
			Log.d(TAG, "got a JSON representation of the annotation state");
			
			try {
				Log.d(TAG, "updating annotation state with received JSON representation");
				AnnotationState.getInstance().updateFromJSONObject(sendAnnotationsCommand.getAnnotationStateObject());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (command instanceof CreateAnnotationCommand) {
			CreateAnnotationCommand createAnnotationCommand = (CreateAnnotationCommand) command;
			
			Log.d(TAG, "got a command to create a new annotation");
			
			try {
				Integer newAnnotationId = createAnnotationCommand.getId();
				AnnotationMemory newAnnotationMemory = AnnotationMemory.fromJSON(createAnnotationCommand.getAnnotationMemoryJSONObject());
				AnnotationState.getInstance().addAnnotationMemoryWithID(newAnnotationId, newAnnotationMemory);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (command instanceof UpdateAnnotationCommand) {
			UpdateAnnotationCommand updateAnnotationCommand = (UpdateAnnotationCommand) command;
			
			Log.d(TAG, "got a command to update an existing annotation");
			
			try {
				Integer updatedAnnotationId = updateAnnotationCommand.getId();
				AnnotationMemory updatedAnnotationMemory = AnnotationMemory.fromJSON(updateAnnotationCommand.getAnnotationMemoryJSONObject());
				AnnotationState.getInstance().updateAnnotationMemoryWithID(updatedAnnotationId, updatedAnnotationMemory);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (command instanceof DeleteAnnotationCommand) {
			DeleteAnnotationCommand deleteAnnotationCommand = (DeleteAnnotationCommand) command;
			
			Log.d(TAG, "got a command to delete an annotation");
			
			Integer annotationId = deleteAnnotationCommand.getId();
			AnnotationMemory removedAnnotationMemory = AnnotationState.getInstance().getAnnotations().remove(annotationId);
			
			if (removedAnnotationMemory != null) {
				Log.d(TAG, "removed annotation memory " + annotationId);
			} else {
				Log.e(TAG, "got request to remove annotation memory " + annotationId + ", but it wasn't present");
			}
			
		} else {
			Log.e(TAG, "unrecognized command type");
		}
	}
}
