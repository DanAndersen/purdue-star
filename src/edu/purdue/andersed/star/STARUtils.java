package edu.purdue.andersed.star;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.opengl.GLES30;
import android.util.Log;
import edu.purdue.andersed.star.settings.Pref;

public class STARUtils {
	
	private static final String TAG = "Utils";
	
	public static Mat initHomography() {
		return Mat.eye(3, 3, CvType.CV_8UC1);
	}
	
	public static int createProgramFromShaderSrc(String vertexShaderSrc,
	        String fragmentShaderSrc)
    {
        int vertShader = initShader(GLES30.GL_VERTEX_SHADER, vertexShaderSrc);
        int fragShader = initShader(GLES30.GL_FRAGMENT_SHADER,
            fragmentShaderSrc);
        
        if (vertShader == 0 || fragShader == 0)
            return 0;
        
        int program = GLES30.glCreateProgram();
        if (program != 0)
        {
            GLES30.glAttachShader(program, vertShader);
            checkGLError("glAttchShader(vert)");
            
            GLES30.glAttachShader(program, fragShader);
            checkGLError("glAttchShader(frag)");
            
            GLES30.glLinkProgram(program);
            int[] glStatusVar = { GLES30.GL_FALSE };
            GLES30.glGetProgramiv(program, GLES30.GL_LINK_STATUS, glStatusVar,
                0);
            if (glStatusVar[0] == GLES30.GL_FALSE)
            {
            	String errorMessage = "Could NOT link program : "
                        + GLES30.glGetProgramInfoLog(program); 
                Log.e(TAG, errorMessage);
                GLES30.glDeleteProgram(program);
                program = 0;
                throw new IllegalStateException("OpenGL error: " + errorMessage);
            }
        }
        
        return program;
    }
	
	public static void checkGLError(String op)
    {
		if (Pref.getInstance().isDebugMode()) {
			for (int error = GLES30.glGetError(); error != 0; error = GLES30
		            .glGetError()) {
		        	String errorMessage = "After operation " + op + " got glError 0x"
		                    + Integer.toHexString(error); 
		            Log.e(TAG, errorMessage);
		            throw new IllegalStateException("OpenGL error: " + errorMessage);
		        }
		}
    }
	
	static int initShader(int shaderType, String source)
    {
        int shader = GLES30.glCreateShader(shaderType);
        if (shader != 0)
        {
            GLES30.glShaderSource(shader, source);
            GLES30.glCompileShader(shader);
            
            int[] glStatusVar = { GLES30.GL_FALSE };
            GLES30.glGetShaderiv(shader, GLES30.GL_COMPILE_STATUS, glStatusVar,
                0);
            if (glStatusVar[0] == GLES30.GL_FALSE)
            {
            	String errorMessage = "Could NOT compile shader " + shaderType + " : "
                        + GLES30.glGetShaderInfoLog(shader);
                Log.e(TAG, errorMessage);
                GLES30.glDeleteShader(shader);
                shader = 0;
                throw new IllegalStateException("OpenGL error: " + errorMessage);
            }
            
        }
        
        return shader;
    }
	
	public static double radiansToDegrees(double radians) { 
		return radians * (180.0 / Math.PI);
	}
	
	public static double degreesToRadians(double degrees) {
		return degrees * (Math.PI / 180.0);
	}
	
	public static int getRandomColor() {
		Random r = new Random();
		int randomColor = Color.rgb(r.nextInt(255)+1, r.nextInt(255)+1, r.nextInt(255)+1);
		return randomColor;
	}
	
	public static void saveStringToFile(String string, String filename) {
		Log.d(TAG, "creating preview bitmap");
		
		File path = new File("sdcard/" + filename);
		
		try {
			FileOutputStream fos = new FileOutputStream(path);
			
			fos.write(string.getBytes(Charset.forName("UTF-8")));
			
			fos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void saveMatToFile(Mat image, String filename) {
		if (image.width() > 0 && image.height() > 0) {
			Log.d(TAG, "creating preview bitmap");
			Bitmap previewBitmap = Bitmap.createBitmap(image.width(), image.height(), Bitmap.Config.ARGB_8888);	
			Utils.matToBitmap(image, previewBitmap);
			
			File path = new File("sdcard/" + filename);
			
			try {
				FileOutputStream fos = new FileOutputStream(path);
				
				boolean result = previewBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
				
				Log.d(TAG, "result of saving frame to file? " + result);
				
				fos.close();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			previewBitmap.recycle();
			previewBitmap = null;
		} else {
			Log.e(TAG, "image mat has size 0");
		}
		
	}
	
	// based off https://github.com/roman10/android-ffmpeg-tutorial/blob/master/android-ffmpeg-tutorial02/src/roman10/tutorial/android_ffmpeg_tutorial02/Utils.java
	public static void copyAssets(Context context, String assetFilePath, String destDirPath) {
		AssetManager assetManager = context.getAssets();
		InputStream in = null;
		OutputStream out = null;
		try {
			in = assetManager.open(assetFilePath);
			File outFile = new File(destDirPath, assetFilePath);
			out = new FileOutputStream(outFile);
			copyFile(in, out);
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
		} catch (IOException e) {
			Log.e(TAG, "Failed to copy asset file: " + assetFilePath, e);
		}
	}
	private static void copyFile(InputStream in, OutputStream out) throws IOException {
	    byte[] buffer = new byte[1024*16];
	    int read;
	    while((read = in.read(buffer)) != -1){
	      out.write(buffer, 0, read);
	    }
	}
}
