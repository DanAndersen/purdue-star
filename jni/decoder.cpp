
// added to fix compilation error about UINT64_C not being defined (solution from https://github.com/AutonomyLab/ardrone_autonomy/issues/1 )
#ifndef UINT64_C
#define UINT64_C(c) (c ## ULL)
#endif

#ifdef __cplusplus
extern "C" {
#endif
// ffmpeg libraries are in C not C++, so this may help with proper linking
// https://stackoverflow.com/questions/15625468/libav-linking-error-undefined-references
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/pixfmt.h>
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
#ifdef __cplusplus
}
#endif



#include <pthread.h>

#include <jni.h>
#include <stdio.h>
#include <android/log.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <android/bitmap.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>



#include <GLES3/gl3.h>
#include <EGL/egl.h>


using namespace cv;

#define  LOG_TAG    "decoder"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGE(...)   __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define CHECK_MAT(cond) if(!(cond)){ LOGI("FAILED: " #cond); return; }

// https://ffmpeg.org/doxygen/trunk/doc_2examples_2decoding_encoding_8c-example.html

extern "C" {
	JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Decoder_nInitDecoder(JNIEnv * env, jobject obj, int width, int height);
	JNIEXPORT jboolean JNICALL Java_edu_purdue_andersed_streaming_Decoder_nDecode(JNIEnv * env, jobject obj, jbyteArray byteArrayToDecode, jlong matNativeObj);
	JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Decoder_nDestroyDecoder(JNIEnv * env, jobject obj);
};

//=============================================================================

#define INBUF_SIZE 4096

// decoding:
AVCodecContext *decoder_c = NULL;
AVFrame *decoder_frame;
AVPacket decoder_pkt;
SwsContext* decoder_sws;

int DECODER_WIDTH_PIXELS;
int DECODER_HEIGHT_PIXELS;


//=============================================================================

JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Decoder_nInitDecoder(JNIEnv * env, jobject obj, int width, int height) {
	LOGI("in nInit");

	DECODER_WIDTH_PIXELS = width;
	DECODER_HEIGHT_PIXELS = height;

	/* register all the codecs */
	avcodec_register_all();

	//enum AVCodecID codec_id = AV_CODEC_ID_MPEG4;
	enum AVCodecID codec_id = AV_CODEC_ID_MJPEG;

	LOGI("in initDecoder()");

	AVCodec *decoder_codec;


	uint8_t inbuf[INBUF_SIZE + FF_INPUT_BUFFER_PADDING_SIZE];

	av_init_packet(&decoder_pkt);

	/* set end of buffer to 0 (this ensures that no overreading happens for damaged mpeg streams) */
	memset(inbuf + INBUF_SIZE, 0, FF_INPUT_BUFFER_PADDING_SIZE);

	/* find the mpeg1 video decoder */
	decoder_codec = avcodec_find_decoder(codec_id);
	if (!decoder_codec) {
		LOGE("Codec not found\n");
		exit(1);
	}

	decoder_c = avcodec_alloc_context3(decoder_codec);
	if (!decoder_c) {
		LOGE("Could not allocate video codec context\n");
		exit(1);
	}

	if(decoder_codec->capabilities&CODEC_CAP_TRUNCATED) {
		decoder_c->flags|= CODEC_FLAG_TRUNCATED; /* we do not send complete frames */
	}

	/* For some codecs, such as msmpeg4 and mpeg4, width and height
		   MUST be initialized there because this information is not
		   available in the bitstream. */

	/* open it */
	if (avcodec_open2(decoder_c, decoder_codec, NULL) < 0) {
		LOGE("Could not open codec\n");
		exit(1);
	}

	// here is where we would open the file from a filename, but we aren't reading from a file

	decoder_frame = av_frame_alloc();
	if (!decoder_frame) {
		LOGE("Could not allocate video frame\n");
		exit(1);
	}

	decoder_sws = sws_getContext(DECODER_WIDTH_PIXELS, DECODER_HEIGHT_PIXELS, AV_PIX_FMT_YUVJ420P, DECODER_WIDTH_PIXELS, DECODER_HEIGHT_PIXELS, AV_PIX_FMT_RGB24, SWS_FAST_BILINEAR, 0, 0, 0);

	// here is where we would decode each frame

	LOGI("done initing decoder");
}

JNIEXPORT jboolean JNICALL Java_edu_purdue_andersed_streaming_Decoder_nDecode(JNIEnv * env, jobject obj, jbyteArray byteArrayToDecode, jlong matNativeObj) {

	jboolean returnValue = false;

	jbyte* bytesToDecode = env->GetByteArrayElements(byteArrayToDecode, NULL);
	jsize lengthOfArray = env->GetArrayLength(byteArrayToDecode);

	if (lengthOfArray > 0) {

		decoder_pkt.size = lengthOfArray;
		decoder_pkt.data = (uint8_t*) bytesToDecode;

		int len;
		int got_frame;

		len = avcodec_decode_video2(decoder_c, decoder_frame, &got_frame, &decoder_pkt);

		if (len < 0) {
			LOGE("error while decoding frame");
			returnValue = false;
		}

		if (got_frame) {

			cv::Mat* decodedMat = (cv::Mat*)matNativeObj;

			uint8_t* rgb24Data = decodedMat->data;
			uint8_t * outData[1] = { rgb24Data };	// rgb24 has one plane
			int outLinesize[1] = { 3*DECODER_WIDTH_PIXELS };	// rgb stride

			sws_scale(decoder_sws, decoder_frame->data, decoder_frame->linesize, 0, DECODER_HEIGHT_PIXELS, outData, outLinesize);

			returnValue = true;

		}

	} else {
		LOGI("bytes to decode were empty, doing nothing");
	}

	env->ReleaseByteArrayElements(byteArrayToDecode, bytesToDecode, 0);

	return returnValue;
}

JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Decoder_nDestroyDecoder(JNIEnv * env, jobject obj) {
	LOGI("destroyDecoder");

	avcodec_close(decoder_c);
	av_free(decoder_c);
	av_frame_free(&decoder_frame);
	printf("\n");
}
