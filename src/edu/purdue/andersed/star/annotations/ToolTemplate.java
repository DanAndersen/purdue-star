package edu.purdue.andersed.star.annotations;

public class ToolTemplate {

	private int mDefaultDrawableResourceId;
	private int mSelectedDrawableResourceId;
	
	// how far right/down from the image center the anchor point is
	private int mAnchorPixelsFromCenterX;
	private int mAnchorPixelsFromCenterY;
	
	public ToolTemplate(int defaultDrawableResourceId, int selectedDrawableResourceId, int imageWidth, int imageHeight, int anchorX, int anchorY) {
		this.mDefaultDrawableResourceId = defaultDrawableResourceId;
		this.mSelectedDrawableResourceId = selectedDrawableResourceId;
		
		this.mAnchorPixelsFromCenterX = anchorX - (imageWidth/2);
		this.mAnchorPixelsFromCenterY = anchorY - (imageHeight/2);
	}

	public int getDefaultDrawableResourceId() {
		return mDefaultDrawableResourceId;
	}

	public int getSelectedDrawableResourceId() {
		return mSelectedDrawableResourceId;
	}
	
	public int getAnchorPixelsFromCenterX() {
		return mAnchorPixelsFromCenterX;
	}

	public int getAnchorPixelsFromCenterY() {
		return mAnchorPixelsFromCenterY;
	}
	
	
	
	
}
