package edu.purdue.andersed.star;

public class EngineerViewData {

	private int mFPS;
	
	private int mMillisSinceLastFrame;
	
	private int mNumKeyPoints;
	
	public String getText() {
		return "FPS:\t" + mFPS + "\n" +
				"\t(" + mMillisSinceLastFrame + " ms since last frame) \n" +
				"Keypoints:\t" + mNumKeyPoints + "\n";
	}

	public int getFPS() {
		return mFPS;
	}

	public void setFPS(int fps) {
		this.mFPS = fps;
	}

	public int getMillisSinceLastFrame() {
		return mMillisSinceLastFrame;
	}

	public void setMillisSinceLastFrame(int millisSinceLastFrame) {
		this.mMillisSinceLastFrame = millisSinceLastFrame;
	}

	public int getNumKeyPoints() {
		return mNumKeyPoints;
	}

	public void setNumKeyPoints(int numKeyPoints) {
		this.mNumKeyPoints = numKeyPoints;
	}
	
	
}
