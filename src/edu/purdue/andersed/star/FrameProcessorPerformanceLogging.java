package edu.purdue.andersed.star;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.util.Log;

public class FrameProcessorPerformanceLogging {

	private static final String TAG = "FrameProcessorPerformanceLogging";
	private static FrameProcessorPerformanceLogging instance;
	
	private int mFrameNumber;
	private long mTimestamp;
	private File mLogFile;
	private BufferedWriter mBufferedWriter;
	
	private boolean mHasWrittenHeader;
	
	public static FrameProcessorPerformanceLogging getInstance() {
		if (instance == null) {
			instance = new FrameProcessorPerformanceLogging();
			try {
				instance.initLogging();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toaster.getInstance().toast("couldn't set up frame processor performance logging");
			}
		}
		
		return instance;
	}

	
	
	public void initLogging() throws IOException {
		Log.d(TAG, "initLogging()");
		
		mFrameNumber = 0;
		
		mHasWrittenHeader = false;
		
		mTimestamp = System.currentTimeMillis();
		
		String path = "sdcard/STAR_processing_performance_log_" + mTimestamp + ".log";
		
		Log.d(TAG, "going to create log file at " + path);
		
		mLogFile = new File(path);
		if (!mLogFile.exists()) {
			mLogFile.createNewFile();
		}
		
		Log.d(TAG, "setting up bufferedwriter for log");
		mBufferedWriter = new BufferedWriter(new FileWriter(mLogFile, true));	
	}
	
	public void writeLogForFrame(FrameTracing frameTracing) {
		if (!mHasWrittenHeader) {
			writeString("Frame number\tTime (ms)\t" + frameTracing.getLogHeader());
			mHasWrittenHeader = true;
		}
		
		mFrameNumber++;
		
		long currentTimestamp = System.currentTimeMillis();
					
		String logLine = mFrameNumber + "\t" + currentTimestamp + "\t" + frameTracing.getLogLine();
		
		writeString(logLine);
	}
	
	private void writeString(String s) {
		try {		
			mBufferedWriter.append(s);
			mBufferedWriter.newLine();
			mBufferedWriter.flush();
			
		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeLog() {
		Log.d(TAG, "closeLog");
		
		if (mBufferedWriter != null) {
			try {
				mBufferedWriter.flush();
				mBufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		mLogFile = null;
	}
}
