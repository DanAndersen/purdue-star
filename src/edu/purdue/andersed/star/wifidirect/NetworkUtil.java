package edu.purdue.andersed.star.wifidirect;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.UserType;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.commands.CreateAnnotationCommand;
import edu.purdue.andersed.star.commands.DeleteAnnotationCommand;
import edu.purdue.andersed.star.commands.SendAnnotationsCommand;
import edu.purdue.andersed.star.commands.UpdateAnnotationCommand;
import edu.purdue.andersed.star.ipconnect.DataServer;
import edu.purdue.andersed.star.settings.Pref;

public class NetworkUtil {

	private static final String TAG = "NetworkUtil";
	
	public static void sendDeleteAnnotationCommand(MainActivity delegate, Integer annotationId) {
		Log.d(TAG, "sendDeleteAnnotationCommand()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {
			DeleteAnnotationCommand deleteAnnotationCommand = new DeleteAnnotationCommand(annotationId);
			
			dataServer.requestSendCommand(deleteAnnotationCommand);
		}
	}
	
	public static void sendCreatedAnnotation(MainActivity delegate, Integer newAnnotationId, AnnotationMemory newAnnotationMemory) {
		Log.d(TAG, "sendCreatedAnnotation()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {
			try {
				CreateAnnotationCommand createAnnotationCommand = new CreateAnnotationCommand(newAnnotationId, newAnnotationMemory.toJSON());
				
				dataServer.requestSendCommand(createAnnotationCommand);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void sendUpdatedAnnotation(MainActivity delegate, Integer updatedAnnotationId,
			AnnotationMemory updatedAnnotationMemory) {
		Log.d(TAG, "sendUpdatedAnnotation()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {

			try {
				UpdateAnnotationCommand updateAnnotationCommand = new UpdateAnnotationCommand(updatedAnnotationId, updatedAnnotationMemory.toJSON());
				
				dataServer.requestSendCommand(updateAnnotationCommand);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void sendAnnotations(MainActivity delegate) {
		Log.d(TAG, "sendAnnotations()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {

			try {				
				JSONObject annotationStateObject = AnnotationState.getInstance().toJSON();
								
				SendAnnotationsCommand sendAnnotationsCommand = new SendAnnotationsCommand(annotationStateObject);
				
				dataServer.requestSendCommand(sendAnnotationsCommand);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	
}
