package edu.purdue.andersed.star;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

public class PreviousStepsState {

	private static final String TAG = "PreviousStepsState";
	
	private static PreviousStepsState instance;
	
	private long mFrameCaptureRequestMillis = -1;
	
	private List<Bitmap> mBitmaps;
	
	private Button mPrevButton;
	private Button mNextButton;
	private ImageView mImageView;
	
	private int mCurrentImageIndex = 0;

	private MainActivity mDelegate;
	
	public static PreviousStepsState getInstance() {
		if (instance == null) {
			instance = new PreviousStepsState();
		}
		
		return instance;
	}
	
	private PreviousStepsState() {
		mFrameCaptureRequestMillis = -1;
		
		mBitmaps = new ArrayList<Bitmap>();
	}
	
	public void registerUI(MainActivity delegate, Button prevButton, Button nextButton, ImageView imageView) {
		
		Log.d(TAG, "registerUI");
		
		mDelegate = delegate;
		
		mPrevButton = prevButton;
		mPrevButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCurrentImageIndex--;
				mCurrentImageIndex = Math.min(mCurrentImageIndex, mBitmaps.size()-1);
				mCurrentImageIndex = Math.max(mCurrentImageIndex, 0);
				
				updateImageView();
			}
		});
		mPrevButton.setEnabled(false);
		
		
		mNextButton = nextButton;
		mNextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				mCurrentImageIndex++;
				mCurrentImageIndex = Math.min(mCurrentImageIndex, mBitmaps.size()-1);
				mCurrentImageIndex = Math.max(mCurrentImageIndex, 0);
				
				updateImageView();
			}
		});
		
		mNextButton.setEnabled(false);
		
		
		mImageView = imageView;
	}
	
	private void updateImageView() {
		
		Log.d(TAG, "updateImageView, num bitmaps: " + mBitmaps.size() + ", mCurrentImageIndex: " + mCurrentImageIndex);
		
		if (mBitmaps.size() > 0) {
			mImageView.setImageBitmap(mBitmaps.get(mCurrentImageIndex));
		}
		
		if (mBitmaps.size() > 0 && mCurrentImageIndex - 1 >= 0) {
			mPrevButton.setEnabled(true);
		} else {
			mPrevButton.setEnabled(false);
		}
		
		if (mBitmaps.size() > 0 && mCurrentImageIndex + 1 <= mBitmaps.size()-1) {
			mNextButton.setEnabled(true);
		} else {
			mNextButton.setEnabled(false);
		}
	}
	
	public void requestFrameCapture() {
		
		Log.d(TAG, "requestFrameCapture");
		
		mFrameCaptureRequestMillis = System.currentTimeMillis();	
	}
	
	public boolean shouldCaptureNextFrame() {
		return (mFrameCaptureRequestMillis > 0 && (System.currentTimeMillis() - mFrameCaptureRequestMillis) > 10);
	}
	
	public void addBitmap(Bitmap b) {
		
		Log.d(TAG, "addBitmap");
		
		mFrameCaptureRequestMillis = -1;
		
		mBitmaps.add(b);
		Log.d(TAG, "num bitmaps: " + mBitmaps.size());
		
		
		mDelegate.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				mCurrentImageIndex = mBitmaps.size() - 1;
				updateImageView();
			}
		});
		
		
		
		
	}
}
