package edu.purdue.andersed.star;

public enum UserType {
	MENTOR("MENTOR"),
	TRAINEE("TRAINEE");
	
	private String mLabel;

	private UserType(String label) {
		mLabel = label;
	}
	public boolean isMentor() {
		return UserType.MENTOR.equals(this);
	}
	
	public boolean isTrainee() {
		return UserType.TRAINEE.equals(this);
	}
	
	public String getLabel() {
		return mLabel;
	}
	
	public static UserType fromLabel(String label) {
	    if (label != null) {
	      for (UserType b : UserType.values()) {
	        if (label.equalsIgnoreCase(b.mLabel)) {
	          return b;
	        }
	      }
	    }
	    return null;
	  }
}
