package edu.purdue.andersed.star;

public enum FrameTracingTag {
	total,
	feature_detection,
	num_features_after_feature_detection,
	descriptor_extraction,
	num_features_after_descriptor_extraction,
	descriptor_matching,
	prepping_list_of_matches,
	num_matches,
	find_homography,
	num_inliers,
	num_outliers
}
