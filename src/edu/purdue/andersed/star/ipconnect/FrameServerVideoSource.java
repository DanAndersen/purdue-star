package edu.purdue.andersed.star.ipconnect;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.videosource.VideoSource;
import edu.purdue.andersed.streaming.DecodeListener;
import edu.purdue.andersed.streaming.Decoder;
import edu.purdue.andersed.streaming.Encoder;

public class FrameServerVideoSource extends VideoSource implements DecodeListener {

	// used by the mentor system, receives an incoming connection from a trainee system and supports receiving frames from the trainee client
	
	private static final String TAG = "FrameServerVideoSource";
	
	private FrameServerAsyncTask mFrameServerAsyncTask;
	private MainActivity mDelegate;
	
	Bitmap mReceivedBitmap = null;
	
	public FrameServerVideoSource(MainActivity delegate) {
		Log.d(TAG, "constructing FrameServer");
		
		mDelegate = delegate;
		
		mFrameServerAsyncTask = new FrameServerAsyncTask();
	}
	
	@Override
	public void initAndStartVideoSource(SurfaceTexture surfaceTexture) {
		Log.d(TAG, "initializing FrameServer");
		
		if (Pref.getInstance().isEncodingDecodingFrames()) {
			int decodeWidth = ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor();
			int decodeHeight = ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor();
			Decoder.getInstance().init(this, decodeWidth, decodeHeight);	
		}
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// this is needed, or else doInBackground() doesn't get called
			mFrameServerAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);	
		} else {
			mFrameServerAsyncTask.execute();
		}
	}

	@Override
	public void stopVideoSource() {
		Log.d(TAG, "destroying FrameServer");
	}
	
	private class FrameServerAsyncTask extends AsyncTask<Void, Void, Void> {

		private ServerSocket mServerSocket;
		
		private boolean mRunning;
		
		@Override
	    protected void onPreExecute() {
			mRunning = true;
	    }
		
		@Override
		protected Void doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			
			
			Mat receivedMat = new Mat(ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), PointOverlay.OPENCV_MAT_TYPE);
			Log.d(TAG, "set up mat for incoming frames, size is " + receivedMat.cols() + " x " + receivedMat.rows());

			
			int port = Pref.getInstance().getVideoServerSocketPort();
			Log.d(TAG, "starting server on port " + port);
			try {
				mServerSocket = new ServerSocket(port);
				Log.d(TAG, "started server socket, waiting for connection...");
				
				while (mRunning) {
					Log.d(TAG, "waiting for connection...");
					Socket clientSocket = mServerSocket.accept();
					Log.d(TAG, "got client connection");
					
					InputStream inputStream = clientSocket.getInputStream();
					
					while (clientSocket.isConnected()) {
						
						//Log.d(TAG, "reading in length bytes...");
						byte[] lengthBytes = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder()).array();
						boolean lengthBytesHaveBeenRead = false;
						int numBytesReadSoFar = 0;
						while (!lengthBytesHaveBeenRead) {
							int numBytesRead = inputStream.read(lengthBytes, numBytesReadSoFar, lengthBytes.length - numBytesReadSoFar);
							//Log.d(TAG, "numBytesRead: " + numBytesRead);
							
							if (numBytesRead >= 0) {
								numBytesReadSoFar += numBytesRead;
							} else {
								lengthBytesHaveBeenRead = true;
							}
							
							//Log.d(TAG, "numBytesReadSoFar: " + numBytesReadSoFar);
							if (numBytesReadSoFar >= lengthBytes.length) {
								lengthBytesHaveBeenRead = true;
							}
						}
						//Log.d(TAG, "done reading all the length bytes");
						
						int numBytesInPayload = ByteBuffer.wrap(lengthBytes).order(ByteOrder.nativeOrder()).asIntBuffer().get();
						//Log.d(TAG, "numBytesInPayload: " + numBytesInPayload);
						
						
						byte buff[] = new byte[numBytesInPayload];
						
						
						
						
						
						
						
						int currentByteOffset = 0;
						int bytesReadInLastCall = 0;
						do {
							//Log.d(TAG, "reading...");
							bytesReadInLastCall = inputStream.read(buff, currentByteOffset, buff.length - currentByteOffset);
							if (bytesReadInLastCall >= 0) {
								currentByteOffset += bytesReadInLastCall;
							}
							//Log.d(TAG, "bytesReadInLastCall: " + bytesReadInLastCall);
							//Log.d(TAG, "currentByteOffset: " + currentByteOffset);
						}
						while (bytesReadInLastCall != -1 && currentByteOffset < buff.length);
						
						if (Pref.getInstance().isEncodingDecodingFrames()) {
							//Log.d(TAG, "got encoded bytes, size: " + currentByteOffset);
							byte[] encodedBytes = new byte[currentByteOffset];
							System.arraycopy(buff, 0, encodedBytes, 0, currentByteOffset);
							Decoder.getInstance().addPacketsToDecode(encodedBytes);
						} else {
							//Log.d(TAG, "putting data into mat");
							receivedMat.put(0, 0, buff);
							
							onReceivedFrame(receivedMat);
						}
						
						
					}
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
	    protected void onPostExecute(Void result) {
	    }
		
		
	}
	
	public void onReceivedFrame(Mat receivedMat) {
		if (mReceivedBitmap == null || mReceivedBitmap.getWidth() != receivedMat.width() || mReceivedBitmap.getHeight() != receivedMat.height()) {
			Log.d(TAG, "creating received bitmap");
			mReceivedBitmap = Bitmap.createBitmap(receivedMat.width(), receivedMat.height(), Bitmap.Config.ARGB_8888);	
		}
		
		Utils.matToBitmap(receivedMat, mReceivedBitmap);
		
		mDelegate.onNewFrameBitmapFromSocket(mReceivedBitmap);
		
		// should have gotten texture loaded, tell system to rerender
		
		mDelegate.onFrameAvailable(null);
	}

	@Override
	public void onReceivedDecodedFrame(Mat decodedRGBFrame) {
		onReceivedFrame(decodedRGBFrame);
	}
}
