package edu.purdue.andersed.star.annotations;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import android.util.Log;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class AnimatedIncisionAnnotation extends PolylineAnnotation {

	private static final String TAG = "AnimatedIncisionAnnotation";
	
	private static final int ANIMATION_MILLIS = 3000;
	
	private ToolAnnotation mIllustrationScalpel;
	
	@Override
	String getAnnotationType() {
		return Annotation.TYPE_ANIMATED_INCISION;
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		
		double elapsedMillis = System.currentTimeMillis() % ANIMATION_MILLIS;
		double animationProgress = elapsedMillis / ANIMATION_MILLIS; // from 0.0 to 1.0
		
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		int endPointIndex = (int) Math.round(pointsInScreenSpace.size() * animationProgress);
		
		for (int i = 0; i < endPointIndex; i++) {
			
			if (i < endPointIndex - 1) {
				pointOverlay.drawLineWithColorAndThickness(pointsInScreenSpace.get(i), pointsInScreenSpace.get(i+1), color, thickness);
			} else {
				// last point
				
				if (endPointIndex > 1) {
					// need at least 2 points to get tangent line to be able to draw the scalpel
					mIllustrationScalpel.setPointInScreenSpace(pointsInScreenSpace.get(i));
					
					Point p1 = pointsInScreenSpace.get(i-1);
					Point p2 = pointsInScreenSpace.get(i);
					
					double radians = Math.atan2(p2.y-p1.y, p2.x-p1.x);
					
					double degrees = STARUtils.radiansToDegrees(radians);
					
					mIllustrationScalpel.setDegrees(degrees);
					
					pointOverlay.drawTool(mIllustrationScalpel);
				}
				
				
			}
		}
	}
	
	@Override
	public Annotation getTransformedAnnotationBasedOnHomography(Mat homography) {
		
		MatOfPoint2f transformedPointMat = new MatOfPoint2f();
		
		// transform is only valid for its own scale (in opencv coords not screen coords) -- scaling the point while working with it
		Core.perspectiveTransform(mPointMat, transformedPointMat, homography);	
		
		return new AnimatedIncisionAnnotation(transformedPointMat);
	}
	
	public AnimatedIncisionAnnotation(MatOfPoint2f pointMat) {
		super(pointMat);
		initScalpel();
	}
	
	public AnimatedIncisionAnnotation() {
		super();
		initScalpel();
	}
	
	private void initScalpel() {
		mIllustrationScalpel = new ToolAnnotation(new Point(0,0), ToolType.SCALPEL, 45, 0.3, STARUtils.getRandomColor());
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		AnimatedIncisionAnnotation polylineAnnotation = new AnimatedIncisionAnnotation(pointMat);
		
		return polylineAnnotation;
	}
}
