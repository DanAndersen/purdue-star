package edu.purdue.andersed.star.annotations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import edu.purdue.andersed.star.Toaster;
import android.util.Log;

public class FullAnnotationSave {

	private static final String TAG = "FullAnnotationSave";
			
	public static void saveAnnotationStateToFile() {
		String path = "sdcard/STAR_saved_annotations_" + System.currentTimeMillis() + ".log";
		
		Log.d(TAG, "going to create save file at " + path);

		try {
			File saveFile = new File(path);
			if (!saveFile.exists()) {
				saveFile.createNewFile();
			}
			
			Log.d(TAG, "setting up bufferedwriter for save");
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(saveFile, true));	
			
			JSONObject currentFullAnnotationState = AnnotationState.getInstance().toJSON();
			
			bufferedWriter.append(currentFullAnnotationState.toString());
			bufferedWriter.newLine();
			bufferedWriter.flush();
			
			bufferedWriter.close();
			
			Toaster.getInstance().toast("annotations saved! to " + path);
			
			bufferedWriter = null;
			
			saveFile = null;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
