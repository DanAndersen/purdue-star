package edu.purdue.andersed.star;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Iterator;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import org.opencv.core.Point;

import android.graphics.Color;
import android.opengl.GLES30;
import android.util.Log;
import android.util.SparseArray;
import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.annotations.ToolAnnotation;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.shaders.SelectableToolShaderProgram;

// The purpose of this class is to go through each annotation that should be 
// "selectable" (i.e. if the user can touch to select it) and draw it on a 
// separate FBO (with a unique solid color), to then be able to do 
// glReadPixels() on it (for a particular X/Y location) and see what the unique
// color is there, and thus know which annotation it is that was selected 
public class SelectableOverlay {

	private static final String TAG = "SelectableOverlay";
	
	private MainActivity mDelegate;

	private SelectableToolShaderProgram mSelectableToolShaderProgram;

	private IntBuffer mTextureBuffer;

	private ByteBuffer mSelectPixelBuffer;
	
	private int mFBO;
	private int mFBODepth;
	private int mFBOTexture;

	private int mSelectionRegionSize = 1;
	
	private Point mRequestedSelectionPoint;
	
	private SparseArray<Integer> mSparseArrayOfSelectableColorsToIDs;
	
	public SelectableOverlay(MainActivity delegate) {
		Log.d(TAG, "setting up SelectableOverlay");
		mDelegate = delegate;
		
		mRequestedSelectionPoint = null;
				
		mSelectableToolShaderProgram = new SelectableToolShaderProgram(mDelegate);
		
		int width = ScreenState.getInstance().getScreenWidth();
		int height = ScreenState.getInstance().getScreenHeight();
		
		// setting up framebuffer
		// http://blog.shayanjaved.com/2011/05/13/android-opengl-es-2-0-render-to-texture/
		init(width, height);
		
		mSelectionRegionSize = Pref.getInstance().getSelectionRegionSize();
		
		mSelectPixelBuffer = ByteBuffer.allocateDirect(mSelectionRegionSize * mSelectionRegionSize * Constants.BYTES_PER_PIXEL).order(ByteOrder.nativeOrder());
	}
	
	private void init(int width, int height) {		
		initFrameBuffer(width, height);
	}

	private void initFrameBuffer(int width, int height) {
		
		int[] framebufferHandles = new int[1];
		GLES30.glGenFramebuffers(1, framebufferHandles, 0);
		STARUtils.checkGLError("glGenFramebuffers");
		mFBO = framebufferHandles[0];
		Log.d(TAG, "set mFBO to " + mFBO);
		
		// bind the framebuffer
		GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, mFBO);
		STARUtils.checkGLError("glBindFramebuffer");
		
		initFrameBufferDepthBuffer(width, height);
		
		initFrameBufferTexture(width, height);
		
		// specify texture as color attachment
		GLES30.glFramebufferTexture2D(GLES30.GL_FRAMEBUFFER, GLES30.GL_COLOR_ATTACHMENT0, GLES30.GL_TEXTURE_2D, mFBOTexture, 0);
		STARUtils.checkGLError("glFramebufferTexture2D"); 
		
		// attach render buffer as depth buffer
		GLES30.glFramebufferRenderbuffer(GLES30.GL_FRAMEBUFFER, GLES30.GL_DEPTH_ATTACHMENT, GLES30.GL_RENDERBUFFER, mFBODepth);
		STARUtils.checkGLError("glFramebufferRenderbuffer"); 
		
		// check status
		int status = GLES30.glCheckFramebufferStatus(GLES30.GL_FRAMEBUFFER);
		STARUtils.checkGLError("glCheckFramebufferStatus");
		if (status != GLES30.GL_FRAMEBUFFER_COMPLETE) {
		    throw new IllegalStateException("framebuffer has bad status: " + status);
		}
		
		GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, 0);	// unbind framebuffer
		STARUtils.checkGLError("glBindFramebuffer");
	}
	
	private void initFrameBufferTexture(int width, int height) {
		int[] renderTextureHandles = new int[1];
		GLES30.glGenTextures(1, renderTextureHandles, 0);
		STARUtils.checkGLError("glGenTextures");
		mFBOTexture = renderTextureHandles[0];
		Log.d(TAG, "set mFBOTexture to " + mFBOTexture);
		
		// generate texture
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, mFBOTexture);
		STARUtils.checkGLError("glBindTexture");
		
		Log.d(TAG, "allocating buffer");
		mTextureBuffer = ByteBuffer.allocateDirect(width * height * Constants.BYTES_PER_PIXEL).order(ByteOrder.nativeOrder()).asIntBuffer();
		
		Log.d(TAG, "generating the texture");
		GLES30.glTexImage2D(GLES30.GL_TEXTURE_2D, 0, GLES30.GL_RGBA, width, height, 0, GLES30.GL_RGBA, GLES30.GL_UNSIGNED_BYTE, mTextureBuffer);
		STARUtils.checkGLError("glTexImage2D");
		
		// parameters - we have to make sure we clamp the textures to the edges
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_WRAP_S,
				GLES30.GL_CLAMP_TO_EDGE);
		STARUtils.checkGLError("glTexParameteri");
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_WRAP_T,
				GLES30.GL_CLAMP_TO_EDGE);
		STARUtils.checkGLError("glTexParameteri");
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER,
				GLES30.GL_LINEAR);
		STARUtils.checkGLError("glTexParameteri");
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER,
				GLES30.GL_LINEAR);
		STARUtils.checkGLError("glTexParameteri");
		
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, 0);
		STARUtils.checkGLError("glBindTexture");
	}
	
	private void initFrameBufferDepthBuffer(int width, int height) {
		int[] depthRenderbufferHandles = new int[1];
		GLES30.glGenRenderbuffers(1, depthRenderbufferHandles, 0);
		STARUtils.checkGLError("glGenRenderbuffers");
		mFBODepth = depthRenderbufferHandles[0];
		Log.d(TAG, "set mFBODepth to " + mFBODepth);
		
		Log.d(TAG, "creating render buffer, binding depth buffer");
		GLES30.glBindRenderbuffer(GLES30.GL_RENDERBUFFER, mFBODepth);
		STARUtils.checkGLError("glBindRenderbuffer");
		
		
		GLES30.glRenderbufferStorage(GLES30.GL_RENDERBUFFER, GLES30.GL_DEPTH_COMPONENT16, width, height);
		STARUtils.checkGLError("glRenderbufferStorage");
		
		// attach render buffer as depth buffer
		GLES30.glFramebufferRenderbuffer(GLES30.GL_FRAMEBUFFER, GLES30.GL_DEPTH_ATTACHMENT, GLES30.GL_RENDERBUFFER, mFBODepth);
		STARUtils.checkGLError("glFramebufferRenderbuffer");
		
		GLES30.glBindRenderbuffer(GLES30.GL_RENDERBUFFER, 0);
		STARUtils.checkGLError("glBindRenderbuffer");
	}
	
	public void draw(GL10 gl) {	
		// bind the framebuffer
		
		GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, mFBO);
		STARUtils.checkGLError("glBindFramebuffer");
		
		GLES30.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		STARUtils.checkGLError("glClearColor");
		
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		STARUtils.checkGLError("glClear");
		
		if (mRequestedSelectionPoint != null) {
			// requested selection point; we should keep track of the colors->tool IDs as we draw
			mSparseArrayOfSelectableColorsToIDs = new SparseArray<Integer>();
		}
		
		
		Map<Integer, AnnotationMemory> annotationMemoryMap = AnnotationState.getInstance().getAnnotations();
		
		Iterator<Integer> iter = annotationMemoryMap.keySet().iterator();
		
		while (iter.hasNext()) {
			Integer id = iter.next();
			AnnotationMemory annotationMemory = annotationMemoryMap.get(id);
			
			if (annotationMemory != null && annotationMemory.isValid()) {
				
				Annotation currentAnnotation = annotationMemory.getCurrentAnnotation();
				if (currentAnnotation instanceof ToolAnnotation) {
					ToolAnnotation currentToolAnnotation = (ToolAnnotation) currentAnnotation;
					
					int toolColor = currentToolAnnotation.getSelectableColor();
					
					if (mRequestedSelectionPoint != null) {
						mSparseArrayOfSelectableColorsToIDs.put(toolColor, id);
					}
					
					drawSolidColorForTool(currentToolAnnotation, id);
				}
			} else {
				Log.d(TAG, "this annotation memory is invalid; remove it");
				annotationMemoryMap.remove(id);
			}
		}
		
		if (mRequestedSelectionPoint != null) {
			Log.d(TAG, "current state of color->id map");
			for (int i = 0; i < mSparseArrayOfSelectableColorsToIDs.size(); i++) {
				int key = mSparseArrayOfSelectableColorsToIDs.keyAt(i);
				Integer value = mSparseArrayOfSelectableColorsToIDs.get(key);
				Log.d(TAG, key + " --> " + value);
			}
		}
		
		// now that we've drawn, try to find the color at mRequestedSelectionPoint
		if (mRequestedSelectionPoint != null) {
			Log.d(TAG, "we have a requested selection point at " + mRequestedSelectionPoint);
			
			int readX = (int)mRequestedSelectionPoint.x - (mSelectionRegionSize / 2);
			int readY = ScreenState.getInstance().getScreenHeight() - (int)mRequestedSelectionPoint.y + (mSelectionRegionSize / 2);
			
			mRequestedSelectionPoint = null;
			
			GLES30.glFlush();
			STARUtils.checkGLError("glFlush");
						
			mSelectPixelBuffer.rewind();	// if we don't have this, then when we write to the buffer we may not be starting at the beginning 
			
			GLES30.glReadPixels(readX, readY, mSelectionRegionSize, mSelectionRegionSize, GLES30.GL_RGBA, GLES30.GL_UNSIGNED_BYTE, mSelectPixelBuffer);
			STARUtils.checkGLError("glReadPixels");
						
			Integer idOfTool = null;
			
			mSelectPixelBuffer.rewind();
			for (int i = 0; i < (mSelectionRegionSize * mSelectionRegionSize); i++) {
				byte rByte = mSelectPixelBuffer.get();
				byte gByte = mSelectPixelBuffer.get();
				byte bByte = mSelectPixelBuffer.get();
				byte aByte = mSelectPixelBuffer.get();
				
				int r = ((int)(rByte) + 256) % 256;
				int g = ((int)(gByte) + 256) % 256;
				int b = ((int)(bByte) + 256) % 256;
				int a = ((int)(aByte) + 256) % 256;
							
				int pixelColor = Color.rgb(r,g,b);
								
				if (!isClearColor(pixelColor)) {
					
					idOfTool = mSparseArrayOfSelectableColorsToIDs.get(pixelColor);
					if (idOfTool != null) {
						mDelegate.onSelectedTool(idOfTool);
						break;
					}
				}
			}
			
			if (idOfTool == null) {
				mDelegate.onSelectedTool(idOfTool);
			}
		}
		
		GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, 0);
		STARUtils.checkGLError("glBindFramebuffer");
	}

	private void drawSolidColorForTool(ToolAnnotation toolAnnotation, Integer id) {
		boolean isToolSelected = toolAnnotation.isSelected();
		
		mSelectableToolShaderProgram.drawTool(toolAnnotation, isToolSelected);
	}

	public void setRequestedSelectionPoint(Point point) {
		Log.d(TAG, "setting requested selection point to " + point);
		if (mRequestedSelectionPoint == null) {
			mRequestedSelectionPoint = point;
		}
	}
		
	private boolean isClearColor(int color) {
		return color == Color.BLACK;
	}
}
