package edu.purdue.andersed.streaming;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

public class Encoder {

	private static final String TAG = "Encoder";
	
	private int mEncodeWidth;
	private int mEncodeHeight;
	
	private boolean mCurrentlyEncodingFrame;
	
	private EncoderAsyncTask mEncoderAsyncTask;
	
	private Mat mRGBMatToEncode;
	
	private EncodeListener mListener;
	
	private static Encoder instance = new Encoder();
	
	static 
    {
    	try
    	{ 
    		Log.d(TAG, "loading streaming_video_manager");
    		System.loadLibrary("encoder");
    	}
    	catch( UnsatisfiedLinkError e )
		{
           System.err.println("Native code library failed to load.\n" + e);
           throw new RuntimeException("unable to load JNI module.");
		}
    }
	
	public static Encoder getInstance() {
		if (instance == null) {
			instance = new Encoder();
		}
		return instance;
	}
	
	public void requestEncodeFrame(Mat frameToEncode) {
		if (!mCurrentlyEncodingFrame) {
			Imgproc.resize(frameToEncode, mRGBMatToEncode, new Size(mEncodeWidth, mEncodeHeight));
			mCurrentlyEncodingFrame = true;
		} else {
			Log.d(TAG, "skipping encoding of this frame -- currenting encoding an existing frame");
		}
	}
	
	private Encoder() {
		Log.d(TAG, "initializing Encoder");
		
		mEncoderAsyncTask = new EncoderAsyncTask();
		
		mCurrentlyEncodingFrame = false;
	}
	
	public void init(EncodeListener listener, int encodeWidth, int encodeHeight) {
		Log.d(TAG, "init");
		
		mListener = listener;
		mEncodeWidth = encodeWidth;
		mEncodeHeight = encodeHeight;
		
		nInitEncoder(mEncodeWidth, mEncodeHeight);
		
		mRGBMatToEncode = new Mat(mEncodeHeight, mEncodeWidth, CvType.CV_8UC3);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// this is needed, or else doInBackground() doesn't get called
			mEncoderAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);	
		} else {
			mEncoderAsyncTask.execute();
		}
	}
	
	private class EncoderAsyncTask extends AsyncTask<Void, Void, Void> {

		private static final String TAG = "EncoderAsyncTask";
		
		private boolean mEncoderRunning;
		
		@Override
		protected Void doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			
			mEncoderRunning = true;
			
			while (mEncoderRunning) {
				if (mCurrentlyEncodingFrame) {
					Mat matToEncode = mRGBMatToEncode;
					
					long start = System.currentTimeMillis();
					byte[] encodedBytes = nEncode(matToEncode.getNativeObjAddr());
					long end = System.currentTimeMillis();
					Log.d(TAG, "did nEncode, took " + (end-start) + " millis, encodedBytes size = " + encodedBytes.length);
					
					if (encodedBytes != null) {
						if (mListener != null) {
							mListener.onFrameEncoded(encodedBytes);
						}
					} else {
						Log.e(TAG, "encoded bytes were null");
					}
					mCurrentlyEncodingFrame = false;
				}
			}
			
			return null;
		}
		
	}
	
	public void destroy() {
		Log.d(TAG, "destroy");
		
		mEncoderAsyncTask.cancel(true);
		
		nDestroyEncoder();
	}
	
	private native void nInitEncoder(int width, int height);
	private native byte[] nEncode(long matNativeObj);
	private native void nDestroyEncoder();
}
