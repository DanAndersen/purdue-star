package edu.purdue.andersed.star;

public class PerformanceLogEntry {
	
	private String mTag;
	private long mStartNanos;
	private long mEndNanos;
	
	public PerformanceLogEntry(String tag) {
		super();
		this.mTag = tag;
		this.mStartNanos = System.nanoTime();
		this.mEndNanos = mStartNanos;
	}
	
	public void complete() {
		this.mEndNanos = System.nanoTime();
	}
	
	public String getTag() {
		return mTag;
	}
	public long getNanosElapsed() {
		return mEndNanos - mStartNanos;
	}
	
	
}
