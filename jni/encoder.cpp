
// added to fix compilation error about UINT64_C not being defined (solution from https://github.com/AutonomyLab/ardrone_autonomy/issues/1 )
#ifndef UINT64_C
#define UINT64_C(c) (c ## ULL)
#endif

#ifdef __cplusplus
extern "C" {
#endif
// ffmpeg libraries are in C not C++, so this may help with proper linking
// https://stackoverflow.com/questions/15625468/libav-linking-error-undefined-references
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/pixfmt.h>
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
#ifdef __cplusplus
}
#endif



#include <pthread.h>

#include <jni.h>
#include <stdio.h>
#include <android/log.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <android/bitmap.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>



#include <GLES3/gl3.h>
#include <EGL/egl.h>


using namespace cv;

#define  LOG_TAG    "encoder"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGE(...)   __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define CHECK_MAT(cond) if(!(cond)){ LOGI("FAILED: " #cond); return; }

// https://ffmpeg.org/doxygen/trunk/doc_2examples_2decoding_encoding_8c-example.html

extern "C" {
	JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Encoder_nInitEncoder(JNIEnv * env, jobject obj, int width, int height);
	JNIEXPORT jbyteArray JNICALL Java_edu_purdue_andersed_streaming_Encoder_nEncode(JNIEnv * env, jobject obj, jlong matNativeObj);
	JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Encoder_nDestroyEncoder(JNIEnv * env, jobject obj);
};

//=============================================================================

#define INBUF_SIZE 4096

// encoding:
AVCodecContext* encoder_c = NULL;
AVFrame *encoder_frame;
AVPacket encoder_pkt;
int encoder_i;
SwsContext* encoder_sws;

int ENCODER_WIDTH_PIXELS;
int ENCODER_HEIGHT_PIXELS;

//=============================================================================

JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Encoder_nInitEncoder(JNIEnv * env, jobject obj, int width, int height) {
	LOGI("in nInit");

	ENCODER_WIDTH_PIXELS = width;
	ENCODER_HEIGHT_PIXELS = height;

	/* register all the codecs */
	avcodec_register_all();

	//enum AVCodecID codec_id = AV_CODEC_ID_MPEG4;
	enum AVCodecID codec_id = AV_CODEC_ID_MJPEG;

	AVCodec *codec;
	encoder_c = NULL;

	encoder_i = 0;


	/* find the video encoder */
	codec = avcodec_find_encoder(codec_id);
	if (!codec) {
		LOGE("codec not found");
		exit(1);
	}

	encoder_c = avcodec_alloc_context3(codec);
	if (!encoder_c) {
		LOGE("could not allocate video codec context");
		exit(1);
	}

	/* put sample parameters */
	encoder_c->bit_rate = 4000000;
	/* resolution must be a multiple of two */
	encoder_c->width = ENCODER_WIDTH_PIXELS;
	encoder_c->height = ENCODER_HEIGHT_PIXELS;
	/* frames per second */
	encoder_c->time_base = (AVRational){1,25};
	//encoder_c->gop_size = 10; /* emit one intra frame every ten frames */
	//encoder_c->max_b_frames = 1;
	//encoder_c->pix_fmt = AV_PIX_FMT_YUV420P;
	encoder_c->pix_fmt = AV_PIX_FMT_YUVJ420P;


	encoder_sws = sws_getContext(ENCODER_WIDTH_PIXELS, ENCODER_HEIGHT_PIXELS, AV_PIX_FMT_RGB24, ENCODER_WIDTH_PIXELS, ENCODER_HEIGHT_PIXELS, AV_PIX_FMT_YUVJ420P, SWS_FAST_BILINEAR, 0, 0, 0);

	if (codec_id == AV_CODEC_ID_H264) {
		av_opt_set(encoder_c->priv_data, "preset", "slow", 0);
	}

	/* open it */
	if (avcodec_open2(encoder_c, codec, NULL) < 0) {
		LOGE("Could not open codec");
		exit(1);
	}

	// here normally the output file would be opened, but we aren't going to write to a file

	encoder_frame = av_frame_alloc();
	if (!encoder_frame) {
		LOGE("Could not allocate video frame\n");
		exit(1);
	}
	encoder_frame->format = encoder_c->pix_fmt;
	encoder_frame->width  = encoder_c->width;
	encoder_frame->height = encoder_c->height;

	/* the image can be allocated by any means and av_image_alloc() is
	 * just the most convenient way if av_malloc() is to be used */
	int ret = av_image_alloc(encoder_frame->data, encoder_frame->linesize, encoder_c->width, encoder_c->height,
			encoder_c->pix_fmt, 32);
	if (ret < 0) {
		LOGE("Could not allocate raw picture buffer");
		exit(1);
	}

	// here is where we would encode each frame

	LOGI("done initing encoder");
}


JNIEXPORT jbyteArray JNICALL Java_edu_purdue_andersed_streaming_Encoder_nEncode(JNIEnv * env, jobject obj, jlong matNativeObj) {
	//LOGI("start nEncode");

	cv::Mat* matToEncode = (cv::Mat*)matNativeObj;

	av_init_packet(&encoder_pkt);
	encoder_pkt.data = NULL;	// packet data will be allocated by the encoder
	encoder_pkt.size = 0;

	uint8_t* rgb24Data = matToEncode->data;
	uint8_t * inData[1] = { rgb24Data };	// rgb24 has one plane
	int inLinesize[1] = { 3*ENCODER_WIDTH_PIXELS };	// rgb stride

	sws_scale(encoder_sws, inData, inLinesize, 0, ENCODER_HEIGHT_PIXELS, encoder_frame->data, encoder_frame->linesize);

	//encoder_frame->pts = encoder_i;	// TODO: check the timing to see what should be done here

	encoder_i++;

	int ret, got_output;

	// encode the image
	ret = avcodec_encode_video2(encoder_c, &encoder_pkt, encoder_frame, &got_output);
	if (ret < 0) {
		LOGE("error encoding frame");
		exit(1);
	}

	jbyteArray returnBytes = NULL;

	if (got_output) {
		//LOGI("write frame %3d (size=%5d)", encoder_i, encoder_pkt.size);

		returnBytes = env->NewByteArray(encoder_pkt.size);
		env->SetByteArrayRegion(returnBytes, 0, encoder_pkt.size, (const signed char*)encoder_pkt.data);
	} else {
		LOGE("didn't get output");
	}

	av_free_packet(&encoder_pkt);

	//LOGI("end nEncode");

	return returnBytes;
}

JNIEXPORT void JNICALL Java_edu_purdue_andersed_streaming_Encoder_nDestroyEncoder(JNIEnv * env, jobject obj) {
	LOGI("destroyEncoder");

	avcodec_close(encoder_c);
	av_free(encoder_c);
	av_freep(&encoder_frame->data[0]);
	av_frame_free(&encoder_frame);
	printf("\n");
}

