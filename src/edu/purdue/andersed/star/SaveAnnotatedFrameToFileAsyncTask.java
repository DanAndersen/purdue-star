package edu.purdue.andersed.star;

import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.features2d.KeyPoint;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.Matches;

public abstract class SaveAnnotatedFrameToFileAsyncTask extends AsyncTask<FrameWithAnnotation, Void, Void> {

	protected static final int ANNOTATION_POINT_RADIUS = 2;
	protected static final Scalar ANNOTATION_POINT_COLOR = new Scalar(0, 0, 255, 255);
	
	private static final int ANNOTATION_FEATURE_CROSS_LINE_LENGTH = 4;
	protected static final Scalar REFERENCE_FEATURE_CROSS_LINE_COLOR = new Scalar(255, 0, 0, 255);
	protected static final Scalar REFERENCE_DESCRIPTOR_RECT_COLOR = REFERENCE_FEATURE_CROSS_LINE_COLOR;
	
	protected static final Scalar CURRENT_DESCRIPTOR_RECT_COLOR = new Scalar(0, 255, 0, 255);
	
	private static final Scalar ANNOTATION_BOUNDING_RECT_COLOR = new Scalar(0, 0, 0, 255);
	
	private static final int BASE_LINE_THICKNESS = 1;
	
	private static final String TAG = "SaveAnnotatedFrameToFileAsyncTask";
	
	protected int mDrawingScaleFactor = 1;
	protected int mBoxOffset = 1;
	protected int mLineThickness = 1;
	protected int mMatchLineGreatestThickness = 1;
	protected int mMatchLineLeastThickness = 1;
	int mPointRadius = 1;
	
	long mTimestamp = 1;
	
	private static final String FILENAME_PREFIX = "star_illustration";

	private int mIllustrationCounter = 0;
	
	protected void saveImage(Mat image, String label) {
		mIllustrationCounter++;
		STARUtils.saveMatToFile(image, 
				FILENAME_PREFIX + "_" + 
				mTimestamp + "_" + 
				String.format("%03d", mIllustrationCounter) + "_" + 
				label + ".png");
	}
	
	@Override
	protected Void doInBackground(FrameWithAnnotation... referenceFrames) {
		//mDrawingScaleFactor = Math.max(8 / Pref.getInstance().getCvImageShrinkFactor(), 1);
		mDrawingScaleFactor = 8;
		mBoxOffset = Math.max(ANNOTATION_FEATURE_CROSS_LINE_LENGTH * mDrawingScaleFactor / 8, 1);
		mLineThickness = Math.max((int)(BASE_LINE_THICKNESS * mDrawingScaleFactor), 1);
		mMatchLineGreatestThickness = mLineThickness;
		mMatchLineLeastThickness = (int) Math.max(Math.round(mLineThickness/(2)), 1);
		mPointRadius = ANNOTATION_POINT_RADIUS * mDrawingScaleFactor;
		
		mTimestamp = System.currentTimeMillis();
		
		FrameWithAnnotation referenceFrameData = referenceFrames[0];
		
		MatOfKeyPoint currentFrameKeyPoints = referenceFrameData.getCurrentFrameKeyPoints();
		
		Mat image = referenceFrameData.getFullReferenceFrameImage();
		
		AnnotationMemory annotationMemory = referenceFrameData.getAnnotationMemory();
		
		doDrawing(currentFrameKeyPoints, annotationMemory, image);
		
		Toaster.getInstance().toast("done saving images");
		
		return null;
	}

	abstract void doDrawing(MatOfKeyPoint currentFrameKeyPoints, AnnotationMemory annotationMemory, Mat image);
	
	protected void drawBoundingRect(AnnotationMemory annotationMemory, Mat image) {
		
		Point tl = ScreenState.getInstance().openCVSpaceToScreenSpace(annotationMemory.getBoundingRect().tl());
		Point br = ScreenState.getInstance().openCVSpaceToScreenSpace(annotationMemory.getBoundingRect().br());
		
		// adjust so rect is within image bounds
		
		if (tl.x < (mLineThickness/2)) {
			tl.x = (mLineThickness/2);
		}
		
		if (tl.x > image.width() - (mLineThickness/2)) {
			tl.x = image.width() - (mLineThickness/2);
		}
		
		if (br.x < (mLineThickness/2)) {
			br.x = (mLineThickness/2);
		}
		
		if (br.x > image.width() - (mLineThickness/2)) {
			br.x = image.width() - (mLineThickness/2);
		}
		
		if (tl.y < (mLineThickness/2)) {
			tl.y = (mLineThickness/2);
		}
		
		if (tl.y > image.height() - (mLineThickness/2)) {
			tl.y = image.height() - (mLineThickness/2);
		}
		
		if (br.y < (mLineThickness/2)) {
			br.y = (mLineThickness/2);
		}
		
		if (br.y > image.height() - (mLineThickness/2)) {
			br.y = image.height() - (mLineThickness/2);
		}
		
		Core.rectangle(image, 
				tl, 
				br,
				ANNOTATION_BOUNDING_RECT_COLOR, 
				mLineThickness);
	}
	
	protected void drawDescriptors(MatOfKeyPoint keypoints, Mat image, Scalar color) {
		List<KeyPoint> keypointList = keypoints.toList();
		for (KeyPoint kp : keypointList) {
			Point p = kp.pt;
			
			Core.rectangle(image, 
					ScreenState.getInstance().openCVSpaceToScreenSpace(new Point(p.x-mBoxOffset, p.y-mBoxOffset)), 
					ScreenState.getInstance().openCVSpaceToScreenSpace(new Point(p.x+mBoxOffset, p.y+mBoxOffset)),
					color, 
					mLineThickness);
		}
	}
	
	protected void drawRawFeatures(MatOfKeyPoint rawKeyPoints, Mat image, Scalar color) {
		List<KeyPoint> rawKeyPointsList = rawKeyPoints.toList();
		for (KeyPoint kp : rawKeyPointsList) {
			Point p = kp.pt;
			
			Core.line(image, 
					ScreenState.getInstance().openCVSpaceToScreenSpace(new Point(p.x, p.y-mBoxOffset)), 
					ScreenState.getInstance().openCVSpaceToScreenSpace(new Point(p.x, p.y+mBoxOffset)), 
					color,
					mLineThickness);
			
			Core.line(image, 
					ScreenState.getInstance().openCVSpaceToScreenSpace(new Point(p.x-mBoxOffset, p.y)), 
					ScreenState.getInstance().openCVSpaceToScreenSpace(new Point(p.x+mBoxOffset, p.y)), 
					color,
					mLineThickness);
		}
	}
	
	protected void drawLinesBetweenMatches(Matches matches, Mat image,
			Scalar referenceDescriptorColor,
			Scalar currentDescriptorColor) {
		if (matches != null) {
			if (matches.getRefFrameKeyPoints().size() == matches.getCurFrameKeyPoints().size() && 
					matches.getRefFrameKeyPoints().size() == matches.getDistances().size()) {
				
				List<Float> distances = matches.getDistances();
				// get min/max distance for thickening match lines
				float minDistance = distances.get(0);
				float maxDistance = distances.get(0);
				for (float distance : distances) {
					if (distance < minDistance) {
						minDistance = distance;
					}
					if (distance > maxDistance) {
						maxDistance = distance;
					}
				}
				
				
				double[] startColor = referenceDescriptorColor.val;
				double[] endColor = currentDescriptorColor.val;
				
				for (int i = 0; i < matches.getRefFrameKeyPoints().size(); i++) {
					Point refPoint = ScreenState.getInstance().openCVSpaceToScreenSpace(matches.getRefFrameKeyPoints().get(i));
					Point curPoint = ScreenState.getInstance().openCVSpaceToScreenSpace(matches.getCurFrameKeyPoints().get(i));
					
					double deltaX = curPoint.x - refPoint.x;
					double deltaY = curPoint.y - refPoint.y;
					
					double distance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
					
					double percentage = 0;
					
					for (int t = 1; t < distance; t++) {
						double startPointX = refPoint.x + deltaX * percentage;
						double startPointY = refPoint.y + deltaY * percentage;

						percentage += (1/distance);
						
						double endPointX = refPoint.x + deltaX * percentage;
						double endPointY = refPoint.y + deltaY * percentage;
						
						float matchDistance = distances.get(i);
						
						float thickness = mMatchLineLeastThickness;
						if (Math.abs(maxDistance - minDistance) > 0.001f) {
							float matchDistanceAlpha = 1.0f-(matchDistance-minDistance)/(maxDistance - minDistance);	
							// when alpha=0, this is max distance, and should be thinnest
							// when alpha=1, this is min distance, and should be thickest
							
							thickness = lerp(mMatchLineLeastThickness, mMatchLineGreatestThickness, matchDistanceAlpha);
						}
						
						Core.line(image, 
								new Point(startPointX, startPointY), 
								new Point(endPointX, endPointY), 
								lerpColor(startColor, endColor, (float)percentage),
								Math.round(thickness));
					}
					
					
					/*
					Core.line(image, 
							refPoint, 
							curPoint, 
							referenceDescriptorColor,
							mMatchLineThickness);
					*/
				}
			} else {
				Log.e(TAG, "size mismatch in match lists: ref = " + matches.getRefFrameKeyPoints().size() + ", cur = " + matches.getCurFrameKeyPoints().size() + ", distances = " + matches.getDistances().size());
			}
		} else {
			Log.e(TAG, "matches is null");
		}
	}
	
	private Scalar lerpColor(double[] aRGBA, double[] bRGBA, float f) {
		float[] aHSV = new float[3];
		Color.RGBToHSV((int)aRGBA[0], (int)aRGBA[1], (int)aRGBA[2], aHSV);
		
		float[] bHSV = new float[3];
		Color.RGBToHSV((int)bRGBA[0], (int)bRGBA[1], (int)bRGBA[2], bHSV);
		
		float[] lerpHSV = new float[3];
		lerpHSV[0] = lerp(aHSV[0], bHSV[0], f);
		lerpHSV[1] = lerp(aHSV[1], bHSV[1], f);
		lerpHSV[2] = lerp(aHSV[2], bHSV[2], f);
		
		int rgb = Color.HSVToColor(lerpHSV);
		
		float alpha = lerp((float)aRGBA[3], (float)bRGBA[3], f);
		
		return new Scalar(Color.red(rgb), Color.green(rgb), Color.blue(rgb), alpha);
		
	}
	
	protected float lerp(float a, float b, float f) 
	{
	    return (a * (1.0f - f)) + (b * f);
	}
	

	protected void drawAnnotation(Annotation annotation, Mat image,
			Scalar color) {
		MatOfPoint2f pointsMat = annotation.getPointsMat();
		
		List<Point> points = pointsMat.toList();
		
		for (int i = 0; i < points.size(); i++) {
			
			Core.circle(image, 
					ScreenState.getInstance().openCVSpaceToScreenSpace(points.get(i)), 
					mPointRadius, 
					color, 
					-1);
			
			if (i < points.size() - 1) {
				Core.line(image, 
						ScreenState.getInstance().openCVSpaceToScreenSpace(points.get(i)), 
						ScreenState.getInstance().openCVSpaceToScreenSpace(points.get(i+1)), 
						color,
						mLineThickness);
			}
		}
	}
}
