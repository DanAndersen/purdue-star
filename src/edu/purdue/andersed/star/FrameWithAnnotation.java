package edu.purdue.andersed.star;

import org.json.JSONException;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;

import edu.purdue.andersed.star.annotations.AnnotationMemory;

public class FrameWithAnnotation {

	private Mat mFullReferenceFrameImage;
	
	private AnnotationMemory mAnnotationMemory;

	private MatOfKeyPoint mCurrentFrameKeyPoints;
	
	public FrameWithAnnotation(Mat fullReferenceFrameImage,
			MatOfKeyPoint currentFrameKeyPoints, AnnotationMemory annotationMemory) {
		super();
		
		mFullReferenceFrameImage = new Mat();
		fullReferenceFrameImage.copyTo(mFullReferenceFrameImage);
		
		mCurrentFrameKeyPoints = new MatOfKeyPoint();
		currentFrameKeyPoints.copyTo(mCurrentFrameKeyPoints);
		
		try {
			mAnnotationMemory = AnnotationMemory.fromJSON(annotationMemory.toJSON());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Mat getFullReferenceFrameImage() {
		return mFullReferenceFrameImage;
	}

	public AnnotationMemory getAnnotationMemory() {
		return mAnnotationMemory;
	}

	public MatOfKeyPoint getCurrentFrameKeyPoints() {
		return mCurrentFrameKeyPoints;
	}
	
	
	
}
