package edu.purdue.andersed.star.videosource;

import java.io.IOException;
import java.util.List;

import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.settings.Pref;

public class CameraVideoSource extends VideoSource {

	private static final String TAG = "CameraVideoSource";
	
	private Camera mCamera;

	private final String DESIRED_FOCUS_MODE = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO;
	
	private MainActivity mDelegate;
	
	private CameraHandlerThread mThread = null;
	
	private static final int NUM_CAMERA_PREVIEW_BUFFERS = 10;
	
	private int mBufferSize;
	
	public CameraVideoSource(MainActivity delegate) {
		super();
		mDelegate = delegate;
	}
	
	private void oldOpenCamera(SurfaceTexture surfaceTexture) {

		Log.d(TAG, "opening camera");
		mCamera = Camera.open();
		
		try {
			mCamera.setPreviewTexture(surfaceTexture);

			// http://stackoverflow.com/questions/3261776/determine-angle-of-view-of-smartphone-camera
			
			Camera.Parameters parameters = mCamera.getParameters();
		
			int imageFormat = parameters.getPreviewFormat();
			Size setSize = parameters.getPreviewSize();
			
			mBufferSize = setSize.width * setSize.height
                    * ImageFormat.getBitsPerPixel(imageFormat) / 8;
		    int sizeWeShouldHave = (setSize.width * setSize.height * 3 / 2);
		    
		    if (mBufferSize != sizeWeShouldHave) {
		        String status = "Bad calculate size. Should have been " + (setSize.width * setSize.height * 3 / 2) +
		                " but got " + imageFormat;
		        throw new UnsupportedOperationException(status);
		    }
			
			
			if (Pref.getInstance().isUsingAutoFocus()) {
				Log.d(TAG, "setting auto-focus setting");
				List<String> focusModes = parameters.getSupportedFocusModes();
				if (focusModes.contains(DESIRED_FOCUS_MODE)) {
					parameters.setFocusMode(DESIRED_FOCUS_MODE);
				}
				mCamera.setParameters(parameters);	
			}
			
			for (int i = 0; i < NUM_CAMERA_PREVIEW_BUFFERS; i++) {
				byte[] cameraBuffer = new byte[mBufferSize];
				mCamera.addCallbackBuffer(cameraBuffer);
			}
			
			mCamera.setPreviewCallbackWithBuffer(mDelegate);
			
			Log.d(TAG, "starting camera preview");
			mCamera.startPreview();
		} catch (IOException e) {
			Log.e(TAG, "unable to set preview texture: " + e.getMessage());
		}
	}
	
	public void initAndStartVideoSource(SurfaceTexture surfaceTexture) {
		Log.d(TAG, "initAndStartVideoSource");
		
		if (mThread == null) {
			mThread = new CameraHandlerThread();
		}
		
		synchronized(mThread) {
			mThread.openCamera(surfaceTexture);
		}
		
	}
	
	public void stopVideoSource() {
		Log.d(TAG, "stopVideoSource");
		
		if (mCamera != null) {
			mCamera.setPreviewCallbackWithBuffer(null);
			mCamera.stopPreview();
	        mCamera.release();	
	        mCamera = null;
		}
		
	}
	
	// https://stackoverflow.com/questions/18149964/best-use-of-handlerthread-over-other-similar-classes/19154438#19154438
	private class CameraHandlerThread extends HandlerThread {
		Handler mHandler = null;
		
		CameraHandlerThread() {
			super("CameraHandlerThread");
			start();
			mHandler = new Handler(getLooper());
		}
		
		synchronized void notifyCameraOpened() {
			notify();
		}
		
		void openCamera(final SurfaceTexture surfaceTexture) {
			mHandler.post(new Runnable() {
				
				@Override
				public void run() {
					oldOpenCamera(surfaceTexture);
					notifyCameraOpened();
				}
			});
			try {
				wait();
			} catch (InterruptedException e) {
				Log.w(TAG, "wait was interrupted");
			}
		}
	}
	
}
