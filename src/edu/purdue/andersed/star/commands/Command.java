package edu.purdue.andersed.star.commands;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public abstract class Command implements Serializable {

	private static final String TAG = "Command";
	
	public static final String KEY_COMMAND = "command";
	
	public abstract JSONObject toJSON() throws JSONException;

	public static Command commandFromJSON(JSONObject commandJsonObject) {
		try {
			String commandType = commandJsonObject.getString(KEY_COMMAND);
			
			if (commandType != null) {
				
				if (commandType.equals(SendAnnotationsCommand.TAG)) {
					return SendAnnotationsCommand.fromJSON(commandJsonObject);
				} else if (commandType.equals(CreateAnnotationCommand.TAG)) {
					return CreateAnnotationCommand.fromJSON(commandJsonObject);
				} else if (commandType.equals(DeleteAnnotationCommand.TAG)) {
					return DeleteAnnotationCommand.fromJSON(commandJsonObject);
				} else if (commandType.equals(UpdateAnnotationCommand.TAG)) {
					return UpdateAnnotationCommand.fromJSON(commandJsonObject);
				}else {
					Log.d(TAG, "error: unknown command type: " + commandType);
					return null;
				}
				
			} else {
				Log.e(TAG, "error: command type is null");
				return null;
			}
		} catch (JSONException e) {
			Log.e(TAG, "error: no command type in this message");
			return null;
		}
		
		
	}
}
