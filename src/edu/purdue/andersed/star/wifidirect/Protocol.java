package edu.purdue.andersed.star.wifidirect;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.features2d.KeyPoint;

import android.util.Log;
import edu.purdue.andersed.star.annotations.Matches;

public class Protocol {

	private static final String TAG_X = "x";
	private static final String TAG_Y = "y";
	
	private static final String TAG_ANGLE = "angle";
	private static final String TAG_CLASS_ID = "class_id";
	private static final String TAG_OCTAVE = "octave";
	private static final String TAG_POINT = "point";
	private static final String TAG_RESPONSE = "response";
	private static final String TAG_SIZE = "size";
	
	private static final String TAG_NUM_ROWS = "num_rows";
	private static final String TAG_NUM_COLS = "num_cols";
	private static final String TAG_MAT_TYPE = "type";
	private static final String TAG_MAT_ROWS = "rows";
	
	private static final String TAG_REF_POINTS = "ref_points";
	private static final String TAG_CUR_POINTS = "cur_points";
	private static final String TAG_DISTANCES = "distances";
	
	private static final String TAG = "Protocol";
	
	public static JSONArray matOfPoint2fToJSONArray(MatOfPoint2f matOfPoint2f) throws JSONException {
		JSONArray matOfPoint2fArray = new JSONArray();
		
		List<Point> points = matOfPoint2f.toList();
		for (Point point : points) {
			matOfPoint2fArray.put(pointToJSONObject(point));
		}
		
		return matOfPoint2fArray;
	}
	
	public static JSONObject pointToJSONObject(Point point) throws JSONException {
		JSONObject pointObject = new JSONObject();
		
		pointObject.put(TAG_X, point.x);
		pointObject.put(TAG_Y, point.y);
		
		return pointObject;
	}
	
	private static Point jsonObjectToPoint(JSONObject pointJSONObject) throws JSONException {
		
		Point p = new Point();
		
		if (pointJSONObject.has(TAG_X)) {
			double x = pointJSONObject.getDouble(TAG_X);
			
			p.x = x;
		}
		
		if (pointJSONObject.has(TAG_Y)) {
			double y = pointJSONObject.getDouble(TAG_Y);
			
			p.y = y;
		}
		
		return p;
	}
	
	public static MatOfKeyPoint jsonArrayToMatOfKeyPoint(JSONArray matOfKeyPointArray) throws JSONException {
		MatOfKeyPoint matOfKeyPoint = new MatOfKeyPoint();
		
		List<KeyPoint> keyPoints = new ArrayList<KeyPoint>();
		
		for (int i = 0; i < matOfKeyPointArray.length(); i++) {
			KeyPoint keyPoint = jsonObjectToKeyPoint(matOfKeyPointArray.getJSONObject(i));
			keyPoints.add(keyPoint);
		}
		
		matOfKeyPoint.fromList(keyPoints);
		
		return matOfKeyPoint;
	}
	
	public static JSONArray matOfKeyPointToJSONArray(MatOfKeyPoint matOfKeyPoint) throws JSONException {
		JSONArray matOfKeyPointArray = new JSONArray();
		
		List<KeyPoint> keyPoints = matOfKeyPoint.toList();
		for (KeyPoint keyPoint : keyPoints) {
			matOfKeyPointArray.put(keyPointToJSONObject(keyPoint));
		}
		return matOfKeyPointArray;
	}
	
	public static JSONObject keyPointToJSONObject(KeyPoint keyPoint) throws JSONException {
		JSONObject keyPointObject = new JSONObject();
		
		keyPointObject.put(TAG_ANGLE, keyPoint.angle);
		keyPointObject.put(TAG_CLASS_ID, keyPoint.class_id);
		keyPointObject.put(TAG_OCTAVE, keyPoint.octave);
		keyPointObject.put(TAG_POINT, pointToJSONObject(keyPoint.pt));
		keyPointObject.put(TAG_RESPONSE, keyPoint.response);
		keyPointObject.put(TAG_SIZE, keyPoint.size);
		
		return keyPointObject;
	}
	
	private static KeyPoint jsonObjectToKeyPoint(JSONObject keyPointObject) throws JSONException {
		
		KeyPoint keyPoint = new KeyPoint();
		
		if (keyPointObject.has(TAG_ANGLE)) {
			float angle = (float) keyPointObject.getDouble(TAG_ANGLE);
			
			keyPoint.angle = angle;
		}
		
		if (keyPointObject.has(TAG_CLASS_ID)) {
			int class_id = keyPointObject.getInt(TAG_CLASS_ID);
			
			keyPoint.class_id = class_id;
		}
		
		if (keyPointObject.has(TAG_OCTAVE)) {
			int octave = keyPointObject.getInt(TAG_OCTAVE);
			
			keyPoint.octave = octave;
		}
		
		if (keyPointObject.has(TAG_POINT)) {
			Point point = jsonObjectToPoint(keyPointObject.getJSONObject(TAG_POINT));
			
			keyPoint.pt = point;
		}
		
		if (keyPointObject.has(TAG_RESPONSE)) {
			float response = (float) keyPointObject.getDouble(TAG_RESPONSE);
			
			keyPoint.response = response;
		}
		
		if (keyPointObject.has(TAG_SIZE)) {
			float size = (float) keyPointObject.getDouble(TAG_SIZE);
			
			keyPoint.size = size;
		}
		
		return keyPoint;
	}

	public static Mat jsonObjectToMat(JSONObject matObject) throws JSONException {
		
		if (matObject != null) {
			int rows = matObject.getInt(TAG_NUM_ROWS);
			int cols = matObject.getInt(TAG_NUM_COLS);
			int type = matObject.getInt(TAG_MAT_TYPE);
			
			Mat mat = new Mat(rows, cols, type);
			
			JSONArray matRowsArray = matObject.getJSONArray(TAG_MAT_ROWS);
			
			for (int i = 0; i < matRowsArray.length(); i++) {
				JSONArray matRowArray = matRowsArray.getJSONArray(i);
				for (int j = 0; j < matRowArray.length(); j++) {
					double dataAtCell = matRowArray.getDouble(j);
					
					mat.put(i, j, dataAtCell);
				}
			}
			
			return mat;	
		} else {
			return null;
		}
	}
	
	// NOTE: I'm assuming that each double[] for a cell in the Mat is of size 1
	// so I'm not storing a double[] but just a double
	private static double[] jsonArrayToDoubleArray(JSONArray jsonArray) throws JSONException {
		int len = jsonArray.length();
		
		double[] doubleArray = new double[len];
		
		for (int i = 0; i < len; i++) {
			doubleArray[i] = jsonArray.getDouble(i);
		}
		return doubleArray;
	}
	
	public static JSONObject matToJSONObject(Mat mat) throws JSONException {
		JSONObject matObject = new JSONObject();
		
		matObject.put(TAG_NUM_ROWS, mat.rows());
		matObject.put(TAG_NUM_COLS, mat.cols());
		matObject.put(TAG_MAT_TYPE, mat.type());
		matObject.put(TAG_MAT_ROWS, matRowsToJSONArray(mat));

		return matObject;
	}
	
	public static JSONArray matRowsToJSONArray(Mat mat) throws JSONException {
		JSONArray matRowsArray = new JSONArray();
		
		for (int i = 0; i < mat.rows(); i++) {
			matRowsArray.put(matRowToJSONArray(mat.row(i)));
		}
		
		return matRowsArray;
	}
	
	public static JSONArray matRowToJSONArray(Mat matRow) throws JSONException {
		JSONArray matRowArray = new JSONArray();
		
		for (int i = 0; i < matRow.cols(); i++) {
			matRowArray.put(matRow.get(0, i)[0]);
		}
		return matRowArray;
	}

	public static MatOfPoint2f jsonArrayToMatOfPoint2f(JSONArray matOfPoint2fJsonArray) throws JSONException {
		MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
		
		List<Point> points = new ArrayList<Point>();
		
		for (int i = 0; i < matOfPoint2fJsonArray.length(); i++) {
			Point point = jsonObjectToPoint(matOfPoint2fJsonArray.getJSONObject(i));
			points.add(point);
		}
		
		matOfPoint2f.fromList(points);
		
		return matOfPoint2f;
	}

	public static JSONObject matchesToJSONObject(Matches matches) throws JSONException {
		JSONObject matchesObject = new JSONObject();
		
		JSONArray refPointsArray = new JSONArray();
		
		JSONArray curPointsArray = new JSONArray();
		
		JSONArray distancesArray = new JSONArray();
		
		matchesObject.put(TAG_REF_POINTS, refPointsArray);
		matchesObject.put(TAG_CUR_POINTS, curPointsArray);
		matchesObject.put(TAG_DISTANCES, distancesArray);
		
		if (matches != null) {
			for (Point refPoint : matches.getRefFrameKeyPoints()) {
				refPointsArray.put(pointToJSONObject(refPoint));
			}
			
			for (Point curPoint : matches.getCurFrameKeyPoints()) {
				curPointsArray.put(pointToJSONObject(curPoint));
			}
			
			for (float distance : matches.getDistances()) {
				distancesArray.put(distance);
			}
		}
				
		return matchesObject;
	}

	public static Matches jsonObjectToMatches(JSONObject matchesObject) throws JSONException {
		List<Point> refPoints = new ArrayList<Point>();
		List<Point> curPoints = new ArrayList<Point>();
		List<Float> distances = new ArrayList<Float>();
		
		JSONArray refPointsJSONArray = matchesObject.getJSONArray(TAG_REF_POINTS);
		
		for (int i = 0; i < refPointsJSONArray.length(); i++) {
			Point refPoint = jsonObjectToPoint(refPointsJSONArray.getJSONObject(i));
			refPoints.add(refPoint);
		}
		
		JSONArray curPointsJSONArray = matchesObject.getJSONArray(TAG_CUR_POINTS);
		
		for (int i = 0; i < curPointsJSONArray.length(); i++) {
			Point curPoint = jsonObjectToPoint(curPointsJSONArray.getJSONObject(i));
			curPoints.add(curPoint);
		}
		
		JSONArray distancesJSONArray = matchesObject.getJSONArray(TAG_DISTANCES);
		
		for (int i = 0; i < distancesJSONArray.length(); i++) {
			float distance = (float) distancesJSONArray.getDouble(i);
			distances.add(distance);
		}
		
		Matches matches = new Matches(refPoints, curPoints, distances);
				
		return matches;
	}


}
