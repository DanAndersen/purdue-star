package edu.purdue.andersed.star.wifidirect;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

import org.opencv.core.Mat;

import android.graphics.Bitmap;
import android.net.wifi.p2p.WifiP2pInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import edu.purdue.andersed.star.Constants;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.UserType;
import edu.purdue.andersed.star.settings.Pref;

public class PeerConnection {

	private static PeerConnection instance;
	
	private static final String TAG = "PeerConnection";
		
	private UserType mOwnUserType;
	
	private String mMentorIP;
		
	private boolean mShouldSendFramesToOtherUser = false;
	
	private PeerConnection() {
		Log.d(TAG, "initializing PeerConnection");
	}
	
	public static PeerConnection getInstance() {
		if (instance == null) {
			instance = new PeerConnection();
		}
		return instance;
	}
	
	public UserType getOwnUserType() {
		if (mOwnUserType == null) {
			throw new IllegalStateException("mOwnUserType not set yet");
		}
		
		return mOwnUserType;
	}
	
	public void setOwnUserType(UserType userType) {
		mOwnUserType = userType;
	}
	
	public void setShouldSendFramesToOtherUser(boolean value) {
		mShouldSendFramesToOtherUser = value;
	}
	
	public boolean shouldSendFramesToOtherUser() {
		return mShouldSendFramesToOtherUser;
	}
	
	public void sendFrameDataToOtherUser(MainActivity delegate, Mat mat) {
		delegate.getFrameClient().requestSendFrame(mat);
	}

	public void setMentorIP(String mentorIP) {
		mMentorIP = mentorIP;
	}

	public String getMentorIP() {
		return mMentorIP;
	}
}
