package edu.purdue.andersed.star.shaders;

import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES11Ext;
import android.opengl.GLES30;
import android.util.Log;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.geometry.CameraPreviewQuad;

public class DirectVideo implements CameraViewBackground {

	private static final String TAG = "DirectVideo";
	
	private int mDirectVideoTextureID;
	
	private CameraPreviewQuad mGeometry;

	private int mShaderProgramID;

	private int mVertexHandle;
	private int mTextureCoordHandle;
	private int mTexSampler2DHandle;
	
	private static final String PASS_THROUGH_VERTEX_SHADER =
		"attribute vec4 vertexPosition; \n" +
		"attribute vec2 vertexTexCoord; \n" +
		"varying vec2 texCoord; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_Position = vertexPosition; \n" +
		"    texCoord = vertexTexCoord; \n" +
		"} \n"
			;
		
	private static final String DIRECT_VIDEO_FRAGMENT_SHADER = 
		"#extension GL_OES_EGL_image_external : require\n" +
		"precision mediump float; \n" + 
		"varying vec2 texCoord; \n" +
		"uniform samplerExternalOES texSamplerExternal; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_FragColor = texture2D(texSamplerExternal, texCoord); \n" +
		"} \n"
			;
	
	public DirectVideo(int directVideoTextureID) {
		Log.d(TAG, "setting up direct video with texture ID = " + directVideoTextureID);
		mDirectVideoTextureID = directVideoTextureID;
		
		Log.d(TAG, "creating geometry for camera preview quad");
		mGeometry = new CameraPreviewQuad();
		
		Log.d(TAG, "creating shader program");
		mShaderProgramID = STARUtils.createProgramFromShaderSrc(PASS_THROUGH_VERTEX_SHADER, DIRECT_VIDEO_FRAGMENT_SHADER);
		
		Log.d(TAG, "setting up handles");
		mVertexHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexPosition");
		STARUtils.checkGLError("glGetAttribLocation");
		mTextureCoordHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexTexCoord");
		STARUtils.checkGLError("glGetAttribLocation");
		mTexSampler2DHandle = GLES30.glGetAttribLocation(mShaderProgramID, "texSampler2D");
		STARUtils.checkGLError("glGetAttribLocation");
	}

	@Override
	public void draw(GL10 gl) {
		
		GLES30.glUseProgram(mShaderProgramID);
		STARUtils.checkGLError("glUseProgram");
		GLES30.glVertexAttribPointer(mVertexHandle, 3, GLES30.GL_FLOAT, false, 0, mGeometry.getVertices());
		STARUtils.checkGLError("glVertexAttribPointer");
		GLES30.glVertexAttribPointer(mTextureCoordHandle, 2, GLES30.GL_FLOAT, false, 0, mGeometry.getTexCoords());
		STARUtils.checkGLError("glVertexAttribPointer");
		GLES30.glEnableVertexAttribArray(mVertexHandle);
		STARUtils.checkGLError("glEnableVertexAttribArray");
		GLES30.glEnableVertexAttribArray(mTextureCoordHandle);
		STARUtils.checkGLError("glEnableVertexAttribArray");
		GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
		STARUtils.checkGLError("glActiveTexture");
        GLES30.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
        		mDirectVideoTextureID);
        STARUtils.checkGLError("glBindTexture");
        GLES30.glUniform1i(mTexSampler2DHandle, 0);
        STARUtils.checkGLError("glUniform1i");
        GLES30.glDrawElements(GLES30.GL_TRIANGLES,
        		mGeometry.getNumObjectIndex(), GLES30.GL_UNSIGNED_SHORT,
        		mGeometry.getIndices());
        STARUtils.checkGLError("glDrawElements");
		GLES30.glDisableVertexAttribArray(mVertexHandle);
		STARUtils.checkGLError("glDisableVertexAttribArray");
		GLES30.glDisableVertexAttribArray(mTextureCoordHandle);
		STARUtils.checkGLError("glDisableVertexAttribArray");
		
	}

}
