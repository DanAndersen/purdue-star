package edu.purdue.andersed.star.videosource;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import edu.purdue.andersed.star.recordings.RecordedVideoFile;


public class RecordedVideoErrorLogging {

	private static final String TAG = "RecordedVideoErrorLogging";
	
	private static RecordedVideoErrorLogging instance;
	
	private List<FrameErrorData> mFrameErrorDataList = new ArrayList<FrameErrorData>();
	
	private int mFrameNumber = 0;
	
	private String mVideoLabel = "(unlabeled)";
	
	public static RecordedVideoErrorLogging getInstance() {
		if (instance == null) {
			instance = new RecordedVideoErrorLogging();
		}
		return instance;
	}
	
	public void resetLogging(RecordedVideoFile videoFile) {
		mFrameErrorDataList = new ArrayList<FrameErrorData>();
		mFrameNumber = 0;
		mVideoLabel = videoFile.getLabel();
	}
	
	public void addLogForFrame(int frameInRecordingNumber, double error) {
		FrameErrorData frameErrorData = new FrameErrorData(mFrameNumber, frameInRecordingNumber, error);
		mFrameErrorDataList.add(frameErrorData);
		
		mFrameNumber++;
	}
	
	public void printErrorLog() {
		Log.i(TAG, "\n");
		
		Log.i(TAG, "# Error logging \n");
		Log.i(TAG, "# Video:\t" + mVideoLabel + "\n");
		Log.i(TAG, "\n");
		
		Log.i(TAG, "# Average error:\n");
		Log.i(TAG, getAverageError() + "\n");
		Log.i(TAG, "\n");
		
		FrameErrorData frameErrorDataWithMaximumError = getFrameErrorDataWithMaximumError();
		if (frameErrorDataWithMaximumError != null) {
			Log.i(TAG, "# Maximum error:\n");
			Log.i(TAG, "# Frame\t" + "Frame in Recording\t" + "Error\t" + "\n");
			Log.i(TAG, frameErrorDataWithMaximumError.getFrameNumber() + "\t" + frameErrorDataWithMaximumError.getFrameInRecordingNumber() + "\t" + frameErrorDataWithMaximumError.getError() + "\t" + "\n");
			Log.i(TAG, "\n");
		}
		
		Log.i(TAG, "# Frame\t" + "Frame in Recording\t" + "Error\t" + "\n");
		
		for (FrameErrorData frameErrorData : mFrameErrorDataList) {
			Log.i(TAG, frameErrorData.getFrameNumber() + "\t" + frameErrorData.getFrameInRecordingNumber() + "\t" + frameErrorData.getError() + "\t" + "\n");
		}
		
			
	}
	
	private FrameErrorData getFrameErrorDataWithMaximumError() {
		double maxError = 0;
		FrameErrorData frameErrorDataWithMaximumError = null;
		for (FrameErrorData frameErrorData : mFrameErrorDataList) {
			if (frameErrorData.getError() > maxError) {
				frameErrorDataWithMaximumError = frameErrorData;
				maxError = frameErrorData.getError();
			}
		}
		return frameErrorDataWithMaximumError;
	}

	public double getAverageError() {
		if (mFrameErrorDataList.size() > 0) {
			double totalError = 0;
			for (FrameErrorData frameErrorData : mFrameErrorDataList) {
				totalError += frameErrorData.getError();
			}
			double averageError = totalError / mFrameErrorDataList.size();
			return averageError;
		} else {
			return 0;
		}
		
	}
}
