package edu.purdue.andersed.star;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

public class FrameTracing {

	private static final String TAG = "FrameTracing";

	private static final double NANOSECONDS_PER_MILLISECOND = 1000000.0;
		
	private Map<FrameTracingTag, Long> mStartNanoMap;
	private Map<FrameTracingTag, Long> mEndNanoMap;
	
	private Map<FrameTracingTag, Integer> mValueMap;
	
	public FrameTracing() {
		init();
	}
	
	public void init() {
		mStartNanoMap = new HashMap<FrameTracingTag, Long>();
		mEndNanoMap = new HashMap<FrameTracingTag, Long>();
		mValueMap = new HashMap<FrameTracingTag, Integer>();
	}
	
	public void start(FrameTracingTag tag) {
		//Log.d(TAG, "start [" + tag + "]");
		long nanos = System.nanoTime();
		
		mStartNanoMap.put(tag, nanos);
		mEndNanoMap.put(tag, nanos);
	}
	
	public void end(FrameTracingTag tag) {
		//Log.d(TAG, "end [" + tag + "]");
		long nanos = System.nanoTime();
		
		mEndNanoMap.put(tag, nanos);
	}
	
	public void log(FrameTracingTag tag, int value) {
		//Log.d(TAG, "log [" + tag + "], value = " + value);
		mValueMap.put(tag, value);
	}
	
	private double getValue(FrameTracingTag tag) {
		Integer value = mValueMap.get(tag);
		
		if (value != null) {
			return value;
		} else {
			return 0;
		}
	}
	
	private double getElapsedMillis(FrameTracingTag tag) {
		Long startNanos = mStartNanoMap.get(tag);
		Long endNanos = mEndNanoMap.get(tag);
		
		if (startNanos != null && endNanos != null) {
			return (endNanos - startNanos) / NANOSECONDS_PER_MILLISECOND;
		} else {
			return 0;
		}
	}
	
	public String getLogHeader() {
		String headerLine = "";
		for (FrameTracingTag tag : FrameTracingTag.values()) {
			headerLine += tag.name() + "\t";
		}
		headerLine += "remainder";
		return headerLine;
	}
	
	public String getLogLine() {
		String valueLine = "";
		double remainder = getElapsedMillis(FrameTracingTag.total);
		for (FrameTracingTag tag : FrameTracingTag.values()) {
			if (mStartNanoMap.containsKey(tag) && mEndNanoMap.containsKey(tag)) {
				double millis = getElapsedMillis(tag);
				valueLine += millis + "\t";
				
				if (tag != FrameTracingTag.total) {
					remainder -= millis;
				}
			} else {
				double value = getValue(tag);
				valueLine += value + "\t";
			}
		}
		valueLine += remainder;
		return valueLine;
	}
	
}
