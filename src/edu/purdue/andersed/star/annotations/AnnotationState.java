package edu.purdue.andersed.star.annotations;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import edu.purdue.andersed.star.FrameKeyPointData;
import edu.purdue.andersed.star.FrameWithAnnotation;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PreviousStepsState;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.SaveAnnotationMatchFrameAsyncTask;
import edu.purdue.andersed.star.SaveAnnotationReferenceFrameAsyncTask;
import edu.purdue.andersed.star.Toaster;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.NetworkUtil;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class AnnotationState {

	private static final String TAG = "AnnotationState";
	
	private static AnnotationState instance;
	
	private Map<Integer, AnnotationMemory> mAnnotationMemoryMap;
			
	private FrameKeyPointData mCurrentFrameKeyPointData;
	
	// storing all points internally in OpenCV's image coordinates
	
	private Mat mCurrentFrame;
	
	private Mat mCurrentFullResolutionFrame;	// used only when saving illustrations
	
	private Integer mAnnotationIdIndex;	// every annotation memory has a particular integer ID -- increments on creation, does not decrement on deletion; goal is to have each annotation with a unique id per session
	
	private AnnotationState() {
		mAnnotationMemoryMap = new ConcurrentHashMap<Integer, AnnotationMemory>();
		mCurrentFrame = new Mat();
		mCurrentFullResolutionFrame = new Mat();
		
		mAnnotationIdIndex = 0;
	}
	
	private FrameKeyPointData getCurrentFrameKeyPointData() {
		return mCurrentFrameKeyPointData;
	}
	
	public void updateCurrentFullResolutionFrame(Mat currentFullResolutionFrame) {
		if (currentFullResolutionFrame != null) {
			synchronized(currentFullResolutionFrame) {
				currentFullResolutionFrame.copyTo(mCurrentFullResolutionFrame);
			}	
		}
	}
	
	public void updateCurrentFrame(Mat currentFrame) {
		synchronized(currentFrame) {
			currentFrame.copyTo(mCurrentFrame);
		}
	}
	
	public void updateFrameData(MatOfKeyPoint rawKeyPoints, MatOfKeyPoint keyPoints, Mat descriptors) {
		synchronized(rawKeyPoints) {
			synchronized(keyPoints) {
				synchronized(descriptors) {
					mCurrentFrameKeyPointData = new FrameKeyPointData(rawKeyPoints, keyPoints, descriptors);		
				}
			}
		}
		
	}
	
	public static AnnotationState getInstance() {
		if (instance == null) {
			instance = new AnnotationState();
		}
		return instance;
	}

	public Integer addPointWithScreenCoordinates(Context context, double screenX, double screenY) {
		Log.d(TAG, "addPointWithScreenCoordinates(" + screenX + ", " + screenY + ")");
		
		PointAnnotation pointAnnotation = new PointAnnotation(screenX, screenY);
		
		AnnotationMemory annotationMemory = new AnnotationMemory(pointAnnotation, getCurrentFrameKeyPointData());
				
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public Integer addToolWithScreenCoordinatesRotationAndScale(Context context, ToolType toolType, float touchX, float touchY, double radians, double scale) {
		Log.d(TAG, "addToolWithScreenCoordinates(" + touchX + ", " + touchY + ")");
		
		ToolAnnotation toolAnnotation = new ToolAnnotation(new Point(touchX, touchY), toolType, radians, scale, STARUtils.getRandomColor());
		AnnotationMemory annotationMemory = new AnnotationMemory(toolAnnotation, getCurrentFrameKeyPointData());
		
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public void updateToolAnchorPoint(Integer toolAnnotationMemoryID, Point anchorPoint) {
		Log.d(TAG, "updateToolAnchorPoint(" + toolAnnotationMemoryID + ", " + anchorPoint + ")");

		if (toolAnnotationMemoryID != null) {
			
			AnnotationMemory toolAnnotationMemory = AnnotationState.getInstance().getAnnotations().get(toolAnnotationMemoryID);
			
			if (toolAnnotationMemory != null) {
				Annotation initialAnnotation = toolAnnotationMemory.getInitialAnnotation();
				if (initialAnnotation instanceof ToolAnnotation) {
					
					ToolAnnotation initialToolAnnotation = (ToolAnnotation) initialAnnotation;
					initialToolAnnotation.setPointInScreenSpace(anchorPoint);
					
					ToolAnnotation replacedToolAnnotation = new ToolAnnotation(anchorPoint, initialToolAnnotation.getToolType(), initialToolAnnotation.getDegrees(), initialToolAnnotation.getScale(), initialToolAnnotation.getSelectableColor());
					
					AnnotationMemory newToolAnnotationMemory = new AnnotationMemory(replacedToolAnnotation, getCurrentFrameKeyPointData());			
					
					mAnnotationMemoryMap.put(toolAnnotationMemoryID, newToolAnnotationMemory);
										
				} else {
					Log.e(TAG, "annotation is not a tool annotation");
				}
			} else {
				Log.e(TAG, "cannot update tool annotation when annotation memory is null");
			}
		} else {
			Log.e(TAG, "cannot update tool annotation when id is null");
		}
	}

	public Integer initializeNewPolygonAnnotation(Context context) {
		PolygonAnnotation polygonAnnotation = new PolygonAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(polygonAnnotation, getCurrentFrameKeyPointData());
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public Integer initializeNewAnimatedIncisionAnnotation(Context context) {
		AnimatedIncisionAnnotation animatedIncisionAnnotation = new AnimatedIncisionAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(animatedIncisionAnnotation, getCurrentFrameKeyPointData());
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public Integer initializeNewMultiPointAnnotation(Context context) {
		MultiPointAnnotation multiPointAnnotation = new MultiPointAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(multiPointAnnotation, getCurrentFrameKeyPointData());
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public Integer initializeNewPolylineAnnotation(Context context) {
		PolylineAnnotation polylineAnnotation = new PolylineAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(polylineAnnotation, getCurrentFrameKeyPointData());
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public void clearAnnotations(MainActivity delegate) {
		Log.d(TAG, "clearAnnotations");		
		
		Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
		
		while (iter.hasNext()) {
			Integer id = iter.next();
			
			NetworkUtil.sendDeleteAnnotationCommand(delegate, id);
			
			mAnnotationMemoryMap.remove(id);
		}
	}
	
	public Map<Integer, AnnotationMemory> getAnnotations() {
		return mAnnotationMemoryMap;
	}

	public Integer addAnnotationMemory(AnnotationMemory annotationMemory) {
		Integer id = (mAnnotationIdIndex++);
		
		return addAnnotationMemoryWithID(id, annotationMemory);
	}
	
	public Integer addAnnotationMemoryWithID(Integer id, AnnotationMemory annotationMemory) {

		updateAnnotationMemoryWithID(id, annotationMemory);
		
		return id;
	}
	
	public void updateAnnotationMemoryWithID(Integer id, AnnotationMemory annotationMemory) {
		
		if (PeerConnection.getInstance().getOwnUserType().isTrainee() && Pref.getInstance().isShowingPreviousSteps()) {
			PreviousStepsState.getInstance().requestFrameCapture();
		}
		
		mAnnotationMemoryMap.put(id, annotationMemory);
	}
	
	public void updateAnnotationMemoryWithFrameData(AnnotationMemory annotationMemory) {
		annotationMemory.initializeMemory(annotationMemory.getInitialAnnotation(), getCurrentFrameKeyPointData());
	}
	
	public void recordReferenceAnnotationIllustrations(Integer annotationMemoryID) {
		if (Pref.getInstance().isSavingAnnotationReferenceFrame()) {
			if (mAnnotationMemoryMap.containsKey(annotationMemoryID)) {
				AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(annotationMemoryID);
				
				SaveAnnotationReferenceFrameAsyncTask saveAnnotationReferenceFrameAsyncTask = new SaveAnnotationReferenceFrameAsyncTask();

				FrameWithAnnotation referenceFrameData = new FrameWithAnnotation(mCurrentFullResolutionFrame, mCurrentFrameKeyPointData.getCurrentFrameKeyPoints(), annotationMemory);
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					// this is needed, or else doInBackground() doesn't get called
					saveAnnotationReferenceFrameAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, referenceFrameData);	
				} else {
					saveAnnotationReferenceFrameAsyncTask.execute(referenceFrameData);
				}
				
			} else {
				Toaster.getInstance().toast("couldn't find annotation memory to save ref frame");
			}
		}
	}
	
	// this is used when we don't need all the feature data, just coordinates of points
	public JSONObject toSimpleJSON() throws JSONException {
		JSONObject annotationStateObject = new JSONObject();
		
		if (mAnnotationMemoryMap != null) {
			Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
			
			while (iter.hasNext()) {
				Integer id = iter.next();
				AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);
				
				if (id != null && annotationMemory != null) {
					String idString = id.toString();
					JSONObject annotationMemoryObject = annotationMemory.toSimpleJSON();
					
					annotationStateObject.put(idString, annotationMemoryObject);
				}
			}
		}
		
		//Log.d(TAG, "result: " + annotationStateObject.toString());
		return annotationStateObject;
	}
	
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject annotationStateObject = new JSONObject();
		
		if (mAnnotationMemoryMap != null) {
			Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
			
			while (iter.hasNext()) {
				Integer id = iter.next();
				AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);
				
				annotationStateObject.put(id.toString(), annotationMemory.toJSON());
			}
		}
		
		//Log.d(TAG, "result: " + annotationStateObject.toString());
		return annotationStateObject;
	}
	
	public void updateFromJSONObject(JSONObject annotationStateObject) throws JSONException {
		Map<Integer, AnnotationMemory> annotationMemoryMap = new ConcurrentHashMap<Integer, AnnotationMemory>();
		
		Iterator<?> keys = annotationStateObject.keys();
		
		while (keys.hasNext()) {
			String idString = (String)keys.next();
			
			Integer id = Integer.parseInt(idString);
			
			JSONObject annotationMemoryObject = annotationStateObject.getJSONObject(idString);
			AnnotationMemory annotationMemory = AnnotationMemory.fromJSON(annotationMemoryObject);
			
			annotationMemoryMap.put(id, annotationMemory);
		}
		
		mAnnotationMemoryMap = annotationMemoryMap;
	}

	public void removeAnnotationWithID(MainActivity delegate, Integer annotationId) {
		
		NetworkUtil.sendDeleteAnnotationCommand(delegate, annotationId);
		
		mAnnotationMemoryMap.remove(annotationId);
	}


	public void captureFrameMatches() {
		Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
		
		synchronized (mCurrentFrameKeyPointData) {
			while (iter.hasNext()) {
				Integer id = iter.next();
				AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);
				
				SaveAnnotationMatchFrameAsyncTask saveAnnotationMatchFrameAsyncTask = new SaveAnnotationMatchFrameAsyncTask();

				FrameWithAnnotation referenceFrameData = new FrameWithAnnotation(mCurrentFullResolutionFrame, mCurrentFrameKeyPointData.getCurrentFrameKeyPoints(), annotationMemory);
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					// this is needed, or else doInBackground() doesn't get called
					saveAnnotationMatchFrameAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, referenceFrameData);	
				} else {
					saveAnnotationMatchFrameAsyncTask.execute(referenceFrameData);
				}
			}
		}
		
	}

	public void translateAllAnnotations(Point point) {
		Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
	
		while (iter.hasNext()) {
			Integer id = iter.next();
			AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);
			Annotation initialAnnotation = annotationMemory.getInitialAnnotation();
			
			initialAnnotation.translate(point);
			annotationMemory.updateCurrentAnnotationBasedOnHomography();
		}
		
	}



	

}
