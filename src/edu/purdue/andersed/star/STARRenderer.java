package edu.purdue.andersed.star;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import org.opencv.core.Point;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.util.Log;
import edu.purdue.andersed.star.shaders.CameraViewBackground;
import edu.purdue.andersed.star.shaders.DirectVideo;
import edu.purdue.andersed.star.shaders.Texture2DVideo;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class STARRenderer implements GLSurfaceView.Renderer {

	private static final String TAG = "STARRenderer";

	int mDirectVideoTextureID;
	private CameraViewBackground mCameraViewBackground;
	
	private SurfaceTexture mSurfaceTexture;
	
	MainActivity mDelegate;
		
	private long mLastFrameMillis = 0L;
	
	private PointOverlay mPointOverlay;
	private SelectableOverlay mSelectableOverlay;
			
	public STARRenderer(MainActivity delegate) {
		mDelegate = delegate;
	}

	public PointOverlay getPointOverlay() {
		return mPointOverlay;
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		Log.d(TAG, "onSurfaceCreated");

		Log.d(TAG, "creating texture for direct video");
		if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
			Log.d(TAG, "mentors don't have direct video source but need a texture to put frames into");
			mDirectVideoTextureID = createTexture();
			
			Log.d(TAG, "created texture for direct video, id = " + mDirectVideoTextureID);
			
			mCameraViewBackground = new Texture2DVideo(mDirectVideoTextureID);
		} else {
			Log.d(TAG, "trainees have the direct video source");
			mDirectVideoTextureID = createTextureForDirectVideo();
			
			Log.d(TAG, "created texture for direct video, id = " + mDirectVideoTextureID);
			
			Log.d(TAG, "creating class to manage drawing video background");
			mCameraViewBackground = new DirectVideo(mDirectVideoTextureID);
		}
		
		

		
		
		
		GLES30.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
		STARUtils.checkGLError("glClearColor");
		
		
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		
		Log.d(TAG, "onSurfaceChanged, width = " + width + ", height = " + height);
		
		Log.d(TAG, "GL surface created, tell camera to start up");
		mDelegate.startCamera(mDirectVideoTextureID);
		
		GLES30.glViewport(0, 0, width, height);
		
		Log.d(TAG, "setting up blend function");
		GLES30.glEnable(GLES30.GL_BLEND);
		GLES30.glBlendFunc(GLES30.GL_SRC_ALPHA, GLES30.GL_ONE_MINUS_SRC_ALPHA);
		GLES30.glBlendEquation(GLES30.GL_FUNC_ADD);
		
		mPointOverlay = new PointOverlay(mDelegate);
		
		mSelectableOverlay = new SelectableOverlay(mDelegate);
	}

	@Override
	public void onDrawFrame(GL10 gl) {
				
		//logFrameRate();
		
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		STARUtils.checkGLError("glClear");
		
		if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {
			//Log.d(TAG, "the trainee is using a camera source that is directly updating a texture for us");
			mSurfaceTexture.updateTexImage(); // It will implicitly bind its texture to the GL_TEXTURE_EXTERNAL_OES texture target.	
		}
		
		mCameraViewBackground.draw(gl);
		
		mPointOverlay.draw(gl);
		
		mSelectableOverlay.draw(gl);
	}

	private static int createTexture() {
		return createTextureFor(GLES30.GL_TEXTURE_2D);
	}
	
	private static int createTextureFor(int textureTarget) {
		int[] textureIDs = new int[1];

		GLES30.glGenTextures(1, textureIDs, 0);
		STARUtils.checkGLError("glGenTextures");
		GLES30.glBindTexture(textureTarget, textureIDs[0]);
		STARUtils.checkGLError("glBindTexture");
		GLES30.glTexParameterf(textureTarget,
				GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
		STARUtils.checkGLError("glTexParameterf");
		GLES30.glTexParameterf(textureTarget,
				GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		STARUtils.checkGLError("glTexParameterf");
		GLES30.glTexParameteri(textureTarget,
				GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		STARUtils.checkGLError("glTexParameteri");
		GLES30.glTexParameteri(textureTarget,
				GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
		STARUtils.checkGLError("glTexParameteri");

		return textureIDs[0];
	}

	private static int createTextureForDirectVideo() {
		return createTextureFor(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);
	}

	public void setSurfaceTexture(SurfaceTexture surfaceTexture) {
		mSurfaceTexture = surfaceTexture;
	}
	
	private void logFrameRate() {
		long currentTimeMillis = System.currentTimeMillis();
		
		if (mLastFrameMillis > 0) {
			long elapsedMillis = currentTimeMillis - mLastFrameMillis;
			long fps = 1000 / elapsedMillis;
			
			mDelegate.onFPSUpdated(fps, elapsedMillis);
			
			Log.d(TAG, "last frame draw time: " + elapsedMillis + " ms (" + fps + " FPS)");
		}
		
		mLastFrameMillis = currentTimeMillis;
	}

	public int getDirectVideoTextureID() {
		return mDirectVideoTextureID;
	}
	
	public CameraViewBackground getCameraViewBackground() {
		return mCameraViewBackground;
	}

	public void onRequestSelectionAt(Point point) {
		Log.d(TAG, "onRequestSelectionAt");
		if (mSelectableOverlay != null) {
			mSelectableOverlay.setRequestedSelectionPoint(point);
		}
	}

	public void onNewFrameBitmapFromSocket(Bitmap bitmap) {
		if (mPointOverlay != null) {
			mPointOverlay.onNewFrameBitmapFromSocket(bitmap);	
		} else {
			Log.d(TAG, "point overlay is currently null");
		}
		
	}

}
