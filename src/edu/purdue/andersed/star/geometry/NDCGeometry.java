package edu.purdue.andersed.star.geometry;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public abstract class NDCGeometry {

	protected static final float MAX_Z = 1.0f;

	protected Buffer mVertBuff;
	
	protected int verticesNumber;
	
	protected void setVerts(float[] verts) {
		
		mVertBuff = fillBuffer(verts);
		verticesNumber = verts.length / 3;
	}
	
	protected Buffer fillBuffer(float[] array) {
		// Each float takes 4 bytes
        ByteBuffer bb = ByteBuffer.allocateDirect(4 * array.length);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for (float d : array)
            bb.putFloat(d);
        bb.rewind();
        
        return bb;
	}
	
	protected Buffer fillBuffer(short[] array)
    {
        // Each short takes 2 bytes
        ByteBuffer bb = ByteBuffer.allocateDirect(2 * array.length);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for (short s : array)
            bb.putShort(s);
        bb.rewind();
        
        return bb;
    }
	
	public Buffer getVertices() {
		return mVertBuff;
	}
	
	public int getNumVertices() {
		return verticesNumber;
	}
}
