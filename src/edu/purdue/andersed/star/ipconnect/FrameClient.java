package edu.purdue.andersed.star.ipconnect;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.json.JSONException;
import org.opencv.core.Mat;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.commands.Command;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.PeerConnection;
import edu.purdue.andersed.streaming.EncodeListener;
import edu.purdue.andersed.streaming.Encoder;

public class FrameClient implements EncodeListener {

	// used by the trainee system, establishes a connection to the mentor system and supports sending frames to the mentor server
	
	private static final String TAG = "FrameClient";
	
	private FrameClientAsyncTask mFrameClientAsyncTask;
		
	private boolean mIsReadyToSendNewFrame;
	
	private Queue<byte[]> mByteArraysToSend;
		
	public FrameClient() {
		Log.d(TAG, "constructing FrameClient");
				
		mFrameClientAsyncTask = new FrameClientAsyncTask();
				
		mByteArraysToSend = new ConcurrentLinkedQueue<byte[]>();
	}
	
	public void init() {
		Log.d(TAG, "initializing FrameClient");
		
		if (Pref.getInstance().isEncodingDecodingFrames()) {
			int encodeWidth = ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor();
			int encodeHeight = ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor();
			Encoder.getInstance().init(this, encodeWidth, encodeHeight);	
		}
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// this is needed, or else doInBackground() doesn't get called
			mFrameClientAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);	
		} else {
			mFrameClientAsyncTask.execute();
		}
		
		mIsReadyToSendNewFrame = true;
		
		
	}
	
	public void requestSendFrame(Mat mat) {
		if (mIsReadyToSendNewFrame) {
			
			if (Pref.getInstance().isEncodingDecodingFrames()) {
				Encoder.getInstance().requestEncodeFrame(mat);
			} else {
				//ready to send another frame, proceeding
				Mat matCopy = new Mat();
				mat.copyTo(matCopy);
				
				byte buff[] = new byte[(int) (matCopy.total() * matCopy.channels())];
				
				matCopy.get(0, 0, buff);
				
				mByteArraysToSend.add(buff);
			}
			
			mIsReadyToSendNewFrame = false;
			
			
		} else {
			// system isn't ready to send another frame, skipping this one
		}
		
		
	}
	
	
	public void destroy() {
		Log.d(TAG, "destroying FrameClient");
	}
	
	private class FrameClientAsyncTask extends AsyncTask<Void, Void, Void> {

		private Socket mSocket;
		
		private boolean mRunning;
		
		@Override
	    protected void onPreExecute() {
			mRunning = true;
	    }
		
		@Override
		protected Void doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			
			String host = PeerConnection.getInstance().getMentorIP();
			int port = Pref.getInstance().getVideoServerSocketPort();
			
			Log.d(TAG, "going to connect to " + host + ":" + port);
			try {
				mSocket = new Socket(host, port);
	            
				Log.d(TAG, "started socket");
				
				OutputStream out = mSocket.getOutputStream();
				
				while (mRunning) {
					
					while (mSocket.isConnected()) {
						
						
						while(!mByteArraysToSend.isEmpty()) {
							mIsReadyToSendNewFrame = false;
							
							byte[] byteArrayToSend = mByteArraysToSend.remove();
							
							byte[] lengthBytes = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder()).putInt(byteArrayToSend.length).array();
							
							if (Pref.getInstance().isEncodingDecodingFrames()) {
								out.write(lengthBytes);
							}
							out.write(byteArrayToSend);
							out.flush();
						}
						
						mIsReadyToSendNewFrame = true;
					}					
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			return null;
		}
		
		@Override
	    protected void onPostExecute(Void result) {
	    }
		
	}

	@Override
	public void onFrameEncoded(byte[] encodedBytes) {
		mByteArraysToSend.add(encodedBytes);
	}
	
}
