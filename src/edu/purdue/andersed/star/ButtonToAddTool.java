package edu.purdue.andersed.star;

import android.content.res.Resources;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import edu.purdue.andersed.star.annotations.ToolType;

public class ButtonToAddTool {

	private ToggleButton mToggleButton;
	
	private MainActivity mDelegate;
	
	private ToolType mToolType;
	
	public ButtonToAddTool(final MainActivity delegate, ViewGroup viewGroupParent, final ToolType toolType, int labelResourceId, int imageResourceId, int imageRotateDegrees) {
		mDelegate = delegate;
		mToolType = toolType;
		
		Resources resources = mDelegate.getResources();
		
		int toolImageSize = (int) resources.getDimension(R.dimen.tool_image_size);
		int toolButtonWidth = (int) resources.getDimension(R.dimen.tool_button_width);
		int toolButtonHeight = (int) resources.getDimension(R.dimen.tool_button_height);
		
		ImageView imageView = new ImageView(mDelegate);
		RelativeLayout.LayoutParams imageViewLayoutParams = new RelativeLayout.LayoutParams(toolImageSize, toolImageSize);
		imageViewLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		imageView.setLayoutParams(imageViewLayoutParams);
		imageView.setRotation(imageRotateDegrees);
		imageView.setScaleType(ScaleType.FIT_CENTER);
		imageView.setImageResource(imageResourceId);
		
		String label = resources.getString(labelResourceId);
		
		mToggleButton = new ToggleButton(mDelegate);
		mToggleButton.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		mToggleButton.setBackground(resources.getDrawable(R.drawable.ic_toggle_bg));
		mToggleButton.setTextOff(label);
		mToggleButton.setTextOn(label);
		
		RelativeLayout relativeLayout = new RelativeLayout(mDelegate);
		relativeLayout.setLayoutParams(new LayoutParams(toolButtonWidth, toolButtonHeight));
		
		relativeLayout.addView(imageView);
		relativeLayout.addView(mToggleButton);
		
		viewGroupParent.addView(relativeLayout);
		
		mToggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				mDelegate.onToolToggled(isChecked, mToolType);
			}
		});
	}
	
	public void setCheckedState() {
		this.mToggleButton.setChecked(
				mDelegate.getUIMode() == UIMode.ADDING_TOOL && 
				mDelegate.getActiveToolType() == mToolType);
	}
}

