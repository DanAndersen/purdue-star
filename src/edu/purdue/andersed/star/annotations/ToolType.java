package edu.purdue.andersed.star.annotations;

import edu.purdue.andersed.star.R;

public enum ToolType {
	BVM("bvm", new ToolTemplate(
			R.drawable.bvm_shadow, 
			R.drawable.bvm_selected,
			572, 268, 
			63, 119)),
	ETTUBE("ettube", new ToolTemplate(
			R.drawable.ettube_shadow, 
			R.drawable.ettube_selected,
			650, 328, 
			42, 168)),
	HEMOSTAT("hemostat", new ToolTemplate(
			R.drawable.hemostat_shadow, 
			R.drawable.hemostat_selected,
			495, 254, 
			6, 125)),
	IODINESWAB("iodineswab", new ToolTemplate(
			R.drawable.iodineswab_shadow, 
			R.drawable.iodineswab_selected,
			775, 96, 
			44, 47)),
	RETRACTOR("retractor", new ToolTemplate(
			R.drawable.retractor_shadow, 
			R.drawable.retractor_selected,
			460, 358, 
			47, 164)),
	SCALPEL("scalpel", new ToolTemplate(
			R.drawable.scalpel_shadow, 
			R.drawable.scalpel_selected,
			475, 51, 
			9, 25)),
	STETHOSCOPE("stethoscope", new ToolTemplate(
			R.drawable.stethoscope_shadow, 
			R.drawable.stethoscope_selected,
			560, 580, 
			92, 320)),
	SURGICALTAPE("surgicaltape", new ToolTemplate(
			R.drawable.surgicaltape_shadow, 
			R.drawable.surgicaltape_selected,
			394, 348, 
			51, 294)),
	SYRINGE("syringe", new ToolTemplate(
			R.drawable.syringe_shadow, 
			R.drawable.syringe_selected,
			534, 103, 
			9, 55)),
	TWEEZERS("tweezers", new ToolTemplate(
			R.drawable.tweezers_shadow, 
			R.drawable.tweezers_selected,
			558, 90, 
			21, 43)),
	LONGHOOK("longhook", new ToolTemplate(
			R.drawable.longhook_shadow, 
			R.drawable.longhook_selected,
			592, 48, 
			33, 24)),
	SCISSORS("scissors", new ToolTemplate(
			R.drawable.scissors_shadow, 
			R.drawable.scissors_selected,
			336, 198, 
			15, 96)),
			
	TEXT_CLOSE("text_close", new ToolTemplate(
			R.drawable.text_close_shadow, 
			R.drawable.text_close_selected,
			211, 66, 
			105, 31)),
	TEXT_INCISION("text_incision", new ToolTemplate(
			R.drawable.text_incision_shadow, 
			R.drawable.text_incision_selected,
			309, 66, 
			155, 32)),
	TEXT_PALPATION("text_palpation", new ToolTemplate(
			R.drawable.text_palpation_shadow,
			R.drawable.text_palpation_selected,
			377, 79, 
			377/2, 79/2)),
	TEXT_REMOVE("text_remove", new ToolTemplate(
			R.drawable.text_remove_shadow, 
			R.drawable.text_remove_selected,
			305, 51, 
			305/2, 51/2)),
	TEXT_STITCH("text_stitch", new ToolTemplate(
			R.drawable.text_stitch_shadow, 
			R.drawable.text_stitch_selected,
			228, 66, 
			228/2, 66/2)),
			
	HAND_PALPATE("hand_palpate", new ToolTemplate(
			R.drawable.hand_palpate_shadow, 
			R.drawable.hand_palpate_selected,
			290, 141, 
			16, 19)),
			
	HAND_POINT("hand_point", new ToolTemplate(
			R.drawable.hand_point_shadow, 
			R.drawable.hand_point_selected,
			287, 157, 
			12, 19)),
			
	HAND_STRETCH("hand_stretch", new ToolTemplate(
			R.drawable.hand_stretch_shadow, 
			R.drawable.hand_stretch_selected,
			256, 237, 
			36, 120));

	//--------------------------------
	
	private String mTag;
	private ToolTemplate mToolTemplate;
	
	ToolType(String tag, ToolTemplate toolTemplate) {
		this.mTag = tag;
		this.mToolTemplate = toolTemplate;
	}
	
	public String getTag() {
		return mTag;
	}
	
	public ToolTemplate getToolTemplate() {
		return mToolTemplate;
	}
	
	public static ToolType fromTag(String tag) {
		if (tag != null) {
			for (ToolType tt : ToolType.values()) {
				if (tag.equalsIgnoreCase(tt.getTag())) {
					return tt;
				}
			}
		}
		
		throw new IllegalStateException("unknown tool type: " + tag);
		
	}
}