package edu.purdue.andersed.star;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Point;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Debug;
import android.preference.PreferenceManager;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import ar.com.daidalos.afiledialog.FileChooserDialog;
import edu.purdue.andersed.star.annotations.AnimatedIncisionAnnotation;
import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.annotations.AnnotationStateLogging;
import edu.purdue.andersed.star.annotations.FullAnnotationSave;
import edu.purdue.andersed.star.annotations.MultiPointAnnotation;
import edu.purdue.andersed.star.annotations.PolygonAnnotation;
import edu.purdue.andersed.star.annotations.PolylineAnnotation;
import edu.purdue.andersed.star.annotations.ToolAnnotation;
import edu.purdue.andersed.star.annotations.ToolType;
import edu.purdue.andersed.star.ipconnect.DataClient;
import edu.purdue.andersed.star.ipconnect.DataServer;
import edu.purdue.andersed.star.ipconnect.FrameClient;
import edu.purdue.andersed.star.ipconnect.FrameServerVideoSource;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.shaders.Texture2DVideo;
import edu.purdue.andersed.star.videosource.CameraVideoSource;
import edu.purdue.andersed.star.videosource.FileVideoSource;
import edu.purdue.andersed.star.videosource.VideoSource;
import edu.purdue.andersed.star.wifidirect.NetworkUtil;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class MainActivity extends Activity implements SurfaceTexture.OnFrameAvailableListener, OnTouchListener, OnClickListener, PreviewCallback {
	private static final String TAG = "MainActivity";
	
	private StarGLSurfaceView mGlSurfaceView;
	private STARRenderer mRenderer;
	private SurfaceTexture mSurfaceTexture;
	
	private RelativeLayout mUILayout;

	private ToggleButton mAddPointAnnotationButton;
	private ToggleButton mAddMultiPointAnnotationButton;
	private ToggleButton mAddLineAnnotationButton;
	private ToggleButton mAddPolygonAnnotationButton;
	private ToggleButton mAddAnimatedIncisionAnnotationButton;
	
	private List<ButtonToAddTool> mButtonsToAddTool;
	
	private Button mRequestStartSendingFramesButton;
	private Button mSendAnnotationsButton;
		
	private Button mSaveAnnotationsButton;
	private Button mLoadAnnotationsButton;
	
	private Button mMoveAllAnnotationsLeftButton;
	private Button mMoveAllAnnotationsRightButton;
	private Button mMoveAllAnnotationsUpButton;
	private Button mMoveAllAnnotationsDownButton;
	private Button mResetAnnotationTranslateButton;
	
	private Button mToggleAnchoringButton;
	
	private UIMode mUIMode;
	
	private Button mClearAnnotationsButton;
	
	private ImageView mPreviewImageView;
	
	private VideoSource mVideoSource;
		
	private Integer mWorkingMultiPointAnnotationMemoryID;
	
	private Integer mWorkingPolylineAnnotationMemoryID;
	
	private Integer mWorkingPolygonAnnotationMemoryID;
	
	private Integer mWorkingToolAnnotationMemoryID;
	
	private Integer mWorkingAnimatedIncisionAnnotationMemoryID;
	
	private ToolType mActiveToolType;
	
	private Button mRemoveSelectedAnnotationButton;
	
	private Button mCaptureFrameMatchButton;
	
	private TextView mEngineerTextView;
	private EngineerViewData mEngineerViewData;
	
	private DataServer mDataServer;
	private DataClient mDataClient;
	
	private FrameClient mFrameClient;
	
	private boolean mShouldRenderNewFrames;
	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			Log.d(TAG, "onManagerConnected");
			switch (status) {
			case LoaderCallbackInterface.SUCCESS:
			{
				Log.d(TAG, "OpenCV loaded successfully");
				
				onOpenCVLoaded();
				break;
			}
			default: 
			{
				super.onManagerConnected(status);
				break;
			}
			}
		}
	};
	
	protected void onOpenCVLoaded() {
		/*
		StreamingVideoManager.getInstance().init(this);
		
		
		RelativeLayout container = (RelativeLayout) findViewById(R.id.streaming_surface_view_container);
		
		SurfaceView streamingSurfaceView = new SurfaceView(this);
		streamingSurfaceView.setLayoutParams(new RelativeLayout.LayoutParams(640, 360));
		
		StreamingVideoManager.getInstance().registerSurface(streamingSurfaceView);
		
		container.addView(streamingSurfaceView);
		*/
		
		if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
			mDataServer = new DataServer();
			mDataServer.init();		
			
		} else if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {
			mDataClient = new DataClient();
			mDataClient.init();
			
			mFrameClient = new FrameClient();
			mFrameClient.init();
		}
		
		initializeDisplay();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");		
		super.onCreate(savedInstanceState);
		
		Toaster.initialize(this);
		
		Display display = getWindowManager().getDefaultDisplay();
		android.graphics.Point size = new android.graphics.Point();
		display.getSize(size);
		
		
		ScreenState.getInstance().initScreenSize(size.x, size.y);
		
		
		
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean pref = prefs.getBoolean("checkbox_preference", false);
        Log.d(TAG, "pref = " + pref);
	    
		
		setContentView(R.layout.main);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		
		//StreamingVideoManager.getInstance().init(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
		
		View decorView = getWindow().getDecorView();
		// Hide the status bar.
		int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
		decorView.setSystemUiVisibility(uiOptions);
		
		mShouldRenderNewFrames = true;
		
		Pref.getInstance().initialize(this);
		
		if (Pref.getInstance().isTracing()) {
			Debug.startMethodTracing();	
		}
		
		try {
			AnnotationStateLogging.getInstance().initLogging();
			FrameProcessorPerformanceLogging.getInstance().initLogging();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Log.d(TAG, "initializing OpenCV...");
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_8, this, mLoaderCallback);
	}
	
	private void initializeDisplay() {
		Log.d(TAG, "initializeDisplay");
		
		
		
		
		mButtonsToAddTool = new ArrayList<ButtonToAddTool>();
		
		mEngineerViewData = new EngineerViewData();
		
		mGlSurfaceView = new StarGLSurfaceView(this);
		mRenderer = mGlSurfaceView.getRenderer();
		
		addContentView(mGlSurfaceView, new LayoutParams(LayoutParams.MATCH_PARENT,
	            LayoutParams.MATCH_PARENT));
		
		Log.d(TAG, "setting up touch listener");
		mGlSurfaceView.setOnTouchListener(this);
		
		Log.d(TAG, "initializing video source");
		//mVideoSource = Config.USING_RECORDED_VIDEO ? new FileVideoSource(this) : new CameraVideoSource();
		
		if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
			// mentor
			
			mVideoSource = new FrameServerVideoSource(this);
		} else {
			// trainee
			if (Pref.getInstance().isUsingRecordedVideo()) {
				mVideoSource = new FileVideoSource(this);
			} else {
				mVideoSource = new CameraVideoSource(this);
			}
			
		}

		addOverlayView();
		
	}

	public void startCamera(int textureID) {
		Log.d(TAG, "startCamera");

		Log.d(TAG, "setting up surface texture");
		mSurfaceTexture = new SurfaceTexture(textureID);
		mSurfaceTexture.setOnFrameAvailableListener(this);
		mRenderer.setSurfaceTexture(mSurfaceTexture);
		
		mVideoSource.initAndStartVideoSource(mSurfaceTexture);
		
		if (!Pref.getInstance().isUsingMockConnection()) {
			if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {
				Log.d(TAG, "trainee should start sending frames by default to mentor");
				PeerConnection.getInstance().setShouldSendFramesToOtherUser(true);
			}	
		}
	}
	
	@Override
    public void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		
		if (mGlSurfaceView != null) {
			mGlSurfaceView.onPause();
    	}
		
		if (mVideoSource != null) {
			mVideoSource.stopVideoSource();
		}
		
		AnnotationStateLogging.getInstance().closeLog();
		FrameProcessorPerformanceLogging.getInstance().closeLog();
		
		if (Pref.getInstance().isTracing()) {
			Debug.stopMethodTracing();	
		}
    }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
			mDataServer.destroy();
		} else if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {
			mDataClient.destroy();
			mFrameClient.destroy();
		}
		
		
		
		//StreamingVideoManager.getInstance().destroy();
	}

	@Override
	public void onFrameAvailable(SurfaceTexture surfaceTexture) {
		//mGlSurfaceView.requestRender();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = MotionEventCompat.getActionMasked(event);
		
		float touchX = event.getX();
		float touchY = event.getY();
		
		switch (action) {
		case (MotionEvent.ACTION_DOWN):
			//Log.d(TAG, "Action was DOWN");
						
			Log.d(TAG, "touch coordinates: " + touchX + ", " + touchY);
		
			if (mUIMode.equals(UIMode.RECEIVING_FRAMES)) {
				updateMode(UIMode.EDIT_MODE);
			}
		
			if (mUIMode.equals(UIMode.EDIT_MODE)) {
				onSelectedTool(null);
				selectToolAnnotationAtScreenCoordinates(touchX, touchY);
			}
			else if (mUIMode.equals(UIMode.ADDING_POINTS)) {
				createPointAtScreenCoordinates(touchX, touchY);		
			} else if (mUIMode.equals(UIMode.ADDING_LINE)) {
				createOrContinueLineAtScreenCoordinates(touchX, touchY);
			} else if (mUIMode.equals(UIMode.ADDING_ANIMATED_INCISION)) {
				createOrContinueAnimatedIncisionAtScreenCoordinates(touchX, touchY);
			} else if (mUIMode.equals(UIMode.ADDING_POLYGON)) {
				createOrContinuePolygonAtScreenCoordinates(touchX, touchY);
			} else if (mUIMode.equals(UIMode.ADDING_TOOL)) {
				if (mActiveToolType != null) {
					createToolAtScreenCoordinates(mActiveToolType, touchX, touchY);
				} else {
					Toaster.getInstance().toast("adding tool, but no active tool type selected");
				}
			}
			
			return true;
		case (MotionEvent.ACTION_MOVE):
			Log.d(TAG, "Action was MOVE");
		
			if (mUIMode.equals(UIMode.ADDING_TOOL) || mUIMode.equals(UIMode.EDIT_MODE) && mWorkingToolAnnotationMemoryID != null) {
				
				int numPointers = event.getPointerCount();
				
				if (numPointers > 1) {
					// multitouch
					
					float anchorTouchX = event.getX(0);
					float anchorTouchY = event.getY(0);
					
					float alignTouchX = event.getX(1);
					float alignTouchY = event.getY(1);
										
					updateActiveToolAnchorPointToScreenCoordinates(anchorTouchX, anchorTouchY);
					updateActiveToolBasedOnMotionAtScreenCoordinates(alignTouchX, alignTouchY);	
				} else if (numPointers == 1) {
					
					updateActiveToolAnchorPointToScreenCoordinates(touchX, touchY);
					
					// single touch
				}
				
				
			} else if (mUIMode.equals(UIMode.ADDING_LINE)) {
				createOrContinueLineAtScreenCoordinates(touchX, touchY);
			} else if (mUIMode.equals(UIMode.ADDING_ANIMATED_INCISION)) {
				createOrContinueAnimatedIncisionAtScreenCoordinates(touchX, touchY);
			} else if (mUIMode.equals(UIMode.ADDING_POLYGON)) {
				createOrContinuePolygonAtScreenCoordinates(touchX, touchY);
			}
		
			return true;
		case (MotionEvent.ACTION_UP):
			//Log.d(TAG, "Action was UP");

			if (mWorkingPolygonAnnotationMemoryID != null) {
				// this means we were working on a new polygon annotation that has just finished being drawn 
				
				NetworkUtil.sendCreatedAnnotation(this, mWorkingPolygonAnnotationMemoryID, AnnotationState.getInstance().getAnnotations().get(mWorkingPolygonAnnotationMemoryID));
				
				mWorkingPolygonAnnotationMemoryID = null;
			}
			
		
			if (mWorkingPolylineAnnotationMemoryID != null) {
				AnnotationState.getInstance().recordReferenceAnnotationIllustrations(mWorkingPolylineAnnotationMemoryID);
				
				// this means we were working on a new polyline annotation that has just finished being drawn
				NetworkUtil.sendCreatedAnnotation(this, mWorkingPolylineAnnotationMemoryID, AnnotationState.getInstance().getAnnotations().get(mWorkingPolylineAnnotationMemoryID));
				
				mWorkingPolylineAnnotationMemoryID = null;
			}	
			
			if (mWorkingAnimatedIncisionAnnotationMemoryID != null) {
				
				// this means we were working on a new polyline annotation that has just finished being drawn
				NetworkUtil.sendCreatedAnnotation(this, mWorkingAnimatedIncisionAnnotationMemoryID, AnnotationState.getInstance().getAnnotations().get(mWorkingAnimatedIncisionAnnotationMemoryID));
				
				mWorkingAnimatedIncisionAnnotationMemoryID = null;
			}	
			
			if (mUIMode.equals(UIMode.ADDING_TOOL) || mUIMode.equals(UIMode.EDIT_MODE) && mWorkingToolAnnotationMemoryID != null) {
				NetworkUtil.sendUpdatedAnnotation(this, mWorkingToolAnnotationMemoryID, AnnotationState.getInstance().getAnnotations().get(mWorkingToolAnnotationMemoryID));
			}
			
			if (mUIMode.equals(UIMode.ADDING_TOOL)) {
				onSelectedTool(null);
			}
			
			if (mUIMode.equals(UIMode.ADDING_MULTI_POINTS)) {
				createOrContinueMultiPointAtScreenCoordinates(touchX, touchY);
			} else {
				updateMode(UIMode.EDIT_MODE);
			}
			
			return true;
		case (MotionEvent.ACTION_CANCEL):
			Log.d(TAG, "Action was CANCEL");
			return true;
		case (MotionEvent.ACTION_OUTSIDE):
			Log.d(TAG, "Movement occurred outside bounds "
					+ "of current screen element");
			return true;
		default:
			return super.onTouchEvent(event);
		}
		
		
	}
	
	private void selectToolAnnotationAtScreenCoordinates(float touchX,
			float touchY) {
		Log.d(TAG, "selectToolAnnotationAtScreenCoordinates");
		
		mRenderer.onRequestSelectionAt(new Point(touchX, touchY));		
	}
	
	private void updateActiveToolAnchorPointToScreenCoordinates(float anchorTouchX, float anchorTouchY) {
		AnnotationState.getInstance().updateToolAnchorPoint(mWorkingToolAnnotationMemoryID, new Point(anchorTouchX, anchorTouchY));
	}
	
	private void updateActiveToolBasedOnMotionAtScreenCoordinates(float touchX,
			float touchY) {
		if (mWorkingToolAnnotationMemoryID != null) {
			
			AnnotationMemory workingToolAnnotationMemory = AnnotationState.getInstance().getAnnotations().get(mWorkingToolAnnotationMemoryID);
			
			if (workingToolAnnotationMemory != null) {
				Annotation initialAnnotation = workingToolAnnotationMemory.getInitialAnnotation();
				if (initialAnnotation instanceof ToolAnnotation) {
					ToolAnnotation initialToolAnnotation = (ToolAnnotation) initialAnnotation;
					Point toolPoint = initialToolAnnotation.getPointInScreenSpace();
					
					double dx = touchX - toolPoint.x;
					double dy = toolPoint.y - touchY;
					
					double radians = Math.atan2(dy, dx);
					
					double degrees = STARUtils.radiansToDegrees(radians);
					
					initialToolAnnotation.setDegrees(degrees);
					
					double distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
					
					double scale = Math.max(distance / ScreenState.getInstance().getScreenWidth(), Pref.getInstance().getMinimumToolScale());
					
					initialToolAnnotation.setScale(scale);
					
				} else {
					Log.e(TAG, "annotation is not a tool annotation");
				}
			} else {
				Log.e(TAG, "cannot update tool annotation when annotation memory is null");
			}
		} else {
			Log.e(TAG, "cannot update tool annotation when ID is null");
		}
	}

	private void createPointAtScreenCoordinates(float touchX, float touchY) {
		Log.d(TAG, "createToolAtScreenCoordinates");
		Integer createdPointAnnotationId = AnnotationState.getInstance().addPointWithScreenCoordinates(this, touchX, touchY);
		
		NetworkUtil.sendCreatedAnnotation(this, createdPointAnnotationId, AnnotationState.getInstance().getAnnotations().get(createdPointAnnotationId));
	}

	private void createToolAtScreenCoordinates(ToolType toolType, float touchX, float touchY) {
		Log.d(TAG, "createToolAtScreenCoordinates");
		mWorkingToolAnnotationMemoryID = AnnotationState.getInstance().addToolWithScreenCoordinatesRotationAndScale(this, toolType, touchX, touchY, 0, Pref.getInstance().getDefaultToolScale());
		updateToolSelection(mWorkingToolAnnotationMemoryID, true);
		
		NetworkUtil.sendCreatedAnnotation(this, mWorkingToolAnnotationMemoryID, AnnotationState.getInstance().getAnnotations().get(mWorkingToolAnnotationMemoryID));
	}
	
	private void createOrContinueMultiPointAtScreenCoordinates(float touchX, float touchY) {
		Log.d(TAG, "createOrContinueMultiPointAtScreenCoordinates");
		
		boolean newlyCreatedMultiPointAnnotation = false;
		
		if (mWorkingMultiPointAnnotationMemoryID == null) {
			newlyCreatedMultiPointAnnotation = true;
			mWorkingMultiPointAnnotationMemoryID = AnnotationState.getInstance().initializeNewMultiPointAnnotation(this);
		}
		
		AnnotationMemory workingMultiPointAnnotationMemory = AnnotationState.getInstance().getAnnotations().get(mWorkingMultiPointAnnotationMemoryID);
		if (workingMultiPointAnnotationMemory != null) {
			MultiPointAnnotation workingMultiPointAnnotation = (MultiPointAnnotation) workingMultiPointAnnotationMemory.getInitialAnnotation();
			// TODO need to update the whole annotation with the current relevant keypoints/descriptors from the current frame
			workingMultiPointAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
			AnnotationState.getInstance().updateAnnotationMemoryWithFrameData(workingMultiPointAnnotationMemory);	
		}
		
		if (newlyCreatedMultiPointAnnotation) {
			NetworkUtil.sendCreatedAnnotation(this, mWorkingMultiPointAnnotationMemoryID, AnnotationState.getInstance().getAnnotations().get(mWorkingMultiPointAnnotationMemoryID));
		}
	}

	private void createOrContinuePolygonAtScreenCoordinates(float touchX, float touchY) {
		Log.d(TAG, "createOrContinuePolygonAtScreenCoordinates");
		if (mWorkingPolygonAnnotationMemoryID == null) {
			
			mWorkingPolygonAnnotationMemoryID = AnnotationState.getInstance().initializeNewPolygonAnnotation(this);
		}
		
		AnnotationMemory workingPolygonAnnotationMemory = AnnotationState.getInstance().getAnnotations().get(mWorkingPolygonAnnotationMemoryID);
		if (workingPolygonAnnotationMemory != null) {
			PolygonAnnotation workingPolygonAnnotation = (PolygonAnnotation) workingPolygonAnnotationMemory.getInitialAnnotation();
			// TODO need to update the whole annotation with the current relevant keypoints/descriptors from the current frame
			workingPolygonAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
			AnnotationState.getInstance().updateAnnotationMemoryWithFrameData(workingPolygonAnnotationMemory);	
		}
		
	}
	
	private void createOrContinueAnimatedIncisionAtScreenCoordinates(float touchX, float touchY) {
		Log.d(TAG, "createOrContinueAnimatedIncisionAtScreenCoordinates");
		if (mWorkingAnimatedIncisionAnnotationMemoryID == null) {
			mWorkingAnimatedIncisionAnnotationMemoryID = AnnotationState.getInstance().initializeNewAnimatedIncisionAnnotation(this);
		}
		
		AnnotationMemory workingAnimatedIncisionAnnotationMemory = AnnotationState.getInstance().getAnnotations().get(mWorkingAnimatedIncisionAnnotationMemoryID);
		if (workingAnimatedIncisionAnnotationMemory != null) {
			AnimatedIncisionAnnotation workingAnimatedIncisionAnnotation = (AnimatedIncisionAnnotation) workingAnimatedIncisionAnnotationMemory.getInitialAnnotation();
			workingAnimatedIncisionAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
			AnnotationState.getInstance().updateAnnotationMemoryWithFrameData(workingAnimatedIncisionAnnotationMemory);	
		}
	}
	
	private void createOrContinueLineAtScreenCoordinates(float touchX, float touchY) {
		Log.d(TAG, "createOrContinueLineAtScreenCoordinates");
		if (mWorkingPolylineAnnotationMemoryID == null) {
			
			mWorkingPolylineAnnotationMemoryID = AnnotationState.getInstance().initializeNewPolylineAnnotation(this);
		}
		
		AnnotationMemory workingPolylineAnnotationMemory = AnnotationState.getInstance().getAnnotations().get(mWorkingPolylineAnnotationMemoryID);
		if (workingPolylineAnnotationMemory != null) {
			PolylineAnnotation workingPolylineAnnotation = (PolylineAnnotation) workingPolylineAnnotationMemory.getInitialAnnotation();
			workingPolylineAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
			AnnotationState.getInstance().updateAnnotationMemoryWithFrameData(workingPolylineAnnotationMemory);	
		}
	}

	private void addOverlayView() {
		Log.d(TAG, "addOverlayView");
		
		if (PeerConnection.getInstance().getOwnUserType().isMentor() || Pref.getInstance().isUsingMockConnection()) {
			Log.d(TAG, "user is a mentor, give the mentor UI");
			LayoutInflater inflater = LayoutInflater.from(this);
			mUILayout = (RelativeLayout) inflater.inflate(R.layout.camera_overlay_mentor, null, false);
			
			mUILayout.setVisibility(View.VISIBLE);
			
			addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			
			mUILayout.bringToFront();
			
			mUILayout.setBackgroundColor(Color.TRANSPARENT);
			
			mAddPointAnnotationButton = setupButtonToAddGeometry(R.id.add_point_annotation_button, UIMode.ADDING_POINTS);
			
			mAddMultiPointAnnotationButton = setupButtonToAddGeometry(R.id.add_multi_point_annotation_button, UIMode.ADDING_MULTI_POINTS);
			
			mAddLineAnnotationButton = setupButtonToAddGeometry(R.id.add_line_annotation_button, UIMode.ADDING_LINE);
			
			mAddPolygonAnnotationButton = setupButtonToAddGeometry(R.id.add_polygon_annotation_button, UIMode.ADDING_POLYGON);
			
			mAddAnimatedIncisionAnnotationButton = setupButtonToAddGeometry(R.id.add_animated_incision_annotation_button, UIMode.ADDING_ANIMATED_INCISION);
			
			
			
			
			//SurfaceView streamingSurfaceView = (SurfaceView) findViewById(R.id.streaming_surface_view);
			//StreamingVideoManager.getInstance().registerSurface(streamingSurfaceView);
			
			
			
			
			
			
			// --- tools ---
			
			GridLayout toolsGridLayout = (GridLayout) mUILayout.findViewById(R.id.tools_grid);
			
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.BVM, R.string.bvm, R.drawable.bvm_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.ETTUBE, R.string.ettube, R.drawable.ettube_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.HEMOSTAT, R.string.hemostat, R.drawable.hemostat_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.IODINESWAB, R.string.iodineswab, R.drawable.iodineswab_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.LONGHOOK, R.string.longhook, R.drawable.longhook_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.RETRACTOR, R.string.retractor, R.drawable.retractor_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SCALPEL, R.string.scalpel, R.drawable.scalpel_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SCISSORS, R.string.scissors, R.drawable.scissors_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.STETHOSCOPE, R.string.stethoscope, R.drawable.stethoscope_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SURGICALTAPE, R.string.surgicaltape, R.drawable.surgicaltape_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SYRINGE, R.string.syringe, R.drawable.syringe_shadow, Constants.ICON_ROTATION_TOOL));
			mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.TWEEZERS, R.string.tweezers, R.drawable.tweezers_shadow, Constants.ICON_ROTATION_TOOL));
						
			// --- labels ---
			
			GridLayout labelsGridLayout = (GridLayout) mUILayout.findViewById(R.id.labels_grid);
			
			mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_CLOSE, R.string.blank, R.drawable.text_close_shadow, Constants.ICON_ROTATION_TEXT));
			mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_INCISION, R.string.blank, R.drawable.text_incision_shadow, Constants.ICON_ROTATION_TEXT));
			mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_PALPATION, R.string.blank, R.drawable.text_palpation_shadow, Constants.ICON_ROTATION_TEXT));
			mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_REMOVE, R.string.blank, R.drawable.text_remove_shadow, Constants.ICON_ROTATION_TEXT));
			mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_STITCH, R.string.blank, R.drawable.text_stitch_shadow, Constants.ICON_ROTATION_TEXT));			
			
			// --- hands ---
			
			GridLayout handsGridLayout = (GridLayout) mUILayout.findViewById(R.id.hands_grid);
			
			mButtonsToAddTool.add(new ButtonToAddTool(this, handsGridLayout, ToolType.HAND_PALPATE, R.string.hand_palpate, R.drawable.hand_palpate_shadow, Constants.ICON_ROTATION_HAND));
			mButtonsToAddTool.add(new ButtonToAddTool(this, handsGridLayout, ToolType.HAND_POINT, R.string.hand_point, R.drawable.hand_point_shadow, Constants.ICON_ROTATION_HAND));
			mButtonsToAddTool.add(new ButtonToAddTool(this, handsGridLayout, ToolType.HAND_STRETCH, R.string.hand_stretch, R.drawable.hand_stretch_shadow, Constants.ICON_ROTATION_HAND));
			
			
			mSaveAnnotationsButton = (Button) mUILayout.findViewById(R.id.save_annotations_button);
			mSaveAnnotationsButton.setOnClickListener(this);
			
			mLoadAnnotationsButton = (Button) mUILayout.findViewById(R.id.load_annotations_button);
			mLoadAnnotationsButton.setOnClickListener(this);
			
			mRemoveSelectedAnnotationButton = (Button) mUILayout.findViewById(R.id.remove_selected_annotation_button);
			mRemoveSelectedAnnotationButton.setOnClickListener(this);
			
			mClearAnnotationsButton = (Button) mUILayout.findViewById(R.id.clear_annotations_button);
			mClearAnnotationsButton.setOnClickListener(this);
			
			mRequestStartSendingFramesButton = (Button) mUILayout.findViewById(R.id.request_start_sending_frames_button);
			mRequestStartSendingFramesButton.setOnClickListener(this);
			
			mSendAnnotationsButton = (Button) mUILayout.findViewById(R.id.send_annotation_state_button);
			mSendAnnotationsButton.setOnClickListener(this);
			
			mMoveAllAnnotationsLeftButton = (Button) mUILayout.findViewById(R.id.move_all_left_button);
			mMoveAllAnnotationsLeftButton.setOnClickListener(this);
			
			mMoveAllAnnotationsRightButton = (Button) mUILayout.findViewById(R.id.move_all_right_button);
			mMoveAllAnnotationsRightButton.setOnClickListener(this);
			
			mMoveAllAnnotationsUpButton = (Button) mUILayout.findViewById(R.id.move_all_up_button);
			mMoveAllAnnotationsUpButton.setOnClickListener(this);
			
			mMoveAllAnnotationsDownButton = (Button) mUILayout.findViewById(R.id.move_all_down_button);
			mMoveAllAnnotationsDownButton.setOnClickListener(this);
			
			mResetAnnotationTranslateButton = (Button) mUILayout.findViewById(R.id.reset_offset_button);
			mResetAnnotationTranslateButton.setOnClickListener(this);
			
			mToggleAnchoringButton = (Button) mUILayout.findViewById(R.id.settings_button);
			mToggleAnchoringButton.setOnClickListener(this);
			
			mPreviewImageView = (ImageView) mUILayout.findViewById(R.id.previewImageView);
			
			mEngineerTextView = (TextView) mUILayout.findViewById(R.id.engineer_textview);
			if (mEngineerTextView != null) {
				if (Pref.getInstance().isUsingEngineerView()) {
					mEngineerTextView.setVisibility(View.VISIBLE);	
					mPreviewImageView.setVisibility(View.VISIBLE);
				} else {
					mEngineerTextView.setVisibility(View.GONE);
					mPreviewImageView.setVisibility(View.GONE);
				}	
			}
			
			mCaptureFrameMatchButton = (Button) mUILayout.findViewById(R.id.capture_frame_match_button);
			mCaptureFrameMatchButton.setOnClickListener(this);
			if (Pref.getInstance().isSavingAnnotationReferenceFrame()){
				mCaptureFrameMatchButton.setVisibility(View.VISIBLE);
			} else {
				mCaptureFrameMatchButton.setVisibility(View.GONE);
			}
			
			
			updateMode(UIMode.RECEIVING_FRAMES);
		} else {
			Log.d(TAG, "user is a trainee");
			
			LayoutInflater inflater = LayoutInflater.from(this);
			mUILayout = (RelativeLayout) inflater.inflate(R.layout.camera_overlay_trainee, null, false);
			
			mUILayout.setVisibility(View.VISIBLE);
			
			addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			
			View prevStepsContainers = findViewById(R.id.previous_steps_container);
			if (PeerConnection.getInstance().getOwnUserType().isTrainee() && Pref.getInstance().isShowingPreviousSteps()) {
				prevStepsContainers.setVisibility(View.VISIBLE);
				
				Button prevButton = (Button) findViewById(R.id.prev_step_button);
				Button nextButton = (Button) findViewById(R.id.next_step_button);
				ImageView stepImageView = (ImageView) findViewById(R.id.previous_step_imageview);
				
				PreviousStepsState.getInstance().registerUI(this, prevButton, nextButton, stepImageView);
				
			} else {
				prevStepsContainers.setVisibility(View.GONE);
			}
			
			
			
			
			
			
			
			mUILayout.bringToFront();
			
			mUILayout.setBackgroundColor(Color.TRANSPARENT);
			
			updateMode(UIMode.RECEIVING_FRAMES);
		}
		
		final TextView titlebarTextView = (TextView) this.findViewById(R.id.titlebar_text);
		if (titlebarTextView != null) {
	
			if (PeerConnection.getInstance().getOwnUserType() == UserType.MENTOR) {
				titlebarTextView.setText(R.string.titlebar_mentor);
				titlebarTextView.setTextColor(Color.RED);
			} else if (PeerConnection.getInstance().getOwnUserType() == UserType.TRAINEE) {
				titlebarTextView.setText(R.string.titlebar_blank);	// we don't want anything appearing on the trainee tablet to obstruct recorded images
				titlebarTextView.setTextColor(Color.GREEN);
			}
	
		}
		
	}


	private ToggleButton setupButtonToAddGeometry(int buttonResourceId, final UIMode uiMode) {
		ToggleButton toggleButton = (ToggleButton) mUILayout.findViewById(buttonResourceId);
		toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				onToggled(isChecked, uiMode);
			}
		});
		return toggleButton;
	}
	
	@Override
	public void onClick(View v) {
		Log.d(TAG, "onClick");
		
		if (v == mClearAnnotationsButton) {
			Log.d(TAG, "pressed clear annotations button");

			updateMode(UIMode.EDIT_MODE);
			
			onSelectedTool(null);
			
			AnnotationState.getInstance().clearAnnotations(this);
		} else if (v == mToggleAnchoringButton) {
			Log.d(TAG, "pressed toggle anchoring button");
        	
			boolean isOnlyDrawingInitialAnnotation = Pref.getInstance().isOnlyDrawingInitialAnnotation();
			Pref.getInstance().setOnlyDrawingInitialAnnotation(!isOnlyDrawingInitialAnnotation);
			
			Toaster.getInstance().toast("set isOnlyDrawingInitialAnnotation to " + Pref.getInstance().isOnlyDrawingInitialAnnotation());
			
		} else if (v == mRequestStartSendingFramesButton) {
			Log.d(TAG, "pressed request start-sending-frames button");
			
			mShouldRenderNewFrames = true;
			
		} else if (v == mSendAnnotationsButton) {
			Log.d(TAG, "pressed send annotations button");
			
			NetworkUtil.sendAnnotations(this);
			
			mShouldRenderNewFrames = true;
			updateMode(UIMode.RECEIVING_FRAMES);
		} else if (v == mRemoveSelectedAnnotationButton) {
			Log.d(TAG, "pressed remove selected annotation button");
			
			if (mWorkingToolAnnotationMemoryID != null) {
				updateToolSelection(mWorkingToolAnnotationMemoryID, false);
				AnnotationState.getInstance().removeAnnotationWithID(this, mWorkingToolAnnotationMemoryID);
				mWorkingToolAnnotationMemoryID = null;
			}
		} else if (v == mCaptureFrameMatchButton) {
			Log.d(TAG, "pressed capture frame match button");
			
			Toaster.getInstance().toast("capturing frame matches");
			
			AnnotationState.getInstance().captureFrameMatches();
		}  else if (v == mSaveAnnotationsButton) {
			Log.d(TAG, "pressed save annotations button");
			
			Toaster.getInstance().toast("saving annotations...");
			
			FullAnnotationSave.saveAnnotationStateToFile();
		} else if (v == mLoadAnnotationsButton) {
			Log.d(TAG, "pressed load annotations button");
			
			
			// Create the dialog.
    		FileChooserDialog dialog = new FileChooserDialog(MainActivity.this);
    		
    		// Assign listener for the select event.
    		dialog.addListener(MainActivity.this.onFileSelectedListener);
    		
    		// Define start folder.
    		//dialog.loadFolder(Environment.getExternalStorageDirectory() + "/");
    		
    		// Show the dialog.
            dialog.show();
		} else if (v == mMoveAllAnnotationsLeftButton) {

			Point p = new Point(-10, 0);
			
			Pref.getInstance().offsetGlobalAnnotationTranslate(p);
			AnnotationState.getInstance().translateAllAnnotations(p);
			
		} else if (v == mMoveAllAnnotationsRightButton) {
			
			Point p = new Point(10, 0);
			
			Pref.getInstance().offsetGlobalAnnotationTranslate(p);
			AnnotationState.getInstance().translateAllAnnotations(p);
			
		} else if (v == mMoveAllAnnotationsUpButton) {

			Point p = new Point(0,-10);
			
			Pref.getInstance().offsetGlobalAnnotationTranslate(p);
			AnnotationState.getInstance().translateAllAnnotations(p);
			
		} else if (v == mMoveAllAnnotationsDownButton) {
			
			Point p = new Point(0,10);
			
			Pref.getInstance().offsetGlobalAnnotationTranslate(p);
			AnnotationState.getInstance().translateAllAnnotations(p);

		} else if (v == mResetAnnotationTranslateButton) {
			
			Point negativeP = Pref.getInstance().getGlobalAnnotationTranslate();
			Point p = new Point(-negativeP.x, -negativeP.y);
			
			AnnotationState.getInstance().translateAllAnnotations(p);
			
			Pref.getInstance().resetGlobalAnnotationTranslate();
		}
	}

	private FileChooserDialog.OnFileSelectedListener onFileSelectedListener = new FileChooserDialog.OnFileSelectedListener() {
		public void onFileSelected(Dialog source, File file) {
			
			boolean success = false;
			
			try {
				
				BufferedReader reader = new BufferedReader( new FileReader (file));
			    String         line = null;
			    StringBuilder  stringBuilder = new StringBuilder();
			    String         ls = System.getProperty("line.separator");

			    while( ( line = reader.readLine() ) != null ) {
			        stringBuilder.append( line );
			        stringBuilder.append( ls );
			    }
			    
			    String annotationStateString = stringBuilder.toString();
			    
			    JSONObject annotationStateJson;
				
				annotationStateJson = new JSONObject(annotationStateString);
				
				AnnotationState.getInstance().updateFromJSONObject(annotationStateJson);
				AnnotationState.getInstance().translateAllAnnotations(Pref.getInstance().getGlobalAnnotationTranslate());
				
				success = true;
			} catch (Exception e) {
				e.printStackTrace();
				
				success = false;
				
				Toaster.getInstance().toast("error: " + e.getMessage());
			}
			
			if (success) {
				source.hide();
				Toaster.getInstance().toast("loaded annotations from file " + file.getAbsolutePath());
			}
		}
		public void onFileSelected(Dialog source, File folder, String name) {
			source.hide();
			Toast toast = Toast.makeText(MainActivity.this, "File created: " + folder.getName() + "/" + name, Toast.LENGTH_LONG);
			toast.show();
		}
	};

	protected void onToolToggled(boolean isChecked, ToolType toolType) {
		if (isChecked) {
			mActiveToolType = toolType;
			updateMode(UIMode.ADDING_TOOL);
		} else if (mActiveToolType == toolType) {
			updateMode(UIMode.EDIT_MODE);
		}
	}
	
	void onToggled(boolean isChecked, UIMode uiMode) {
		if (isChecked) {
			updateMode(uiMode);
		} else if (mUIMode == uiMode) {
			updateMode(UIMode.EDIT_MODE);
		}
	}
	
	private void updateMode(UIMode uiMode) {
		mUIMode = uiMode;
		updateAllModes();
	}
	
	private void updateAllModes() {
		
		if (mUIMode != UIMode.RECEIVING_FRAMES) {
			mShouldRenderNewFrames = false;
		}
		
		if (mUIMode != UIMode.ADDING_MULTI_POINTS) {
			mWorkingMultiPointAnnotationMemoryID = null;
		}
		
		if (mUIMode != UIMode.ADDING_POLYGON) {
			mWorkingPolygonAnnotationMemoryID = null;
		}
		
		if (mUIMode != UIMode.ADDING_LINE) {
			mWorkingPolylineAnnotationMemoryID = null;
		}
		
		if (mUIMode != UIMode.ADDING_ANIMATED_INCISION) {
			mWorkingAnimatedIncisionAnnotationMemoryID = null;
		}
		
		if (mUIMode != UIMode.ADDING_TOOL) {
			mActiveToolType = null;
		}
		
		if (mAddPolygonAnnotationButton != null) {
			mAddPolygonAnnotationButton.setChecked(mUIMode == UIMode.ADDING_POLYGON);	
		}
		
		if (mAddPointAnnotationButton != null) {
			mAddPointAnnotationButton.setChecked(mUIMode == UIMode.ADDING_POINTS);	
		}
		
		if (mAddMultiPointAnnotationButton != null) {
			mAddMultiPointAnnotationButton.setChecked(mUIMode == UIMode.ADDING_MULTI_POINTS);	
		}
		
		if (mAddLineAnnotationButton != null) {
			mAddLineAnnotationButton.setChecked(mUIMode == UIMode.ADDING_LINE);	
		}
		
		if (mAddAnimatedIncisionAnnotationButton != null) {
			mAddAnimatedIncisionAnnotationButton.setChecked(mUIMode == UIMode.ADDING_ANIMATED_INCISION);	
		}
		
		if (mButtonsToAddTool != null) {
			for (ButtonToAddTool buttonToAddTool : mButtonsToAddTool) {
				buttonToAddTool.setCheckedState();
			}
		}
		
	}
	
	public void updatePreviewImage(Bitmap bitmap) {
		if (mPreviewImageView != null) {
			mPreviewImageView.setImageBitmap(bitmap);
		} else {
			Log.e(TAG, "preview imageview is null");
		}
	}
	
	public VideoSource getVideoSource() {
		return mVideoSource;
	}
	
	public GLSurfaceView getGLSurfaceView() {
		return mGlSurfaceView;
	}

	public void onNewFrameBitmapFromSocket(Bitmap bitmap) {
		//Log.d(TAG, "onNewFrameBitmapFromSocket");
		if (mShouldRenderNewFrames) {
			// TODO Auto-generated method stub
			if (mRenderer.getCameraViewBackground() instanceof Texture2DVideo) {
				Texture2DVideo texture2DVideo = (Texture2DVideo) mRenderer.getCameraViewBackground();
				
				// setting the frame bitmap for our texture2dvideo
				texture2DVideo.setFrameBitmap(bitmap);
			} else {
				Log.e(TAG, "this cameraviewbackground can't do anything with this bitmap frame");
			}
			
			this.mRenderer.onNewFrameBitmapFromSocket(bitmap);
		}	
	}

	private void updateToolSelection(Integer toolAnnotationMemoryID, boolean selected) {
		if (toolAnnotationMemoryID != null) {
			AnnotationMemory selectedAnnotation = AnnotationState.getInstance().getAnnotations().get(toolAnnotationMemoryID);
			if (selectedAnnotation != null) {
				Annotation initial = selectedAnnotation.getInitialAnnotation();
				if (initial != null && initial instanceof ToolAnnotation) {
					((ToolAnnotation) initial).setSelected(selected);
				}
				
				Annotation current = selectedAnnotation.getCurrentAnnotation();
				if (current != null && current instanceof ToolAnnotation) {
					((ToolAnnotation) current).setSelected(selected);
				}
			}
		}
	}
	
	public void onSelectedTool(Integer toolID) {
		updateToolSelection(mWorkingToolAnnotationMemoryID, false);
		mWorkingToolAnnotationMemoryID = toolID;
		updateToolSelection(mWorkingToolAnnotationMemoryID, true);
		
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mRemoveSelectedAnnotationButton != null) {
					mRemoveSelectedAnnotationButton.setEnabled(mWorkingToolAnnotationMemoryID != null);
				}
			}
		});
	}
	
	RelativeLayout getUILayout() {
		return mUILayout;
	}

	public UIMode getUIMode() {
		return mUIMode;
	}

	public ToolType getActiveToolType() {
		return mActiveToolType;
	}
	
	private void updateEngineerTextView() {
		if (mEngineerTextView != null && mEngineerViewData != null) {
			mEngineerTextView.setText(mEngineerViewData.getText());
		}
	}

	public void onFPSUpdated(long fps, long elapsedMillis) {
		if (mEngineerViewData != null) {
			mEngineerViewData.setFPS((int) fps);
			mEngineerViewData.setMillisSinceLastFrame((int) elapsedMillis);
			
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					updateEngineerTextView();
				}
			});
		}
	}

	public void onNumKeyPointsUpdated(int numKeyPoints) {
		if (mEngineerViewData != null) {
			mEngineerViewData.setNumKeyPoints(numKeyPoints);
			
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					updateEngineerTextView();
				}
			});
		}
	}

	public STARRenderer getRenderer() {
		return mRenderer;
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		if (mShouldRenderNewFrames) {
			if (mRenderer.getPointOverlay() != null) {
				mRenderer.getPointOverlay().onPreviewFrame(data, camera);
			}
		}
		camera.addCallbackBuffer(data);
	}

	public DataServer getDataServer() {
		return mDataServer;
	}
	
	public FrameClient getFrameClient() {
		return mFrameClient;
	}
}
