package edu.purdue.andersed.star;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.Matches;

public class SaveAnnotationMatchFrameAsyncTask extends SaveAnnotatedFrameToFileAsyncTask{
	
	private static final int GRID_NUM_ROWS = 5;
	private static final int GRID_NUM_COLS = 5;
	
	private static final Scalar GRID_COLOR = new Scalar(255, 0, 0, 255);
	private static final Scalar TRANSFORMED_GRID_COLOR = new Scalar(0, 255, 0, 255);
	
	private static final Scalar INITIAL_ANNOTATION_COLOR = new Scalar(255, 0, 0, 255);
	private static final Scalar CURRENT_ANNOTATION_COLOR = new Scalar(0, 255, 0, 255);
	
	@Override
	void doDrawing(MatOfKeyPoint currentFrameKeyPoints, AnnotationMemory annotationMemory, Mat imageOriginal) {
				
		Mat image = new Mat();
		imageOriginal.copyTo(image);
		
		this.drawRawFeatures(annotationMemory.getCurrentRawKeyPoints(), image, CURRENT_DESCRIPTOR_RECT_COLOR);
		this.drawDescriptors(currentFrameKeyPoints, image, CURRENT_DESCRIPTOR_RECT_COLOR);
		
		saveImage(image, "current_descriptors");
		
		imageOriginal.copyTo(image);
		
		this.drawDescriptors(annotationMemory.getInitialKeyPoints(), image, REFERENCE_DESCRIPTOR_RECT_COLOR);
		
		this.drawDescriptors(currentFrameKeyPoints, image, CURRENT_DESCRIPTOR_RECT_COLOR);
		
		Matches matches = annotationMemory.getMatches();
		
		this.drawLinesBetweenMatches(matches, image, REFERENCE_DESCRIPTOR_RECT_COLOR, CURRENT_DESCRIPTOR_RECT_COLOR);
		
		saveImage(image, "matches");
		
		Rect boundingRect = annotationMemory.getBoundingRect();
		
		Grid grid = new Grid(boundingRect, GRID_NUM_ROWS, GRID_NUM_COLS);
		
		Mat homography = annotationMemory.getCurrentHomography();
		if (homography != null) {
			Grid transformedGrid = grid.getTransformedGrid(homography);
			
			imageOriginal.copyTo(image);
			
			this.drawGridIntersectionMatches(grid, transformedGrid, image, GRID_COLOR, TRANSFORMED_GRID_COLOR);
			
			this.drawGrid(grid, image, GRID_COLOR);
			this.drawGrid(transformedGrid, image, TRANSFORMED_GRID_COLOR);
			
			saveImage(image, "grid_transformed");
			
			imageOriginal.copyTo(image);
			
			this.drawAnnotation(annotationMemory.getInitialAnnotation(), image, INITIAL_ANNOTATION_COLOR);
			this.drawLinesBetweenAnnotationPoints(annotationMemory.getInitialAnnotation(), annotationMemory.getCurrentAnnotation(), image, INITIAL_ANNOTATION_COLOR, CURRENT_ANNOTATION_COLOR);
			this.drawAnnotation(annotationMemory.getCurrentAnnotation(), image, CURRENT_ANNOTATION_COLOR);
			
			saveImage(image, "annotation_transformed");
			
			imageOriginal.copyTo(image);
			
			this.drawAnnotation(annotationMemory.getInitialAnnotation(), image, INITIAL_ANNOTATION_COLOR);
			this.drawAnnotation(annotationMemory.getCurrentAnnotation(), image, ANNOTATION_POINT_COLOR);
			
			saveImage(image, "annotations_only");
			
		} else {
			Toaster.getInstance().toast("homography is null, could not save image showing transformation");
		}
	}



	private void drawGridCornerMatches(Grid grid, Grid transformedGrid,
			Mat image, Scalar gridColor, Scalar transformedGridColor) {
		List<Point> oldCornerPoints = new ArrayList<Point>();
		
		oldCornerPoints.add(grid.getRowStart(0));	// top left
		oldCornerPoints.add(grid.getRowEnd(0));	// top right
		oldCornerPoints.add(grid.getRowStart(grid.getNumRows()-1));	// bottom left
		oldCornerPoints.add(grid.getRowEnd(grid.getNumRows()-1));	// bottom right
		
		List<Point> newCornerPoints = new ArrayList<Point>();
		
		newCornerPoints.add(transformedGrid.getRowStart(0));	// top left
		newCornerPoints.add(transformedGrid.getRowEnd(0));	// top right
		newCornerPoints.add(transformedGrid.getRowStart(transformedGrid.getNumRows()-1));	// bottom left
		newCornerPoints.add(transformedGrid.getRowEnd(transformedGrid.getNumRows()-1));	// bottom right
		
		List<Float> dummyDistances = new ArrayList<Float>();
		for (int i = 0; i < oldCornerPoints.size(); i++) {
			dummyDistances.add(1.0f);
		}
		
		this.drawLinesBetweenMatches(new Matches(oldCornerPoints, newCornerPoints, dummyDistances), image, gridColor, transformedGridColor);
	}


	private void drawGridIntersectionMatches(Grid grid, Grid transformedGrid,
			Mat image, Scalar gridColor, Scalar transformedGridColor) {
		List<Point> oldCornerPoints = grid.getPointMat().toList();
		
		List<Point> newCornerPoints = transformedGrid.getPointMat().toList();
		
		List<Float> dummyDistances = new ArrayList<Float>();
		for (int i = 0; i < oldCornerPoints.size(); i++) {
			dummyDistances.add(1.0f);
		}
		
		this.drawLinesBetweenMatches(new Matches(oldCornerPoints, newCornerPoints, dummyDistances), image, gridColor, transformedGridColor);
	}


	private void drawLinesBetweenAnnotationPoints(Annotation oldAnnotation, Annotation newAnnotation, Mat image, Scalar oldColor, Scalar newColor) {
		MatOfPoint2f oldPointsMat = oldAnnotation.getPointsMat();
		MatOfPoint2f newPointsMat = newAnnotation.getPointsMat();
		
		List<Point> oldPoints = oldPointsMat.toList();
		List<Point> newPoints = newPointsMat.toList();
		
		List<Float> dummyDistances = new ArrayList<Float>();
		for (int i = 0; i < oldPoints.size(); i++) {
			dummyDistances.add(1.0f);
		}
		
		this.drawLinesBetweenMatches(new Matches(oldPoints, newPoints, dummyDistances), image, oldColor, newColor);
	}

	private void drawGridPerimeter(Grid grid, Mat image, Scalar color) {
		
		Point tl = grid.getRowStart(0);	// top left
		Point tr = grid.getRowEnd(0); // top right
		Point bl = grid.getRowStart(grid.getNumRows()-1); // bottom left
		Point br = grid.getRowEnd(grid.getNumRows()-1); // bottom left
		
		Core.line(image, 
				ScreenState.getInstance().openCVSpaceToScreenSpace(tl), 
				ScreenState.getInstance().openCVSpaceToScreenSpace(tr), 
				color,
				mLineThickness);
		
		Core.line(image, 
				ScreenState.getInstance().openCVSpaceToScreenSpace(bl), 
				ScreenState.getInstance().openCVSpaceToScreenSpace(br), 
				color,
				mLineThickness);
		
		Core.line(image, 
				ScreenState.getInstance().openCVSpaceToScreenSpace(tl), 
				ScreenState.getInstance().openCVSpaceToScreenSpace(bl), 
				color,
				mLineThickness);
		
		Core.line(image, 
				ScreenState.getInstance().openCVSpaceToScreenSpace(tr), 
				ScreenState.getInstance().openCVSpaceToScreenSpace(br), 
				color,
				mLineThickness);
	}
	
	private void drawGrid(Grid grid, Mat image, Scalar color) {
		for (int i = 0; i < grid.getNumCols(); i++) {
			Point colStart = grid.getColStart(i);
			Point colEnd = grid.getColEnd(i);
			
			Core.line(image, 
					ScreenState.getInstance().openCVSpaceToScreenSpace(colStart), 
					ScreenState.getInstance().openCVSpaceToScreenSpace(colEnd), 
					color,
					mLineThickness);
		}
		
		for (int i = 0; i < grid.getNumRows(); i++) {
			Point rowStart = grid.getRowStart(i);
			Point rowEnd = grid.getRowEnd(i);
			
			Core.line(image, 
					ScreenState.getInstance().openCVSpaceToScreenSpace(rowStart), 
					ScreenState.getInstance().openCVSpaceToScreenSpace(rowEnd), 
					color,
					mLineThickness);
		}
	}
	

	private class Grid {
		MatOfPoint2f mPointMat = new MatOfPoint2f();
		
		int mNumRows;
		int mNumCols;
		
		private Grid(MatOfPoint2f pointMat, int numRows, int numCols) {
			mPointMat = pointMat;
			mNumRows = numRows;
			mNumCols = numCols;
		}
		
		public Grid(Rect rect, int numRows, int numCols) {
			mNumRows = numRows;
			mNumCols = numCols;
			
			double hSpace = rect.width / (numCols-1);
			double vSpace = rect.height / (numRows-1);
			
			for (int i = 0; i < numRows; i++) {
				for (int j = 0; j < numCols; j++) {
					Point p = new Point(
							rect.tl().x + (j * hSpace),
							rect.tl().y + (i * vSpace)
							);
					mPointMat.push_back(new MatOfPoint2f(p));
				}
			}
		}
		
		public Point getRowStart(int rowIndex) {
			List<Point> points = mPointMat.toList();
			return points.get(rowIndex * mNumCols);
		}
		
		public Point getRowEnd(int rowIndex) {
			List<Point> points = mPointMat.toList();
			return points.get(rowIndex * mNumCols + (mNumCols - 1));
		}
		
		public Point getColStart(int colIndex) {
			List<Point> points = mPointMat.toList();
			return points.get(colIndex);
		}
		
		public Point getColEnd(int colIndex) {
			List<Point> points = mPointMat.toList();
			return points.get((mNumRows-1)*mNumCols + colIndex);
		}

		public int getNumRows() {
			return mNumRows;
		}

		public int getNumCols() {
			return mNumCols;
		}
		
		public Grid getTransformedGrid(Mat homography) {
			MatOfPoint2f transformedPointMat = new MatOfPoint2f();
			
			Core.perspectiveTransform(mPointMat, transformedPointMat, homography);
			
			return new Grid(transformedPointMat, mNumRows, mNumCols);
		}
		
		public MatOfPoint2f getPointMat() {
			return this.mPointMat;
		}
	}

}

