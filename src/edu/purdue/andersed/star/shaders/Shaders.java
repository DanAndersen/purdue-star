package edu.purdue.andersed.star.shaders;

public class Shaders {
	
	// used when coords are already defined in Normalized Device Coordinates
	public static final String PASS_THROUGH_VERTEX_SHADER =
		"attribute vec4 vertexPosition; \n" +
		"attribute vec2 vertexTexCoord; \n" +
		"varying vec2 texCoord; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_Position = vertexPosition; \n" +
		"    texCoord = vertexTexCoord; \n" +
		"} \n"
			;
	
	public static final String PASS_THROUGH_FRAGMENT_SHADER = 
		"precision mediump float; \n" + 
		"varying vec2 texCoord; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_FragColor = vec4(texCoord, 0.0, 1.0); \n" +
		//"    gl_FragColor = vec4(1.0, 1.0, 0.0, 0.5); \n" +
		"} \n"
			;
}
