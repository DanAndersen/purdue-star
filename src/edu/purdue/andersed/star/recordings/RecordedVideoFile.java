package edu.purdue.andersed.star.recordings;


public class RecordedVideoFile {

	private int mRawVideoId;
	private String mLabel;
	
	public RecordedVideoFile(int rawVideoId, String label) {
		this.mRawVideoId = rawVideoId;
		this.mLabel = label;
	}
	
	public int getRawVideoId() {
		return mRawVideoId;
	}
	
	public String getLabel() {
		return mLabel;
	}
}
