package edu.purdue.andersed.star.settings;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.opencv.core.Point;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;
import edu.purdue.andersed.star.R;
import edu.purdue.andersed.star.recordings.RecordedVideoFile;

public class Pref {
	
	private static final String TAG = "Pref";
	
	private static Pref instance;
	
	private Context mContext;
	private Resources mResources;
	private SharedPreferences mSharedPreferences;
	
	private Map<String, RecordedVideoFile> mRecordedVideoFiles;

	private Pref() {
	}
	
	public static Pref getInstance() {
		if (instance == null) {
			instance = new Pref();
		}
		return instance;
	}
	
	public void initialize(Context context) {
		Log.d(TAG, "initializing Pref with context " + context);
		mContext = context;
		mResources = mContext.getResources();
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		
		resetGlobalAnnotationTranslate();
	}


	private float getFloat(int keyResId, int keyDefaultId) {
		String key = mResources.getString(keyResId);
		String defaultValueString = mResources.getString(keyDefaultId);
		String resultString =  mSharedPreferences.getString(key, defaultValueString);
		float result = Float.parseFloat(resultString);
		return result;
	}
	
	private void setBoolean(int keyResId, boolean value) {
		String key = mResources.getString(keyResId);
		
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	private int getInt(int keyResId, int keyDefaultId) {
		String prefString = mSharedPreferences.getString(
				mResources.getString(keyResId), 
				mResources.getString(keyDefaultId));
		return Integer.valueOf(prefString);
	}
	
	private boolean getBoolean(int keyResId, int keyDefaultId) {
		return mSharedPreferences.getBoolean(
				mResources.getString(keyResId), 
				mResources.getBoolean(keyDefaultId));
	}
	
	private String getString(int keyResId, int keyDefaultId) {
		return mSharedPreferences.getString(
				mResources.getString(keyResId), 
				mResources.getString(keyDefaultId));
	}
	
	public boolean isShowingPreviousSteps() {
		return getBoolean(R.string.pref_showing_previous_steps_key, R.bool.pref_showing_previous_steps_default);
	}
	
	public boolean isEncodingDecodingFrames() {
		return getBoolean(R.string.pref_encoding_decoding_frames_key, R.bool.pref_encoding_decoding_frames_default);
	}
	
	public boolean isDebugMode() {
		return getBoolean(R.string.pref_debug_mode_key, R.bool.pref_debug_mode_default);
	}
	
	public int getCvImageShrinkFactor() {
		return getInt(R.string.pref_cv_image_shrink_factor_key, R.integer.pref_cv_image_shrink_factor_default);
	}
	
	public float getCvImageScaleFactor() {
		return 1.0f / getCvImageShrinkFactor();
	}
	
	public double getScaledAnnotationRegionPadding() {
		int paddingPixels = getInt(R.string.pref_annotation_region_padding_key, R.integer.pref_annotation_region_padding_default);
		double scaledPadding = paddingPixels * getCvImageScaleFactor();
		return scaledPadding;
	}

	public boolean isUsingGoodHomographyCheck() {
		return getBoolean(R.string.pref_good_homography_check_key, R.bool.pref_good_homography_check_default);
	}
	
	private boolean isNetworkingEnabled() {
		return getBoolean(R.string.pref_networking_enabled_key, R.bool.pref_networking_enabled_default);
	}
	
	public boolean isUsingMockConnection() {
		return !isNetworkingEnabled();
	}

	public int getVideoServerSocketPort() {
		return getInt(R.string.pref_video_server_socket_port_key, R.integer.pref_video_server_socket_port_default);
	}
	
	public int getDataServerSocketPort() {
		return getInt(R.string.pref_data_server_socket_port_key, R.integer.pref_data_server_socket_port_default);
	}

	public boolean isUsingDoubleBuffering() {
		return getBoolean(R.string.pref_using_double_buffering_key, R.bool.pref_using_double_buffering_default);
	}

	public double getMinimumToolScale() {
		Log.d(TAG, "getMinimumToolScale");
		double minimumToolScale = getFloat(R.string.pref_minimum_tool_scale_key, R.string.pref_minimum_tool_scale_default);
		Log.d(TAG, "minimumToolScale = " + minimumToolScale);
		return minimumToolScale;
	}

	public double getDefaultToolScale() {
		Log.d(TAG, "getDefaultToolScale");
		double defaultToolScale = getFloat(R.string.pref_default_tool_scale_key, R.string.pref_default_tool_scale_default);
		Log.d(TAG, "defaultToolScale = " + defaultToolScale);
		return defaultToolScale;
	}
	
	public float getToolTransparency() {
		return getFloat(R.string.pref_tool_transparency_key, R.string.pref_tool_transparency_default);
	}
	

	public float getAnnotationLineThickness() {
		return getInt(R.string.pref_annotation_line_thickness_key, R.integer.pref_annotation_line_thickness_default);
	}

	public int getSelectionRegionSize() {
		return getInt(R.string.pref_selection_region_size_key, R.integer.pref_selection_region_size_default);
	}

	public float getDefaultLineThickness() {
		return getInt(R.string.pref_default_line_thickness_key, R.integer.pref_default_line_thickness_default);
	}
	
	public int getCircleAnnotationRadiusPixels() {
		return getInt(R.string.pref_circle_annotation_radius_pixels_key, R.integer.pref_circle_annotation_radius_pixels_default);
	}
	
	public boolean isUsingAutoFocus() {
		return getBoolean(R.string.pref_using_auto_focus_key, R.bool.pref_using_auto_focus_default);
	}

	public boolean isUsingRecordedVideo() {
		return getBoolean(R.string.pref_using_recorded_video_key, R.bool.pref_using_recorded_video_default);
	}

	public boolean isUsingEngineerView() {
		return getBoolean(R.string.pref_using_engineer_view_key, R.bool.pref_using_engineer_view_default);
	}

	public RecordedVideoFile getRecordedVideoFile() {
		if (this.mRecordedVideoFiles == null) {
			mRecordedVideoFiles = new HashMap<String, RecordedVideoFile>();
			
			/*
			addVideoToMap(new ClosedRotateVideo());
			addVideoToMap(new ClosedStillVideo());
			addVideoToMap(new OpenedStill2VideoWithBorderTruth());
			addVideoToMap(new PigSkinVideo());
			addVideoToMap(new RecordedVideoFile(R.raw.dummy_static, "dummy_rotate"));
			addVideoToMap(new DummyTask1StaticVideo());
			addVideoToMap(new DummyIncisionRefVideo());
			addVideoToMap(new RecordedVideoFile(R.raw.dummy_zoom, "dummy_zoom"));
			addVideoToMap(new RecordedVideoFile(R.raw.dummy_translation, "dummy_translation"));
			addVideoToMap(new RecordedVideoFile(R.raw.dummy_rotation, "dummy_rotation"));
			addVideoToMap(new RecordedVideoFile(R.raw.dummy_occlusion_major, "dummy_occlusion_major"));
			addVideoToMap(new RecordedVideoFile(R.raw.dummy_occlusion_minor, "dummy_occlusion_minor"));
			addVideoToMap(new RecordedVideoFile(R.raw.dummy_deformation, "dummy_deformation"));
			addVideoToMap(new RecordedVideoFile(R.raw.poster_movement, "poster_movement"));
			addVideoToMap(new RecordedVideoFile(R.raw.poster_occlusion, "poster_occlusion"));
			addVideoToMap(new RecordedVideoFile(R.raw.task2_demonstration, "task2_demonstration"));
			*/
		}
		
		String recordedVideoFileLabel = getString(R.string.pref_recorded_video_file_key, R.string.pref_recorded_video_file_default);
		
		Iterator<Entry<String, RecordedVideoFile>> iter = mRecordedVideoFiles.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, RecordedVideoFile> pair = iter.next();
			String label = pair.getKey();
			if (label.equals(recordedVideoFileLabel)) {
				return pair.getValue();
			}
		}
		return null;
	}
	
	private void addVideoToMap(RecordedVideoFile recordedVideoFile) {
		mRecordedVideoFiles.put(recordedVideoFile.getLabel(), recordedVideoFile);
	}

	public boolean isDrawingKeyPoints() {
		return getBoolean(R.string.pref_drawing_key_points_key, R.bool.pref_drawing_key_points_default);
	}

	public boolean isDrawingPointMatching() {
		return getBoolean(R.string.pref_drawing_point_matching_key, R.bool.pref_drawing_point_matching_default);
	}
	
	public boolean isTracing() {
		return getBoolean(R.string.pref_tracing_key, R.bool.pref_tracing_default);
	}

	public boolean isDrawingToolAnchorPoint() {
		return getBoolean(R.string.pref_drawing_tool_anchor_point_key, R.bool.pref_drawing_tool_anchor_point_default);
	}

	public boolean isSavingFrameToBeProcessed() {
		return getBoolean(R.string.pref_saving_processed_frame_key, R.bool.pref_saving_processed_frame_default);
	}

	public boolean isSavingAnnotationReferenceFrame() {
		return getBoolean(R.string.pref_saving_annotation_reference_frame_key, R.bool.pref_saving_annotation_reference_frame_default);
	}

	public boolean isOnlyDrawingInitialAnnotation() {
		return getBoolean(R.string.pref_only_drawing_initial_annotation_key, R.bool.pref_only_drawing_initial_annotation_default);
	}
	
	public void setOnlyDrawingInitialAnnotation(boolean value) {
		setBoolean(R.string.pref_only_drawing_initial_annotation_key, value);
	}
	
	public boolean isTrackingEnabled() {
		return getBoolean(R.string.pref_is_tracking_enabled_key, R.bool.pref_is_tracking_enabled_default);
	}

	private Point mGlobalAnnotationTranslate;
	
	public Point getGlobalAnnotationTranslate() {
		return mGlobalAnnotationTranslate;
	}

	public void resetGlobalAnnotationTranslate() {
		mGlobalAnnotationTranslate = new Point(0,0);
	}

	public void offsetGlobalAnnotationTranslate(Point point) {
		mGlobalAnnotationTranslate.x += point.x;
		mGlobalAnnotationTranslate.y += point.y;
	}

	
}
