package edu.purdue.andersed.star.geometry;



public class Line extends NDCGeometry {
	
	public Line(float ndcX1, float ndcY1, float ndcX2, float ndcY2) {
		float[] VERTS = {
			ndcX1, ndcY1, MAX_Z,
			ndcX2, ndcY2, MAX_Z
		};
		
		setVerts(VERTS);
	}
	

}
