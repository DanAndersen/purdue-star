package edu.purdue.andersed.star.geometry;

import java.nio.Buffer;

import android.util.Log;

public class Quad extends NDCGeometry {

	private static final String TAG = "Quad";
	
	// define the quad in normalized device coordinates directly
		
	private static short[] INDICES = {0, 1, 2, 0, 2, 3};
	private static float[] TEX_COORDS = {
			0.f, 1.f,
			1.f, 1.f,
			1.f, 0.f,
			0.f, 0.f
	};
	
	private Buffer mTexCoordBuff;
	private Buffer mIndBuff;
	
	private int indicesNumber;

	private float mScaleX;

	private float mScaleY;

	private float mScaledOffsetFromCenterX;
	private float mScaledOffsetFromCenterY;
		
	// for taking an image of dimens width x height,
	// and fitting it so that it would fit inside a quad of a particular scale
	public Quad(float scaleOfMaxSide, float widthPixels, float heightPixels, float anchorPixelsFromCenterX, float anchorPixelsFromCenterY) {
		
		float scaleX, scaleY;

		scaleX = scaleOfMaxSide;
		scaleY = scaleOfMaxSide * (heightPixels / widthPixels);
		
		mScaledOffsetFromCenterX = -2.f * (anchorPixelsFromCenterX/widthPixels) * scaleX;
		mScaledOffsetFromCenterY = 2.f * (anchorPixelsFromCenterY/heightPixels) * scaleY;

		
		initWithScales(scaleX, scaleY);
	}
	
	public Quad(float scale) {
		initWithScales(scale, scale);
	}
	
	private void initWithScales(float scaleX, float scaleY) {
		Log.d(TAG, "initWithScales(" + scaleX + ", " + scaleY + ")");
		
		mScaleX = scaleX;
		mScaleY = scaleY;
		
		float[] VERTS = {
			(-1.f * scaleX), (-1.f * scaleY), MAX_Z,
			(1.f * scaleX), (-1.f * scaleY), MAX_Z,
			(1.f * scaleX), (1.f * scaleY), MAX_Z,
			(-1.f * scaleX), (1.f * scaleY), MAX_Z
		};
		
		setVerts(VERTS);
		setTexCoords();
		setIndices();
	}
	
	private void setTexCoords() {
		mTexCoordBuff = fillBuffer(TEX_COORDS);
	}

	private void setIndices() {
		mIndBuff = fillBuffer(INDICES);
		indicesNumber = INDICES.length;
	}

	public int getNumObjectIndex() {
		return indicesNumber;
	}

	public Buffer getIndices() {
		return mIndBuff;
	}

	public Buffer getTexCoords() {
		return mTexCoordBuff;
	}

	public float getScaledOffsetFromCenterX() {
		return mScaledOffsetFromCenterX;
	}

	public float getScaledOffsetFromCenterY() {
		return mScaledOffsetFromCenterY;
	}
	
	
}
