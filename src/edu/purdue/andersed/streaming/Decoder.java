package edu.purdue.andersed.streaming;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

public class Decoder {

	private DecoderAsyncTask mDecoderAsyncTask;

	private DecodeListener mListener;

	private int mDecodeWidth;

	private int mDecodeHeight;
	
	private static final String TAG = "Decoder";
	
	private Mat mRGBMatDecoded;
	
	private Queue<byte[]> mPacketsToDecode;
	
	private static Decoder instance = new Decoder();
	
	static 
    {
    	try
    	{ 
    		Log.d(TAG, "loading streaming_video_manager");
    		System.loadLibrary("decoder");
    	}
    	catch( UnsatisfiedLinkError e )
		{
           System.err.println("Native code library failed to load.\n" + e);
           throw new RuntimeException("unable to load JNI module.");
		}
    }
	
	private Decoder() {
		Log.d(TAG, "initializing Decoder");
		
		mDecoderAsyncTask = new DecoderAsyncTask();
		
	}
	
	public static Decoder getInstance() {
		if (instance == null) {
			instance = new Decoder();
		}
		return instance;
	}
	
	public void addPacketsToDecode(byte[] encodedBytes) {
		mPacketsToDecode.add(encodedBytes);
	}
	
	public void init(DecodeListener listener, int decodeWidth, int decodeHeight) {
		Log.d(TAG, "init");
		
		mListener = listener;
		
		mDecodeWidth = decodeWidth;
		mDecodeHeight = decodeHeight;
		
		nInitDecoder(mDecodeWidth, mDecodeHeight);
		
		mRGBMatDecoded = new Mat(decodeHeight, decodeWidth, CvType.CV_8UC3);
		mPacketsToDecode = new ConcurrentLinkedQueue<byte[]>();
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// this is needed, or else doInBackground() doesn't get called
			mDecoderAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);	
		} else {
			mDecoderAsyncTask.execute();
		}
	}
	
	public void destroy() {
		Log.d(TAG, "destroy");
		
		mDecoderAsyncTask.cancel(true);
		
		nDestroyDecoder();
	}
	
	private class DecoderAsyncTask extends AsyncTask<Void, Void, Void> {

		private static final String TAG = "DecoderAsyncTask";
		
		private boolean mDecoderRunning;
		
		@Override
		protected Void doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			
			mDecoderRunning = true;
			
			while (mDecoderRunning) {
				while (!mPacketsToDecode.isEmpty()) {
					byte[] packetToDecode = mPacketsToDecode.remove();
					
					Log.d(TAG, "going to nDecode");
					long start = System.currentTimeMillis();
					boolean gotFrame = nDecode(packetToDecode, mRGBMatDecoded.getNativeObjAddr());
					long end = System.currentTimeMillis();
					Log.d(TAG, "did nDecode, took " + (end-start) + " millis");
					
					if (gotFrame) {
						Log.d(TAG, "got a frame");
						
						if (mListener != null) {
							mListener.onReceivedDecodedFrame(mRGBMatDecoded);
						}
					}
				}
			}
			
			return null;
		}
		
	}
	
	private native void nInitDecoder(int width, int height);
	private native boolean nDecode(byte[] byteArrayToDecode, long matNativeObj);
	private native void nDestroyDecoder();
}
