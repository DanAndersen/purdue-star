#include <jni.h>
#include <stdio.h>
#include <android/log.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <iostream>

#include <GLES3/gl3.h>
#include <EGL/egl.h>

using namespace cv;

#define  LOG_TAG    "point_overlay_jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define CHECK_MAT(cond) if(!(cond)){ LOGI("FAILED: " #cond); return; }

#define DEBUG 1

#define BYTES_PER_PIXEL 4

typedef unsigned char uchar;

void Mat_to_vector_KeyPoint(Mat& mat, vector<KeyPoint>& v_kp);
void vector_KeyPoint_to_Mat(vector<KeyPoint>& v_kp, Mat& mat);

extern "C" {
    JNIEXPORT void JNICALL Java_edu_purdue_andersed_star_PointOverlay_doNativeFeatureDetection(JNIEnv * env, jobject obj, jlong imagePtr, jlong destinationMatOfKeyPointPtr);
    JNIEXPORT void JNICALL Java_edu_purdue_andersed_star_PointOverlay_doNativeGlReadPixels(JNIEnv * env, jobject obj, int current_frame_pbo_id, int previous_frame_pbo_id, int screenWidth, int screenHeight, int pbo_size, jlong workingImageMatPtr, int shrink_factor);
    JNIEXPORT int JNICALL Java_edu_purdue_andersed_star_PBOBuffer_doNativePBOSetup(JNIEnv * env, jobject obj, int pbo_size);
};

void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
    GLenum err = glGetError();
    if (err != GL_NO_ERROR)
    {
    	LOGI("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
        abort();
    }
}

#ifdef DEBUG
    #define GL_CHECK(stmt) do { \
            stmt; \
            CheckOpenGLError(#stmt, __FILE__, __LINE__); \
        } while (0)
#else
    #define GL_CHECK(stmt) stmt
#endif

JNIEXPORT void JNICALL Java_edu_purdue_andersed_star_PointOverlay_doNativeFeatureDetection(JNIEnv * env, jobject obj, jlong imagePtr, jlong destinationMatOfKeyPointPtr)
{
	LOGI( "Start doNativeFeatureDetection! \n");

	LOGI("detecting keypoints \n");
	int minHessian = 100;

	LOGI("creating featureDetector \n");
	SurfFeatureDetector featureDetector(minHessian);

	const Mat* image = (Mat*) imagePtr;

	Mat * matOfKeyPoint = (Mat *) destinationMatOfKeyPointPtr;
	vector<KeyPoint> vectorOfKeyPoint;


	Mat_to_vector_KeyPoint(*matOfKeyPoint, vectorOfKeyPoint);


	LOGI("starting featureDetector.detect \n");
	featureDetector.detect(*image, vectorOfKeyPoint);
	LOGI("ending featureDetector.detect \n");

	LOGI("num features: %d", vectorOfKeyPoint.size());

	vector_KeyPoint_to_Mat(vectorOfKeyPoint, *matOfKeyPoint);

	LOGI( "End doNativeFeatureDetection!\n");
}

// C++ / JNI
// vector_KeyPoint converters
// from opencv code: https://code.ros.org/trac/opencv/browser/trunk/opencv/modules/java/src/cpp/converters.cpp?rev=6308

void Mat_to_vector_KeyPoint(Mat& mat, vector<KeyPoint>& v_kp)
{
	LOGI("starting Mat_to_vector_KeyPoint \n");
    v_kp.clear();
    CHECK_MAT(mat.type()==CV_32FC(7) && mat.cols==1);
    for(int i=0; i<mat.rows; i++)
    {
        Vec<float, 7> v = mat.at< Vec<float, 7> >(i, 0);
        KeyPoint kp(v[0], v[1], v[2], v[3], v[4], (int)v[5], (int)v[6]);
        v_kp.push_back(kp);
    }
    LOGI("ending Mat_to_vector_KeyPoint \n");
    return;
}


void vector_KeyPoint_to_Mat(vector<KeyPoint>& v_kp, Mat& mat)
{
	LOGI("starting vector_KeyPoint_to_Mat \n");
    int count = (int)v_kp.size();
    mat.create(count, 1, CV_32FC(7));
    for(int i=0; i<count; i++)
    {
        KeyPoint kp = v_kp[i];
        mat.at< Vec<float, 7> >(i, 0) = Vec<float, 7>(kp.pt.x, kp.pt.y, kp.size, kp.angle, kp.response, (float)kp.octave, (float)kp.class_id);
    }
    LOGI("ending vector_KeyPoint_to_Mat \n");
}

JNIEXPORT void JNICALL Java_edu_purdue_andersed_star_PointOverlay_doNativeGlReadPixels(JNIEnv * env, jobject obj,  int current_frame_pbo_id, int previous_frame_pbo_id, int screenWidth, int screenHeight, int pbo_size, jlong workingImageMatPtr, int shrink_factor) {
	LOGI("starting doNativeGlReadPixels \n");

	LOGI("current_frame_pbo_id: %i", current_frame_pbo_id);

	LOGI("previous_frame_pbo_id: %i", previous_frame_pbo_id);

	Mat* workingImageMat = (Mat*) workingImageMatPtr;

	LOGI("binding pixel pack buffer to current frame's PBO \n");
	GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, current_frame_pbo_id) );

	LOGI("reading pixels \n");

	GL_CHECK( glReadPixels(0, 0, screenWidth, screenHeight, GL_RGBA, GL_UNSIGNED_BYTE, 0) );

	LOGI("unbinding pixel pack buffer \n");
	GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, 0) );

	LOGI("binding pixel pack buffer to previous frame's PBO \n");
	GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, previous_frame_pbo_id) );

	LOGI("mapping buffer range \n");

	GLubyte *ptr;
	GL_CHECK( ptr = (GLubyte *)glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, pbo_size, GL_MAP_READ_BIT) );

	LOGI("ptr from glMapBufferRange: %p", ptr);


	GLubyte *workingImageMatData = workingImageMat->data;


	LOGI("going to copy pixels, shrunk by a factor of %i", shrink_factor);
	int workingImageIndex = 0;
	int fullImageIndex = (screenHeight - 1) * BYTES_PER_PIXEL*screenWidth;
	for(int i = 0; i < workingImageMat->rows; i++) {
		for(int j = 0; j < workingImageMat->cols; j++) {
			//LOGI("i=%i, j=%i", i, j);
			//LOGI("workingImageIndex=%i, fullImageIndex=%i", workingImageIndex, fullImageIndex);
			//memcpy(workingImageMatData + workingImageIndex, ptr + fullImageIndex, BYTES_PER_PIXEL);
			if (fullImageIndex < pbo_size) {
				memcpy(workingImageMatData + workingImageIndex, ptr + fullImageIndex, BYTES_PER_PIXEL);
			} else {
				LOGI("fullImageIndex is too big: %i", fullImageIndex);
			}

			workingImageIndex += BYTES_PER_PIXEL;
			fullImageIndex += (BYTES_PER_PIXEL*shrink_factor);
		}
		fullImageIndex -= ((shrink_factor+1)*BYTES_PER_PIXEL*screenWidth);
	}


	/*
	memcpy(workingImageMatData, ptr, workingImageMat->rows * workingImageMat->cols * BYTES_PER_PIXEL);
	*/

	LOGI("unmapping buffer \n");
	GL_CHECK( glUnmapBuffer(GL_PIXEL_PACK_BUFFER) );

	LOGI("unbinding pixel pack buffer \n");
	GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, 0) );

	LOGI("ending doNativeGlReadPixels \n");
}

JNIEXPORT int JNICALL Java_edu_purdue_andersed_star_PBOBuffer_doNativePBOSetup(JNIEnv * env, jobject obj, int pbo_size) {
	LOGI("starting doNativePBOSetup \n");

	LOGI("pbo_size: %i", pbo_size);

	GLuint pbo_id;



	GL_CHECK( glGenBuffers(1, &pbo_id) );

	GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_id) );

	GL_CHECK( glBufferData(GL_PIXEL_PACK_BUFFER, pbo_size, 0, GL_DYNAMIC_READ) );

	GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, 0) );

	LOGI("pbo_id: %i", pbo_id);

	LOGI("ending doNativePBOSetup \n");

	return pbo_id;
}
