package edu.purdue.andersed.star.annotations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import edu.purdue.andersed.star.Toaster;
import android.util.Log;

public class AnnotationStateLogging {

	private static final String TAG = "AnnotationStateLogging";
	
	private static AnnotationStateLogging instance;
	
	private int mFrameNumber;
	private long mTimestamp;

	private File mLogFile;

	private BufferedWriter mBufferedWriter;
	
	private static final String LOG_PREFIX = "Frame\tTimestamp\tAnnotationState";
	
	public static AnnotationStateLogging getInstance() {
		if (instance == null) {
			instance = new AnnotationStateLogging();
			try {
				instance.initLogging();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toaster.getInstance().toast("couldn't set up annotation state logging");
			}
		}
		return instance;
	}
	
	public void initLogging() throws IOException {
		Log.d(TAG, "initLogging()");
		
		mFrameNumber = 0;
		
		mTimestamp = System.currentTimeMillis();
		
		String path = "sdcard/STAR_annotation_state_log_" + mTimestamp + ".log";
		
		Log.d(TAG, "going to create log file at " + path);
		
		mLogFile = new File(path);
		if (!mLogFile.exists()) {
			mLogFile.createNewFile();
		}
		
		Log.d(TAG, "setting up bufferedwriter for log");
		mBufferedWriter = new BufferedWriter(new FileWriter(mLogFile, true));	
		
		writeLogPrefix();
		
	}
	
	private void writeLogPrefix() throws IOException {
		mBufferedWriter.append(LOG_PREFIX);
		mBufferedWriter.newLine();
		mBufferedWriter.flush();
	}
	
	public void writeLogForFrame() {
		try {
			mFrameNumber++;
			
			long currentTimestamp = System.currentTimeMillis();
			
			JSONObject currentAnnotationState = AnnotationState.getInstance().toSimpleJSON();
			
			String logLine = mFrameNumber + "\t" + currentTimestamp + "\t" + currentAnnotationState.toString();
			
			mBufferedWriter.append(logLine);
			mBufferedWriter.newLine();
			mBufferedWriter.flush();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeLog() {
		Log.d(TAG, "closeLog");
		
		if (mBufferedWriter != null) {
			try {
				mBufferedWriter.flush();
				mBufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		mLogFile = null;
	}
}
