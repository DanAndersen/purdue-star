package edu.purdue.andersed.star.commands;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SendAnnotationsCommand extends Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7334571164481678545L;
	public static final String TAG = "SendAnnotationsCommand";
	
	private JSONObject mAnnotationStateJSONObject;
	
	public static final String KEY_ANNOTATION_STATE = "annotation_state";
	
	public SendAnnotationsCommand(JSONObject annotationStateObject) {
		Log.d(TAG, "constructing SendAnnotationsCommand");
		mAnnotationStateJSONObject = annotationStateObject;
	}

	public JSONObject getAnnotationStateObject() {
		return mAnnotationStateJSONObject;
	}
	
	@Override
	public JSONObject toJSON() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put(KEY_COMMAND, TAG);
		obj.put(KEY_ANNOTATION_STATE, mAnnotationStateJSONObject);
		return obj;
	}
	
	public static SendAnnotationsCommand fromJSON(JSONObject commandJsonObject) throws JSONException {
		
		JSONObject annotationStateObject = commandJsonObject.getJSONObject(KEY_ANNOTATION_STATE);
		
		return new SendAnnotationsCommand(annotationStateObject);
	}
	
}
