package edu.purdue.andersed.star.annotations;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import android.util.Log;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class MultiPointAnnotation extends Annotation {

	private static final String TAG = "MultiPointAnnotation";
	
	private static final int NUM_POINTS_IN_CIRCLE = 8;
	
	protected MatOfPoint2f mPointsMat = new MatOfPoint2f();
		
	public MultiPointAnnotation(MatOfPoint2f pointsMat) {
		mPointsMat = pointsMat;
	}
	
	public MultiPointAnnotation() {
		Log.d(TAG, "creating empty MultiPointAnnotation");
	}

	
	public MultiPointAnnotation(List<Point> pointsInScreenSpace) {
		setPointsInScreenSpace(pointsInScreenSpace);
	}
	
	public void setPointsInScreenSpace(List<Point> pointsInScreenSpace) {
		List<Point> pointsInOpenCVSpace = new ArrayList<Point>();
		for (Point pointInScreenSpace : pointsInScreenSpace) {
			pointsInOpenCVSpace.add(ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace));
		}
		
		mPointsMat.fromList(pointsInOpenCVSpace);
	}

	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		drawWithColorAndThickness(pointOverlay, color, Pref.getInstance().getAnnotationLineThickness());
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		int circleRadiusPx = Pref.getInstance().getCircleAnnotationRadiusPixels();
		
		List<Point> pointsInOpenCVSpace = mPointsMat.toList();
		for (Point pointInOpenCVSpace : pointsInOpenCVSpace) {
			Point pointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(pointInOpenCVSpace);
			

			Point currentPointInCircle = new Point(
				pointInScreenSpace.x + circleRadiusPx * Math.cos(0), 
				pointInScreenSpace.y + circleRadiusPx * Math.sin(0)
				);
			Point nextPointInCircle;
			
			for (int i = 0; i <= NUM_POINTS_IN_CIRCLE; i++) {
				double radians = i * (2.0 * Math.PI) / NUM_POINTS_IN_CIRCLE; 
				nextPointInCircle = new Point(
						pointInScreenSpace.x + circleRadiusPx * Math.cos(radians), 
						pointInScreenSpace.y + circleRadiusPx * Math.sin(radians)
						);
				pointOverlay.drawLineWithColorAndThickness(currentPointInCircle, nextPointInCircle, color, thickness);
				currentPointInCircle = nextPointInCircle;
			}
			
			//pointOverlay.drawPointWithColor(pointInScreenSpace, color);
		}
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_MULTI_POINT;
	}

	@Override
	String getGeometryString() {
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		return pointsInScreenSpace.toString();		
	}
	
	public List<Point> getPointsInScreenSpace() {
		List<Point> pointsInOpenCVSpace = mPointsMat.toList();
		
		List<Point> pointsInScreenSpace = new ArrayList<Point>();
		for (Point pointInOpenCVSpace : pointsInOpenCVSpace) {
			Point pointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(pointInOpenCVSpace);
			pointsInScreenSpace.add(pointInScreenSpace);
		}
		
		return pointsInScreenSpace;
	}

	@Override
	public Annotation getTransformedAnnotationBasedOnHomography(Mat homography) {
		MatOfPoint2f transformedPointMat = new MatOfPoint2f();
		
		// transform is only valid for its own scale (in opencv coords not screen coords) -- scaling the point while working with it
		Core.perspectiveTransform(mPointsMat, transformedPointMat, homography);	
		
		return new MultiPointAnnotation(transformedPointMat);
	}

	public void addPointWithScreenCoordinates(Point pointInScreenSpace) {
		Point pointInOpenCVSpace = ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace);
		mPointsMat.push_back(new MatOfPoint2f(pointInOpenCVSpace));
	}
	
	@Override
	public MatOfPoint2f getPointsMat() {
		return mPointsMat;
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject multiPointAnnotationObject = new JSONObject();
		
		multiPointAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		multiPointAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(getPointsMat()));
		
		//Log.d(TAG, "result: " + multiPointAnnotationObject.toString());
		return multiPointAnnotationObject;
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		MultiPointAnnotation multiPointAnnotation = new MultiPointAnnotation(pointMat);
		
		return multiPointAnnotation;
	}

	@Override
	public void translate(Point point) {
		List<Point> pointsInScreenSpace = this.getPointsInScreenSpace();
		for (Point p : pointsInScreenSpace) {
			p.x += point.x;
			p.y += point.y;
		}
		this.setPointsInScreenSpace(pointsInScreenSpace);
	}

	

}
