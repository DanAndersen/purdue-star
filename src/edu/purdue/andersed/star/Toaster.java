package edu.purdue.andersed.star;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

public class Toaster {

	private Activity mActivity;
	
	private static Toaster instance = null;
	
	private static final String TAG = "Toaster";
	
	private Toaster(Activity activity) {
		Log.d(TAG, "constructing toaster");
		mActivity = activity;
	}
	
	public static Toaster getInstance() {
		if (instance == null) {
			throw new IllegalStateException("Toaster hasn't been initialized yet");
		}
		return instance;
	}
	
	public static void initialize(Activity activity) {
		Log.d(TAG, "initing toaster");
		instance = new Toaster(activity);
	}

	public void toast(final String message) {
		Log.d(TAG, "toast(" + message + ")");
		mActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Log.d(TAG, "making toast");
				Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
			}
		});
		
	}
}
