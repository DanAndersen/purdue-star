package edu.purdue.andersed.star.shaders;

import javax.microedition.khronos.opengles.GL10;

public interface CameraViewBackground {

	public void draw(GL10 gl);
}
