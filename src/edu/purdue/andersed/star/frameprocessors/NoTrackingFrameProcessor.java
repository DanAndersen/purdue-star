package edu.purdue.andersed.star.frameprocessors;

import org.opencv.core.Mat;

import android.util.Log;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PointOverlay;

public class NoTrackingFrameProcessor extends FrameProcessor {

	private static final String TAG = "NoTrackingFrameProcessor";
	
	public NoTrackingFrameProcessor(MainActivity delegate,
			PointOverlay pointOverlay) {
		super(delegate, pointOverlay);
		
	}

	@Override
	protected void initFrameProcessor() {
		Log.d(TAG, "initFrameProcessor -- nothing to do");
	}

	@Override
	protected void resetFrameProcessor() {
		Log.d(TAG, "resetFrameProcessor -- nothing to do");
	}

	@Override
	public void processFrameIfReady(Mat workingImage, Mat fullResolutionImage) {
		Log.d(TAG, "processFrameIfReady -- nothing to do");
	}

}
