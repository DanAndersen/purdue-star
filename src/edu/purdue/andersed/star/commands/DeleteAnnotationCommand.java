package edu.purdue.andersed.star.commands;

import org.json.JSONException;
import org.json.JSONObject;

public class DeleteAnnotationCommand extends Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1007975793339983569L;

	public static final String TAG = "DeleteAnnotationCommand";
	
	public static final String KEY_ID = "id";
	
	private Integer mId;
	
	public DeleteAnnotationCommand(Integer id) {
		mId = id;
	}
	
	@Override
	public JSONObject toJSON() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put(KEY_COMMAND, TAG);
		obj.put(KEY_ID, mId);
		return obj;
	}
	
	public Integer getId() {
		return mId;
	}
	
	public static DeleteAnnotationCommand fromJSON(JSONObject commandJsonObject) throws JSONException {
		
		Integer id = commandJsonObject.getInt(KEY_ID);
		
		return new DeleteAnnotationCommand(id);
	}

}
