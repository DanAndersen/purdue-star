package edu.purdue.andersed.star.shaders;

import org.opencv.core.Point;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.geometry.Quad;
import android.graphics.Color;
import android.opengl.GLES30;
import android.util.Log;

public class QuadShaderProgram {
	
	private static final String TAG = "QuadShaderProgram";
	
	private static final String PASS_THROUGH_VERTEX_SHADER =
		"attribute vec4 vertexPosition; \n" +
		"uniform vec2 screenResolution; \n" +
		"uniform vec2 point; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_Position = vec4(vertexPosition.x + (2.0 * point.x / screenResolution.x - 1.0), vertexPosition.y - (2.0 * point.y / screenResolution.y - 1.0), vertexPosition.z, vertexPosition.w); \n" +
		"} \n"
			;
	
	private static final String POINT_OVERLAY_FRAGMENT_SHADER = 
		"#extension GL_OES_EGL_image_external : require\n" +
		"precision mediump float; \n" + 
		"uniform vec4 pointColor; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_FragColor = pointColor; \n" +
		"} \n"
			;
	
	private int mQuadShaderProgramID;
	
	private int mQuadVertexHandle;
	private int mResolutionHandle;
	private int mPointHandle;
	private int mPointColorHandle;
	
	private Quad mQuadGeometry;
	
	public QuadShaderProgram() {
		setupQuadShaderProgram();
	}
	
	private void setupQuadShaderProgram() {
		Log.d(TAG, "creating geometry");
		mQuadGeometry = new Quad(0.01f);
		
		Log.d(TAG, "creating shader program");
		mQuadShaderProgramID = STARUtils.createProgramFromShaderSrc(PASS_THROUGH_VERTEX_SHADER, POINT_OVERLAY_FRAGMENT_SHADER );		
		STARUtils.checkGLError("createProgramFromShaderSrc");

		Log.d(TAG, "setting up handles");
		mQuadVertexHandle = GLES30.glGetAttribLocation(mQuadShaderProgramID, "vertexPosition");
		STARUtils.checkGLError("glGetAttribLocation");
		mResolutionHandle = GLES30.glGetUniformLocation(mQuadShaderProgramID, "screenResolution");
		STARUtils.checkGLError("glGetUniformLocation");
		
		mPointHandle = GLES30.glGetUniformLocation(mQuadShaderProgramID, "point");
		
		mPointColorHandle = GLES30.glGetUniformLocation(mQuadShaderProgramID, "pointColor");
	
	}
	
	public void drawPointWithColor(Point p, int color) {
		GLES30.glUseProgram(mQuadShaderProgramID);
		STARUtils.checkGLError("glUseProgram");
		
		GLES30.glUniform2f(mResolutionHandle, ScreenState.getInstance().getScreenWidth(), ScreenState.getInstance().getScreenHeight());
		STARUtils.checkGLError("glUniform2f");

		
		GLES30.glVertexAttribPointer(mQuadVertexHandle, 3, GLES30.GL_FLOAT, false, 0, mQuadGeometry.getVertices());
		STARUtils.checkGLError("glVertexAttribPointer");

		
		GLES30.glEnableVertexAttribArray(mQuadVertexHandle);
		STARUtils.checkGLError("glEnableVertexAttribArray");
		
		// now draw something on that point
		GLES30.glUniform4f(mPointColorHandle, (float)Color.red(color) / 255, (float)Color.green(color) / 255, (float)Color.blue(color) / 255, 255);
		STARUtils.checkGLError("glUniform4f");

		GLES30.glUniform2f(mPointHandle, (float)p.x, (float)p.y);
		STARUtils.checkGLError("glUniform2f");
		GLES30.glDrawElements(GLES30.GL_TRIANGLES,
				mQuadGeometry.getNumObjectIndex(), GLES30.GL_UNSIGNED_SHORT,
				mQuadGeometry.getIndices());
		STARUtils.checkGLError("glDrawElements");
		
		GLES30.glDisableVertexAttribArray(mQuadVertexHandle);
	}
}
