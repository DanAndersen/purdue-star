package edu.purdue.andersed.star;

import android.util.Log;

public class PBOBuffer {

	private static final String TAG = "PBOBuffer";
	
	private int[] mPBOs;
	
	public PBOBuffer(int numPBOs, int pbo_size) {
		Log.d(TAG, "setting up PBOBuffer for " + numPBOs + " PBOs");
		mPBOs = new int[numPBOs];
		
		for (int i = 0; i < numPBOs; i++) {
			mPBOs[i] = doNativePBOSetup(pbo_size);
		}
	}
	
	private native int doNativePBOSetup(int pbo_size);
	
	public void advanceBuffer() {
		//Log.d(TAG, "advanceBuffer");
		int[] newPBOsArray = new int[mPBOs.length];
		for (int i = 0; i < mPBOs.length; i++) {
			if (i < mPBOs.length - 1) {
				newPBOsArray[i] = mPBOs[i+1];	
			} else {
				newPBOsArray[i] = mPBOs[0];
			}
		}
		mPBOs = newPBOsArray;
	}
	
	public int getNthPBO(int n) {
		if (n < 0 || n >= mPBOs.length) {
			throw new IllegalArgumentException("invalid index: " + n + ", numPBOs = " + mPBOs.length);
		}
		
		return mPBOs[n];
	}
}
