package edu.purdue.andersed.star;

import org.opencv.core.Point;

import edu.purdue.andersed.star.settings.Pref;

public class ScreenState {

	private static ScreenState instance;
	
	private int mScreenWidth = -1;
	private int mScreenHeight = -1;
	
	private int mHalfScreenWidth = -1;
	private int mHalfScreenHeight = -1;
	
	private float mAspectRatio = -1;
	
	public int getScreenWidth() {
		if (mScreenWidth < 0) {
			throw new IllegalStateException("screen width has not yet been initialized");
		}
		return mScreenWidth;
	}

	public int getScreenHeight() {
		if (mScreenHeight < 0) {
			throw new IllegalStateException("screen height has not yet been initialized");
		}
		return mScreenHeight;
	}
	
	public int getHalfScreenWidth() {
		if (mHalfScreenWidth < 0) {
			throw new IllegalStateException("half screen width has not yet been initialized");
		}
		return mHalfScreenWidth;
	}

	public int getHalfScreenHeight() {
		if (mHalfScreenHeight < 0) {
			throw new IllegalStateException("half screen height has not yet been initialized");
		}
		return mHalfScreenHeight;
	}
	
	public float getAspectRatio() {
		if (mAspectRatio < 0) {
			throw new IllegalStateException("aspect ratio has not yet been initialized");
		}
		return mAspectRatio;
	}

	public static ScreenState getInstance() {
		if (instance == null) {
			instance = new ScreenState();
		}
		return instance;
	}
	
	public void initScreenSize(int width, int height) {
		mScreenWidth = width;
		mHalfScreenWidth = mScreenWidth / 2;
		mScreenHeight = height;
		mHalfScreenHeight = mScreenHeight / 2;
		
		mAspectRatio = (float) mScreenWidth / (float) mScreenHeight;
	}
	
	public Point screenSpaceToOpenCVSpace(Point screenPoint) {
		double x = screenPoint.x / getScreenWidth();
		double y = screenPoint.y / getScreenHeight();
		return new Point(x,y);
	}
	
	public Point openCVSpaceToScreenSpace(Point openCVSpacePoint) {
		double x = openCVSpacePoint.x * getScreenWidth();
		double y = openCVSpacePoint.y * getScreenHeight();
		return new Point(x,y);
	}

}
