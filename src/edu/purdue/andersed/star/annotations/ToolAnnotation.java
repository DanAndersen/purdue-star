package edu.purdue.andersed.star.annotations;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class ToolAnnotation extends PointAnnotation {

	private static final String TAG = "ToolAnnotation";
	
	// a tool annotation is stored like a point annotation. 
	// it is scale-invariant, with the point being manipulated as an anchor point,
	// around which the bitmap quad is drawn
		
	private ToolType mToolType;
	
	private double mDegrees;
	
	private double mScale;
	
	private int mSelectableColor;	// used to uniquely identify this annotation instance when attempting selection
			
	private boolean mIsSelected;
	
	public ToolAnnotation(MatOfPoint2f pointMat, ToolType toolType, double degrees, double scale, int selectableColor) {
		super(pointMat);
		
		mToolType = toolType;
		
		mDegrees = degrees;
		
		mScale = scale;
		
		mSelectableColor = selectableColor;
						
		mIsSelected = false;
		
		validate();
	}
	
	public ToolAnnotation(Point pointInScreenSpace, ToolType toolType, double degrees, double scale, int selectableColor) {
		super(pointInScreenSpace);
		
		mToolType = toolType;
		
		mDegrees = degrees;
		
		mScale = scale;
		
		mSelectableColor = selectableColor;
		
		mIsSelected = false;
						
		validate();
	}
	
	private void validate() {
		if (this.mPointMat == null || this.mPointMat.rows() != 1) {
			throw new IllegalStateException("invalid point mat");
		}
		
		if (mToolType == null) {
			throw new IllegalStateException("invalid tool type: " + mToolType);
		}
	}
	
	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		//Log.d(TAG, "drawWithColor");
		drawTool(pointOverlay);
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_TOOL;
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		//Log.d(TAG, "drawWithColorAndThickness");
		drawTool(pointOverlay);
	}
	
	public void drawTool(PointOverlay pointOverlay) {
		//Log.d(TAG, "drawTool");
		pointOverlay.drawTool(this);
	}
	
	public ToolType getToolType() {
		return this.mToolType;
	}
	
	// this is needed -- otherwise the tool annotation turns into a point annotation
	@Override
	public Annotation getTransformedAnnotationBasedOnHomography(Mat homography) {

		MatOfPoint2f transformedPointMat = new MatOfPoint2f();
		
		// transform is only valid for its own scale (in opencv coords not screen coords) -- scaling the point while working with it
		Core.perspectiveTransform(mPointMat, transformedPointMat, homography);	
		
		return new ToolAnnotation(transformedPointMat, mToolType, mDegrees, mScale, mSelectableColor);
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject toolAnnotationObject = new JSONObject();
		
		toolAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		toolAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(getPointsMat()));
		toolAnnotationObject.put(TAG_ANNOTATION_TOOL_TYPE, getToolType().getTag());
		toolAnnotationObject.put(TAG_ANNOTATION_ROTATION, mDegrees);
		toolAnnotationObject.put(TAG_ANNOTATION_SCALE, mScale);
		toolAnnotationObject.put(TAG_ANNOTATION_SELECTABLE_COLOR, mSelectableColor);
		
		//Log.d(TAG, "result: " + toolAnnotationObject.toString());
		return toolAnnotationObject;
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		ToolType toolType = ToolType.fromTag(annotationJSONObject.getString(TAG_ANNOTATION_TOOL_TYPE));
		
		double degrees = annotationJSONObject.getDouble(TAG_ANNOTATION_ROTATION);
		double scale = annotationJSONObject.getDouble(TAG_ANNOTATION_SCALE);
		int selectableColor = annotationJSONObject.getInt(TAG_ANNOTATION_SELECTABLE_COLOR);
		
		ToolAnnotation toolAnnotation = new ToolAnnotation(pointMat, toolType, degrees, scale, selectableColor);
		
		return toolAnnotation;
	}
	
	public void setDegrees(double degrees) {
		mDegrees = degrees;
	}
	
	public double getDegrees() {
		return mDegrees;
	}

	public double getScale() {
		return mScale;
	}

	public void setScale(double scale) {
		this.mScale = scale;
	}
	
	public int getSelectableColor() {
		return mSelectableColor;
	}
	
	public boolean isSelected() {
		return mIsSelected;
	}
	
	public void setSelected(boolean selected) {
		mIsSelected = selected;
	}
	
}
