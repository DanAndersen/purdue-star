package edu.purdue.andersed.star.annotations;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import android.util.Log;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class PolylineAnnotation extends Annotation {

	private static final String TAG = "PolylineAnnotation";
	
	protected MatOfPoint2f mPointMat = new MatOfPoint2f();
	
	public PolylineAnnotation(MatOfPoint2f pointMat) {
		Log.d(TAG, "creating PolylineAnnotation from pointmat");
		mPointMat = pointMat;
	}
	
	public PolylineAnnotation() {
		Log.d(TAG, "creating empty PolylineAnnotation");
	}
	
	public void addPointWithScreenCoordinates(Point pointInScreenSpace) {
		Point pointInOpenCVSpace = ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace);
		mPointMat.push_back(new MatOfPoint2f(pointInOpenCVSpace));
	}
	
	public PolylineAnnotation(List<Point> pointsInScreenSpace) {
		for (Point pointInScreenSpace : pointsInScreenSpace) {
			addPointWithScreenCoordinates(pointInScreenSpace);
		}
	}
	
	// Takes a String in the form:
	// 638 364
	// 748 388
	// 847 403
	// etc
	public PolylineAnnotation(String stringOfPoints) {
		Log.d(TAG, "creating new polyline annotation from string \"" + stringOfPoints + "\"");
		
		String lines[] = stringOfPoints.split("\\r?\\n");
		
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			String coords[] = line.split("\\s+");
			
			double xCoord = Double.parseDouble(coords[0]);
			double yCoord = Double.parseDouble(coords[1]);
			
			addPointWithScreenCoordinates(new Point(xCoord, yCoord));
		}
	}

	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		drawWithColorAndThickness(pointOverlay, color, Pref.getInstance().getDefaultLineThickness());
	}

	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		for (int i = 0; i < pointsInScreenSpace.size(); i++) {
			
			if (i < pointsInScreenSpace.size() - 1) {
				pointOverlay.drawLineWithColorAndThickness(pointsInScreenSpace.get(i), pointsInScreenSpace.get(i+1), color, thickness);
			}
		}
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_POLYLINE;
	}

	@Override
	String getGeometryString() {
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		return pointsInScreenSpace.toString();
	}

	@Override
	public Annotation getTransformedAnnotationBasedOnHomography(Mat homography) {
		
		MatOfPoint2f transformedPointMat = new MatOfPoint2f();
		
		// transform is only valid for its own scale (in opencv coords not screen coords) -- scaling the point while working with it
		Core.perspectiveTransform(mPointMat, transformedPointMat, homography);	
		
		return new PolylineAnnotation(transformedPointMat);
	}
	
	public List<Point> getPointsInScreenSpace() {
		List<Point> pointsInOpenCVSpace = mPointMat.toList();
		
		List<Point> pointsInScreenSpace = new ArrayList<Point>();
		for (Point pointInOpenCVSpace : pointsInOpenCVSpace) {
			Point pointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(pointInOpenCVSpace);
			pointsInScreenSpace.add(pointInScreenSpace);
		}
		
		return pointsInScreenSpace;
	}


	@Override
	public MatOfPoint2f getPointsMat() {
		return mPointMat;
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		
		JSONObject polylineAnnotationObject = new JSONObject();
		
		polylineAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		polylineAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(getPointsMat()));
		
		return polylineAnnotationObject;
	}

	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		PolylineAnnotation polylineAnnotation = new PolylineAnnotation(pointMat);
		
		return polylineAnnotation;
	}
	
	public void setPointsInScreenSpace(List<Point> pointsInScreenSpace) {
		List<Point> pointsInOpenCVSpace = new ArrayList<Point>();
		for (Point pointInScreenSpace : pointsInScreenSpace) {
			pointsInOpenCVSpace.add(ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace));
		}
		
		mPointMat.fromList(pointsInOpenCVSpace);
	}

	@Override
	public void translate(Point point) {
		List<Point> pointsInScreenSpace = this.getPointsInScreenSpace();
		for (Point p : pointsInScreenSpace) {
			p.x += point.x;
			p.y += point.y;
		}
		this.setPointsInScreenSpace(pointsInScreenSpace);
	}

}
