package edu.purdue.andersed.star;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

	private static final String TAG = "CameraPreview";
	
	private SurfaceHolder mHolder;
	private Camera mCamera;
	
	public CameraPreview(Context context, Camera camera) {
		super(context);
		
		mCamera = camera;
		
		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated");
		// The Surface has been created, now tell the camera where to draw the preview
		
		initAndStartPreview(holder);	
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		Log.d(TAG, "surfaceChanged, format = " + format + ", width = " + width + ", height = " + height);
		// If preview can change or rotate, take care of those events here.
		// Make sure to stop preview before resizing or reformatting it.
		
		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}
		
		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existed preview
		}
		
		// set preview size and make any resize, rotate or
		// reformatting changes here
		Camera.Parameters cameraParameters = mCamera.getParameters();
		List<Size> supportedPreviewSizes = cameraParameters.getSupportedPreviewSizes();
		Size maxSize = supportedPreviewSizes.get(0);	// getSupportedPreviewSizes() always returns a list with >= 1 element
		for (Size supportedPreviewSize : supportedPreviewSizes) {
			if (supportedPreviewSize.height > maxSize.height || supportedPreviewSize.width > maxSize.width) {
				maxSize = supportedPreviewSize;
			}
		}
		
		Log.d(TAG, "setting preview size to " + maxSize.width + " x " + maxSize.height);
		cameraParameters.setPreviewSize(maxSize.width, maxSize.height);
		
		initAndStartPreview(mHolder);
	}

	private void initAndStartPreview(SurfaceHolder holder) {
		try {
			mCamera.setPreviewDisplay(holder);
			mCamera.startPreview();
		} catch (IOException e) {
			Log.e(TAG, "Error setting camera preview: " + e.getMessage());
		}
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "surfaceDestroyed");
		// empty. Take care of releasing the Camera preview in activity.
	}
}
