package edu.purdue.andersed.star;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.opengl.GLES30;
import android.util.Log;
import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.annotations.AnnotationStateLogging;
import edu.purdue.andersed.star.annotations.ToolAnnotation;
import edu.purdue.andersed.star.frameprocessors.FrameProcessor;
import edu.purdue.andersed.star.frameprocessors.NoTrackingFrameProcessor;
import edu.purdue.andersed.star.frameprocessors.StandardFrameProcessor;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.shaders.LineShaderProgram;
import edu.purdue.andersed.star.shaders.QuadShaderProgram;
import edu.purdue.andersed.star.shaders.TexturedToolShaderProgram;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class PointOverlay {
	
	private static final String TAG = "PointOverlay";
	
	// shader programs
	private QuadShaderProgram mQuadShaderProgram;
	private LineShaderProgram mLineShaderProgram;
	private TexturedToolShaderProgram mTexturedToolShaderProgram;
	
	private MainActivity mDelegate;
	
	private Bitmap mPreviewBitmap;

	private Mat mOpenCVWorkingImageBufferA;
	private Mat mOpenCVWorkingImageBufferB;

	private Mat mOpenCVFullResolutionImageBufferA;
	private Mat mOpenCVFullResolutionImageBufferB;
	
	private boolean mOpenCVImageBuffersSwapped = false;
		
	private Mat mYUVMat;
	
	private final int mScreenBufferSize;
	
	public static final int NUM_CHANNELS = 4;
	
	
	public static final int OPENCV_MAT_TYPE = (NUM_CHANNELS == 4 ? CvType.CV_8UC4 : CvType.CV_8UC3);

	private FrameProcessor mFrameProcessor;
	
	private List<Point> mKeyPointsToDraw;
	
	private List<PointMatchingPair> mPointMatchingPairsToDraw;
	
	// --- PBO, only used for readpixels when reading from file, because cannot get good performance getting bitmap frames from video file
	private static final int NUM_PBO_BUFFERS = 2;
	private PBOBuffer mPBOBuffer;
	
	static 
    {
    	try
    	{ 
    		
    		// Load necessary libraries.
    		Log.d(TAG, "loading avutil");
    		System.loadLibrary("avutil-54");
    		Log.d(TAG, "loading avcodec");
            System.loadLibrary("avcodec-56");
            Log.d(TAG, "loading avformat");
            System.loadLibrary("avformat-56");
            Log.d(TAG, "loading swscale");
            System.loadLibrary("swscale-3");
            Log.d(TAG, "loading avfilter");
            System.loadLibrary("avfilter-5");
            Log.d(TAG, "loading swresample");
            System.loadLibrary("swresample-1");
            
            
            
            Log.d(TAG, "loading opencv_java");
    		System.loadLibrary("opencv_java");
    		Log.d(TAG, "loading nonfree");
    		System.loadLibrary("nonfree");
    		Log.d(TAG, "loading point_overlay_jni");
    		System.loadLibrary("point_overlay_jni");
    	}
    	catch( UnsatisfiedLinkError e )
		{
           System.err.println("Native code library failed to load.\n" + e);
           throw new RuntimeException("unable to load JNI opencv nonfree modules.");
		}
    }
	
	private void resetImageProcessingData() {
		mOpenCVWorkingImageBufferA = new Mat(ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), OPENCV_MAT_TYPE);
		mOpenCVWorkingImageBufferB = new Mat(ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), OPENCV_MAT_TYPE);
		
		mOpenCVFullResolutionImageBufferA = new Mat(ScreenState.getInstance().getScreenHeight(), ScreenState.getInstance().getScreenWidth(), OPENCV_MAT_TYPE);
		mOpenCVFullResolutionImageBufferB = new Mat(ScreenState.getInstance().getScreenHeight(), ScreenState.getInstance().getScreenWidth(), OPENCV_MAT_TYPE);
		
		mKeyPointsToDraw = new ArrayList<Point>();
		mPointMatchingPairsToDraw = new ArrayList<PointMatchingPair>();
	}

	public PointOverlay(MainActivity delegate) {
		Log.d(TAG, "setting up point overlay");
		mDelegate = delegate;
		
		mScreenBufferSize = ScreenState.getInstance().getScreenWidth() * ScreenState.getInstance().getScreenHeight() * NUM_CHANNELS;
						
		mPBOBuffer = new PBOBuffer(NUM_PBO_BUFFERS, mScreenBufferSize);
		
		mQuadShaderProgram = new QuadShaderProgram();
		mLineShaderProgram = new LineShaderProgram();
		mTexturedToolShaderProgram = new TexturedToolShaderProgram(mDelegate);
						
		resetImageProcessingData();
		
		if (Pref.getInstance().isTrackingEnabled()) {
			mFrameProcessor = new StandardFrameProcessor(mDelegate, this);
		} else {
			mFrameProcessor = new NoTrackingFrameProcessor(mDelegate, this);
		}
		
		
	}
	
	private void readPixelsIntoBuffer() {
		int currentPBO = mPBOBuffer.getNthPBO(0);
		int prevPBO = mPBOBuffer.getNthPBO(0);
		if (Pref.getInstance().isUsingDoubleBuffering()) {
			prevPBO = mPBOBuffer.getNthPBO(1);
		}
		
		doNativeGlReadPixels(
				currentPBO, 
				prevPBO, 
				ScreenState.getInstance().getScreenWidth(), 
				ScreenState.getInstance().getScreenHeight(), 
				mScreenBufferSize, 
				getCurrentOpenCVFullResolutionImageBuffer().getNativeObjAddr(), 
				1);	
		Imgproc.resize(getCurrentOpenCVFullResolutionImageBuffer(), getCurrentOpenCVWorkingImageBuffer(), new Size(ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor()));
		
		
		/*
		if (Pref.getInstance().isSavingAnnotationReferenceFrame()) {
			// in this case, we care about the full-resolution images, so do the pixel read differently
			doNativeGlReadPixels(
					currentPBO, 
					prevPBO, 
					ScreenState.getInstance().getScreenWidth(), 
					ScreenState.getInstance().getScreenHeight(), 
					mScreenBufferSize, 
					getCurrentOpenCVFullResolutionImageBuffer().getNativeObjAddr(), 
					1);	
			Imgproc.resize(getCurrentOpenCVFullResolutionImageBuffer(), getCurrentOpenCVWorkingImageBuffer(), new Size(ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor()));
		} else {
			doNativeGlReadPixels(
					currentPBO, 
					prevPBO, 
					ScreenState.getInstance().getScreenWidth(), 
					ScreenState.getInstance().getScreenHeight(), 
					mScreenBufferSize, 
					getCurrentOpenCVWorkingImageBuffer().getNativeObjAddr(), 
					Pref.getInstance().getCvImageShrinkFactor());	
		}
		*/
		
		
		mPBOBuffer.advanceBuffer();
	}
	
	private Mat getCurrentOpenCVFullResolutionImageBuffer() {
		return mOpenCVImageBuffersSwapped ? mOpenCVFullResolutionImageBufferB : mOpenCVFullResolutionImageBufferA;
	}
	
	private Mat getCurrentOpenCVWorkingImageBuffer() {
		return mOpenCVImageBuffersSwapped ? mOpenCVWorkingImageBufferB : mOpenCVWorkingImageBufferA;
	}
	
	private void swapOpenCVImageBuffers() {
		mOpenCVImageBuffersSwapped = !mOpenCVImageBuffersSwapped; 
	}
	
	public void draw(GL10 gl) {
		//Log.d(TAG, "starting draw()");
		
		if (mFrameProcessor.isReadyToProcessFrame()) {
			if (Pref.getInstance().isUsingRecordedVideo()) {
				// recorded video requires we still do the glreadpixels thing
				readPixelsIntoBuffer();
				mFrameProcessor.processFrameIfReady(getCurrentOpenCVWorkingImageBuffer(), getCurrentOpenCVFullResolutionImageBuffer());
			} else {
				//synchronized(getCurrentOpenCVWorkingImageBuffer()) {
					mFrameProcessor.processFrameIfReady(getCurrentOpenCVWorkingImageBuffer(), getCurrentOpenCVFullResolutionImageBuffer());
				//}	
			}
			
		}
		
		if(Pref.getInstance().isDrawingKeyPoints() && mKeyPointsToDraw.size() > 0) {
			for (Point p : mKeyPointsToDraw) {
				drawPointWithColor(p, Color.DKGRAY);
			}
		}
		
		if (Pref.getInstance().isDrawingPointMatching() && mPointMatchingPairsToDraw.size() > 0) {
			for (PointMatchingPair p : mPointMatchingPairsToDraw) {
				drawPointMatching(p.getP1(), p.getP2(), p.isInlier());
			}
		}

		
		
		drawAnnotations(AnnotationState.getInstance().getAnnotations());
		
		AnnotationStateLogging.getInstance().writeLogForFrame();
		
		
		// now that all annotation drawing has happened...
		if (PeerConnection.getInstance().getOwnUserType().isTrainee() && Pref.getInstance().isShowingPreviousSteps() && PreviousStepsState.getInstance().shouldCaptureNextFrame()) {
			Log.d(TAG, "capturing frame for prev steps");
			int width = ScreenState.getInstance().getScreenWidth();
			int height = ScreenState.getInstance().getScreenHeight();
			int screenshotSize = width * height;
            ByteBuffer bb = ByteBuffer.allocateDirect(screenshotSize * NUM_CHANNELS);
            bb.order(ByteOrder.nativeOrder());
			
			GLES30.glReadPixels(0, 0, width, height, GLES30.GL_RGBA, GLES30.GL_UNSIGNED_BYTE, bb);
			
			int pixelsBuffer[] = new int[screenshotSize];
            bb.asIntBuffer().get(pixelsBuffer);
            bb = null;
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            bitmap.setPixels(pixelsBuffer, screenshotSize-width, -width, 0, 0, width, height);
            pixelsBuffer = null;

            short sBuffer[] = new short[screenshotSize];
            ShortBuffer sb = ShortBuffer.wrap(sBuffer);
            bitmap.copyPixelsToBuffer(sb);

            //Making created bitmap (from OpenGL points) compatible with Android bitmap
            for (int i = 0; i < screenshotSize; ++i) {                  
                short v = sBuffer[i];
                sBuffer[i] = (short) (((v&0x1f) << 11) | (v&0x7e0) | ((v&0xf800) >> 11));
            }
            sb.rewind();
            bitmap.copyPixelsFromBuffer(sb);
			
            Log.d(TAG, "done capturing frame for prev steps");
            
			PreviousStepsState.getInstance().addBitmap(bitmap);
		}
		
	}
	
	public void setKeyPointsToDraw(List<Point> keyPointsToDraw) {
		mKeyPointsToDraw = keyPointsToDraw;
	}
	
	private void sendFrameToMentor(Mat frameMat) {
		PeerConnection.getInstance().sendFrameDataToOtherUser(mDelegate, frameMat);
	}

	private void drawAnnotations(Map<Integer, AnnotationMemory> annotationMemoryMap) {
		Iterator<Integer> iter = annotationMemoryMap.keySet().iterator();
		
		while (iter.hasNext()) {
			Integer id = iter.next();
			AnnotationMemory annotationMemory = annotationMemoryMap.get(id);
			
			if (annotationMemory != null && annotationMemory.isValid()) {
				
				if (annotationMemory.getCurrentHomography() != null) {
					drawAnnotation(annotationMemory);	
				}
			}
		}
	}

	private native void doNativeGlReadPixels( int current_frame_pbo_id, int previous_frame_pbo_id, int screenWidth, int screenHeight, int pbo_size, long workingImageMatPtr, int shrink_factor);

	private void drawAnnotation(AnnotationMemory annotationMemory) {
		//Log.d(TAG, "drawing annotation");
			
		Annotation annotationToDraw = annotationMemory.getCurrentAnnotation();
		
		if (Pref.getInstance().isOnlyDrawingInitialAnnotation()) {
			annotationToDraw = annotationMemory.getInitialAnnotation();
		}
		
		annotationToDraw.drawWithColorAndThickness(this, Color.BLUE, Pref.getInstance().getAnnotationLineThickness());
			
	}
	
	
	private boolean isInfiniteOrNaN(double num) {
		return Double.isInfinite(num) || Double.isNaN(num);
	}
	
    private native void doProcessingNative();
	
	private native void doNativeFeatureDetection(long imageNativeObj, long destinationMatOfKeyPointNativeObj);

	private void drawPointMatching(Point prevPointInScreenSpace, Point nextPointInScreenSpace, boolean isInlier) {
		if (isInlier) {
			drawLineWithColor(prevPointInScreenSpace, nextPointInScreenSpace, Color.GREEN);
			drawPointWithColor(nextPointInScreenSpace, Color.BLACK);
		} else {
			drawLineWithColor(prevPointInScreenSpace, nextPointInScreenSpace, Color.RED);
		}
	}

	private void logSampleMatInts(Mat mat) {
		int numPreviewBytes = 1024;
		
		int[] pixelsFromMat = new int[numPreviewBytes];
		mat.get(0, 0, pixelsFromMat);
		
		String pixelSample = "";
		for (int i = 0; i < numPreviewBytes; i++) {
			pixelSample += pixelsFromMat[i] + " ";
		}
		Log.d(TAG, "pixels from mat: " + pixelSample);
	}
	
	private void logSampleMatBytes(Mat mat) {
		int numPreviewBytes = 1024;
		
		byte[] pixelsFromMat = new byte[numPreviewBytes];
		mat.get(0, 0, pixelsFromMat);
		
		String pixelSample = "";
		for (int i = 0; i < numPreviewBytes; i++) {
			pixelSample += pixelsFromMat[i] + " ";
		}
		Log.d(TAG, "pixels from mat: " + pixelSample);
	}
	
	// takes input in screen coords, will convert to NDC
	public void drawLineWithColor(Point p1, Point p2, int color) {
		mLineShaderProgram.drawLineWithColor(p1, p2, color);
	}
	
	public void drawLineWithColorAndThickness(Point p1, Point p2, int color, float thickness) {
		mLineShaderProgram.drawLineWithColorAndThickness(p1, p2, color, thickness);
	}
	
	public void drawPointWithColor(Point p, int color) {
		mQuadShaderProgram.drawPointWithColor(p, color);
	}
	
	public Bitmap createBitmapFromMat(Mat mat) throws CvException{
		if (mPreviewBitmap == null || mPreviewBitmap.getWidth() != mat.width() || mPreviewBitmap.getHeight() != mat.height()) {
			Log.d(TAG, "creating preview bitmap");
			mPreviewBitmap = Bitmap.createBitmap(mat.width(), mat.height(), Bitmap.Config.ARGB_8888);	
		}
		
		Utils.matToBitmap(mat, mPreviewBitmap);
		
		if (Pref.getInstance().isUsingEngineerView()) {
	    	mDelegate.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					//Log.d(TAG, "updating preview image");
					mDelegate.updatePreviewImage(mPreviewBitmap);
				}
			});
	    }
		
		return mPreviewBitmap;
	}

	public void drawTool(ToolAnnotation toolAnnotation) {
		
		boolean isToolSelected = toolAnnotation.isSelected();
		
		this.mTexturedToolShaderProgram.drawTool(toolAnnotation, isToolSelected);
		
		if (Pref.getInstance().isDrawingToolAnchorPoint()) {
			// drawing where anchor point is, for debug purposes
			this.mQuadShaderProgram.drawPointWithColor(toolAnnotation.getPointInScreenSpace(), Color.CYAN);	
		}
	}

	public void setPointMatchingPairsToDraw(
			List<PointMatchingPair> pointMatchingPairsToDraw) {
		mPointMatchingPairsToDraw = pointMatchingPairsToDraw;
	}

	public void onNewFrameBitmapFromSocket(Bitmap bitmap) {
		//Log.d(TAG, "onNewFrameBitmapFromSocket");
		
		swapOpenCVImageBuffers();
		
		if (Pref.getInstance().isSavingAnnotationReferenceFrame()) {
			// in this case, we care about saving the full-resolution image too
			synchronized(getCurrentOpenCVWorkingImageBuffer()) {
				synchronized(getCurrentOpenCVFullResolutionImageBuffer()) {
					Utils.bitmapToMat(bitmap, getCurrentOpenCVFullResolutionImageBuffer());
					
					Imgproc.resize(getCurrentOpenCVFullResolutionImageBuffer(), getCurrentOpenCVWorkingImageBuffer(), new Size(ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor()));
				}
			}
		} else {
			synchronized(getCurrentOpenCVWorkingImageBuffer()) {
				Utils.bitmapToMat(bitmap, getCurrentOpenCVWorkingImageBuffer());
			
				Imgproc.resize(getCurrentOpenCVWorkingImageBuffer(), getCurrentOpenCVWorkingImageBuffer(), new Size(ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor()));
			}	
		}
		
		
	}
	
	private int mCameraWidth = -1;
	private int mCameraHeight = -1;
	
	public void onPreviewFrame(byte[] data, Camera camera) {
		if (mCameraWidth == -1 || mCameraHeight == -1) {
			Camera.Parameters parameters = camera.getParameters();
			mCameraWidth = parameters.getPreviewSize().width;
			mCameraHeight = parameters.getPreviewSize().height;
		}
		

	    if (mYUVMat != null) {
	    	mYUVMat.release();
	    }
	    
	    // http://stackoverflow.com/questions/16471884/opencv-for-android-convert-camera-preview-from-yuv-to-rgb-with-imgproc-cvtcolor
	    mYUVMat = new Mat(mCameraHeight + mCameraHeight/2, mCameraWidth, CvType.CV_8UC1);
	    mYUVMat.put(0, 0, data);
	    
	    swapOpenCVImageBuffers();

	    if (Pref.getInstance().isSavingAnnotationReferenceFrame()) {
	    	// if true, we care about saving the high-res image too
	    	synchronized(getCurrentOpenCVWorkingImageBuffer()) {
	    		synchronized(getCurrentOpenCVFullResolutionImageBuffer()) {
	    			Imgproc.cvtColor(mYUVMat, getCurrentOpenCVFullResolutionImageBuffer(), Imgproc.COLOR_YUV420sp2BGR);
	    			
	    			// need to get the fullres into full screenspace resolution
	    			Imgproc.resize(getCurrentOpenCVFullResolutionImageBuffer(), getCurrentOpenCVFullResolutionImageBuffer(), new Size(ScreenState.getInstance().getScreenWidth(), ScreenState.getInstance().getScreenHeight()));
	    			
				    Imgproc.resize(getCurrentOpenCVFullResolutionImageBuffer(), getCurrentOpenCVWorkingImageBuffer(), new Size(ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor()));
				    
				    
	    		}
			    
		    }
	    } else {
	    	synchronized(getCurrentOpenCVWorkingImageBuffer()) {
			    Imgproc.cvtColor(mYUVMat, getCurrentOpenCVWorkingImageBuffer(), Imgproc.COLOR_YUV420sp2BGR);
			    
			    Imgproc.resize(getCurrentOpenCVWorkingImageBuffer(), getCurrentOpenCVWorkingImageBuffer(), new Size(ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor()));
		    }
	    }   
	    
	    synchronized(getCurrentOpenCVWorkingImageBuffer()) {
		    if (PeerConnection.getInstance().getOwnUserType().isTrainee() && PeerConnection.getInstance().shouldSendFramesToOtherUser()) {
				sendFrameToMentor(getCurrentOpenCVWorkingImageBuffer());
			}
	    }
	    
	    if (Pref.getInstance().isUsingEngineerView()) {
	    	synchronized(getCurrentOpenCVWorkingImageBuffer()) {
	    		
	    		try {
			    	createBitmapFromMat(getCurrentOpenCVWorkingImageBuffer());
				} catch (CvException e) {
					Log.e(TAG, "failed to create bitmap from mat: " + e.getMessage());
					e.printStackTrace();
				}
	    	}
	    }
		    
	    
	}

}
