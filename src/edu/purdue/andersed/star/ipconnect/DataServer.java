package edu.purdue.andersed.star.ipconnect;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.json.JSONException;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.commands.Command;
import edu.purdue.andersed.star.settings.Pref;

public class DataServer {

	// used by the mentor system, receives an incoming connection from a trainee system and supports sending to the trainee client
	
	private static final String TAG = "DataServer";
	
	private DataServerAsyncTask mDataServerAsyncTask;
	
	private Queue<Command> mCommandsToSend;
	
	public DataServer() {
		Log.d(TAG, "constructing DataServer");
		
		mCommandsToSend = new ConcurrentLinkedQueue<Command>();
		
		mDataServerAsyncTask = new DataServerAsyncTask();
	}
	
	public void init() {
		Log.d(TAG, "initializing DataServer");
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// this is needed, or else doInBackground() doesn't get called
			mDataServerAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);	
		} else {
			mDataServerAsyncTask.execute();
		}
		
	}
	
	public void requestSendCommand(Command command) {
		mCommandsToSend.add(command);
	}
	
	public void destroy() {
		Log.d(TAG, "destroying DataServer");
	}
	
	private class DataServerAsyncTask extends AsyncTask<Void, Void, Void> {

		private ServerSocket mServerSocket;
		
		private boolean mRunning;
		
		@Override
	    protected void onPreExecute() {
			mRunning = true;
	    }
		
		@Override
		protected Void doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			
			int port = Pref.getInstance().getDataServerSocketPort();
			Log.d(TAG, "starting server on port " + port);
			try {
				mServerSocket = new ServerSocket(port);
				Log.d(TAG, "started server socket, waiting for connection...");
				
				while (mRunning) {
					Log.d(TAG, "waiting for connection...");
					Socket clientSocket = mServerSocket.accept();
					Log.d(TAG, "got client connection");
					
					OutputStreamWriter out = new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8);
					
					while (clientSocket.isConnected()) {
						
						while(!mCommandsToSend.isEmpty()) {
							
							Command command = mCommandsToSend.remove();
							
							Log.d(TAG, "writing data...");
														
							String stringToWrite = command.toJSON().toString() + "\n";
							
							out.write(stringToWrite);
							out.flush();
							
							STARUtils.saveStringToFile(stringToWrite, "star_sent_command_" + System.currentTimeMillis() + ".log");
							
							Log.d(TAG, "data written");
						}
					}
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
	    protected void onPostExecute(Void result) {
	    }
		
	}
}
