package edu.purdue.andersed.star.annotations;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.features2d.KeyPoint;

import android.util.Log;
import edu.purdue.andersed.star.Constants;
import edu.purdue.andersed.star.FrameKeyPointData;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.Toaster;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class AnnotationMemory {
	// contains the current coordinates of an annotation, as well as info about the image around it when it was first created
	
	// the goal is to have individual homographies for each annotation, where the homography is from t=0 to t=n, instead of t=n-1 to t=n

	private static final String TAG = "AnnotationMemory";
	
	private Annotation mInitialAnnotation;	// the coords of the annotation as it was when first created (or last updated)
	private Annotation mCurrentAnnotation;	// the coords of the annotation currently, after being transformed to frame t=n

	private MatOfKeyPoint mInitialRawKeyPoints = new MatOfKeyPoint();	// keypoints detected before any descriptor matching changes the number of them
	private MatOfKeyPoint mCurrentRawKeyPoints = new MatOfKeyPoint();	// keypoints detected before any descriptor matching changes the number of them
	private MatOfKeyPoint mInitialKeyPoints = new MatOfKeyPoint();	// "relevant" key points from the area around the annotation, from the frame when the annotation was created (or last updated)
	private Mat mInitialDescriptors = new Mat();					// associated descriptors for each relevant key point around the annotation
	
	private Matches mMatches;
	
	private Mat mHomography;
		
	private static final String TAG_INITIAL_ANNOTATION = "initialAnnotation";
	private static final String TAG_INITIAL_RAW_KEY_POINTS = "initialRawKeyPoints";
	private static final String TAG_CURRENT_RAW_KEY_POINTS = "currentRawKeyPoints";
	private static final String TAG_INITIAL_KEY_POINTS = "initialKeyPoints";
	private static final String TAG_INITIAL_DESCRIPTORS = "initialDescriptors";
	private static final String TAG_MATCHES = "matches";
	private static final String TAG_CURRENT_HOMOGRAPHY = "currentHomography";
	
	private Rect mBoundingRect = new Rect();
	
	// only used when getting the current annotation state in toSimpleJSON
	private static final String TAG_CURRENT_ANNOTATION = "currentAnnotation";
	
	private static final double PROPORTION_OF_TOTAL_FEATURES_NEEDED = 0.1;	// if the frame has N keypoints, we want N * this factor to call it a good fit
	
	private boolean mIsValid;
	
	public AnnotationMemory(Annotation initialAnnotation,
			FrameKeyPointData initialFrameKeyPointData, Matches matches) {
		mMatches = matches;
		initializeMemory(initialAnnotation, initialFrameKeyPointData);
	}
	
	public AnnotationMemory(Annotation initialAnnotation,
			FrameKeyPointData initialFrameKeyPointData) {
		initializeMemory(initialAnnotation, initialFrameKeyPointData);
	}

	public void initializeMemory(Annotation initialAnnotation, FrameKeyPointData initialFrameKeyPointData) {
		
		mIsValid = true;
		
		mInitialAnnotation = initialAnnotation;
		
		// setting current annotation to same as initial, initially
		mCurrentAnnotation = mInitialAnnotation;
		
		// initialize the homography to the identity matrix
		mHomography = STARUtils.initHomography();
		
		// if we're tracking, then we need to make sure to bring in the keypoint/descriptor data
		// if not, then we don't need it, just the screen-space positions of the points in the annotation
		
		if (Pref.getInstance().isTrackingEnabled()) {
			MatOfKeyPoint copyRawKeyPoints = new MatOfKeyPoint();
			MatOfKeyPoint copyKeyPoints = new MatOfKeyPoint();
			Mat copyDescriptors = new Mat();

			double annotationRegionPadding = Pref.getInstance().getScaledAnnotationRegionPadding();
			
			MatOfKeyPoint initialRawKeyPoints = initialFrameKeyPointData.getCurrentFrameRawKeyPoints();
			MatOfKeyPoint initialKeyPoints = initialFrameKeyPointData.getCurrentFrameKeyPoints();
			Mat initialDescriptors = initialFrameKeyPointData.getCurrentFrameDescriptors();
			
			synchronized (initialRawKeyPoints) {
				synchronized (initialKeyPoints) {
					synchronized (initialDescriptors) {
					
						
						if (initialKeyPoints == null) {
							mIsValid = false;
							Toaster.getInstance().toast("initial keypoints is null");
						}
						
						if (initialDescriptors == null) {
							mIsValid = false;
							Toaster.getInstance().toast("initial descriptors is null");
						}
						
						if (initialKeyPoints.rows() != initialDescriptors.rows()) {
							mIsValid = false;
							Toaster.getInstance().toast("size mismatch between keypoints and descriptors; " + initialKeyPoints.rows() + " keypoints, " + initialDescriptors.rows() + " descriptors");
						}
						
						// first, copy the keypoints and descriptors so they don't get changed on us during construction
						initialRawKeyPoints.copyTo(copyRawKeyPoints);
						
						Log.d(TAG, "copyRawKeyPoints rows = " + copyRawKeyPoints.rows());
						
						initialKeyPoints.copyTo(copyKeyPoints);
						
						Log.d(TAG, "copyKeyPoints rows = " + copyKeyPoints.rows());
						
						initialDescriptors.copyTo(copyDescriptors);
						
						Log.d(TAG, "copyDescriptors rows = " + copyDescriptors.rows());
					}
				}
			}
			

			
			// TODO: this is where we would decide which of the keypoints/descriptors are really relevant, and only save those
			// also remember the importance of copying these values, not just having an external reference that will be overwritten each frame
			
			MatOfPoint2f annotationPoints2f = initialAnnotation.getPointsMat();
			
			if (annotationPoints2f.rows() > 0) {
				List<Point> annotationPointsList = annotationPoints2f.toList();
				
				double minX = annotationPointsList.get(0).x;
				double minY = annotationPointsList.get(0).y;
				double maxX = annotationPointsList.get(0).x;
				double maxY = annotationPointsList.get(0).y;
				
				for (Point annotationPoint : annotationPointsList) {
					Log.d(TAG, "annotation point: " + annotationPoint);
					double pointX = annotationPoint.x;
					double pointY = annotationPoint.y;
					if (pointX < minX) {
						minX = pointX;
					}
					if (pointX > maxX) {
						maxX = pointX;
					}
					if (pointY < minY) {
						minY = pointY;
					}
					if (pointY > maxY) {
						maxY = pointY;
					}
				}
				
				// set up some padding around the bounding rect		
				
				minX -= annotationRegionPadding;
				maxX += annotationRegionPadding;
				
				minY -= annotationRegionPadding;
				maxY += annotationRegionPadding;
				
				mBoundingRect = new Rect((int)minX, (int)minY, (int)(maxX - minX), (int)(maxY - minY));
				
				Log.d(TAG, "set up bounding rect of " + mBoundingRect.toString());
				
				boolean initFeaturesSuccess = initializeFeaturesWithinBoundingRect(copyRawKeyPoints, copyKeyPoints, copyDescriptors, mBoundingRect);
				while (mIsValid && !initFeaturesSuccess) {
					if (boundingRectIsScreenSize(mBoundingRect)) {
						Toaster.getInstance().toast("unable to create annotation: not enough features");
						mIsValid = false;
					} else {
						Log.d(TAG, "failed to initialize features with bounding rect " + mBoundingRect + ", expanding region");
						mBoundingRect.x = (int) Math.max(mBoundingRect.x - annotationRegionPadding, 0);
						mBoundingRect.y = (int) Math.max(mBoundingRect.y - annotationRegionPadding, 0);
						mBoundingRect.width = (int) Math.min(mBoundingRect.width + annotationRegionPadding, ScreenState.getInstance().getScreenWidth());
						mBoundingRect.height = (int) Math.min(mBoundingRect.height + annotationRegionPadding, ScreenState.getInstance().getScreenHeight());
						
						Log.d(TAG, "trying again with bounding rect expanded to " + mBoundingRect);
						initFeaturesSuccess = initializeFeaturesWithinBoundingRect(copyRawKeyPoints, copyKeyPoints, copyDescriptors, mBoundingRect);	
					}
				}
			} else {
				Log.d(TAG, "annotation has no points yet");
			}
		}
		
	}
	
	public Rect getBoundingRect() {
		return mBoundingRect;
	}
	
	private boolean boundingRectIsScreenSize(Rect boundingRect) {
		return boundingRect.x <= 0 && boundingRect.y <= 0 && boundingRect.width >= ScreenState.getInstance().getScreenWidth() && boundingRect.height >= ScreenState.getInstance().getScreenHeight();
	}

	private boolean initializeFeaturesWithinBoundingRect(MatOfKeyPoint initialRawKeyPoints, MatOfKeyPoint initialKeyPoints, Mat initialDescriptors, Rect boundingRect) {
		
		int numFeaturesNeeded = (int) Math.max(PROPORTION_OF_TOTAL_FEATURES_NEEDED * initialKeyPoints.rows(), 4);
		Log.d(TAG, "num features needed for good fit: " + numFeaturesNeeded);
		
		boolean success = true;
		
		mInitialRawKeyPoints = new MatOfKeyPoint();
		mCurrentRawKeyPoints = new MatOfKeyPoint();
		mInitialKeyPoints = new MatOfKeyPoint();
		mInitialDescriptors = new Mat();
		
		List<KeyPoint> rawKeyPointList = initialRawKeyPoints.toList();
		for (int i = 0; i < rawKeyPointList.size(); i++) {
			KeyPoint rawKeyPoint = rawKeyPointList.get(i);
			Point point = rawKeyPoint.pt;
			if (point.inside(boundingRect)) {
				mInitialRawKeyPoints.push_back(initialRawKeyPoints.row(i));
			}
		}
		
		List<KeyPoint> keyPointList = initialKeyPoints.toList();

		for (int i = 0; i < keyPointList.size(); i++) {
			KeyPoint keyPoint = keyPointList.get(i);
			Point point = keyPoint.pt;
			if (point.inside(boundingRect)) {
				mInitialKeyPoints.push_back(initialKeyPoints.row(i));
				mInitialDescriptors.push_back(initialDescriptors.row(i));
			}
		}
		
		Log.d(TAG, "total frame had " + initialKeyPoints.rows() + " rows, we're tracking using " + mInitialKeyPoints.rows() + " rows");
		
		if (mInitialKeyPoints.rows() < numFeaturesNeeded) {
			Log.d(TAG, "with bounding rect " + boundingRect + ", initial keypoints are insufficient: " + mInitialKeyPoints.rows());
			success = false;
		}
		
		if (mInitialDescriptors.rows() < numFeaturesNeeded) {
			Log.d(TAG, "with bounding rect " + boundingRect + ", initial descriptors are insufficient: " + mInitialDescriptors.rows());
			success = false;
		}
		
		return success;
	}

	public Annotation getInitialAnnotation() {
		return mInitialAnnotation;
	}

	public Annotation getCurrentAnnotation() {
		return mCurrentAnnotation;
	}

	public void updateCurrentAnnotationBasedOnHomography() {
		if (mHomography != null && mHomography.rows() > 0) {
			mCurrentAnnotation = mInitialAnnotation.getTransformedAnnotationBasedOnHomography(mHomography);
		} else {
			Log.e(TAG, "homography for this annotation isn't set");
		}
		
	}

	public void setHomography(Mat homography) {
		//Log.d(TAG, "setting homography to " + homography);
		mHomography = homography;
	}

	public MatOfKeyPoint getInitialKeyPoints() {
		return mInitialKeyPoints;
	}

	public Mat getInitialDescriptors() {
		return mInitialDescriptors;
	}

	public JSONObject toSimpleJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject annotationMemoryObject = new JSONObject();
		
		annotationMemoryObject.put(TAG_CURRENT_ANNOTATION, mCurrentAnnotation.toJSON());
		
		//Log.d(TAG, "result: " + annotationMemoryObject.toString());
		return annotationMemoryObject;
	}
	
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject annotationMemoryObject = new JSONObject();
		
		annotationMemoryObject.put(TAG_INITIAL_ANNOTATION, mInitialAnnotation.toJSON());
		annotationMemoryObject.put(TAG_CURRENT_ANNOTATION, mCurrentAnnotation.toJSON());
		annotationMemoryObject.put(TAG_INITIAL_RAW_KEY_POINTS, Protocol.matOfKeyPointToJSONArray(mInitialRawKeyPoints));
		annotationMemoryObject.put(TAG_CURRENT_RAW_KEY_POINTS, Protocol.matOfKeyPointToJSONArray(mCurrentRawKeyPoints));
		annotationMemoryObject.put(TAG_INITIAL_KEY_POINTS, Protocol.matOfKeyPointToJSONArray(mInitialKeyPoints));
		annotationMemoryObject.put(TAG_INITIAL_DESCRIPTORS, Protocol.matToJSONObject(mInitialDescriptors));
		annotationMemoryObject.put(TAG_MATCHES, Protocol.matchesToJSONObject(mMatches));
		if (mHomography != null) {
			annotationMemoryObject.put(TAG_CURRENT_HOMOGRAPHY, Protocol.matToJSONObject(mHomography));	
		}
		
		//Log.d(TAG, "result: " + annotationMemoryObject.toString());
		return annotationMemoryObject;
	}

	public static AnnotationMemory fromJSON(JSONObject annotationMemoryObject) throws JSONException {
		
		Annotation initialAnnotation = getAnnotationFromJSON(annotationMemoryObject.getJSONObject(TAG_INITIAL_ANNOTATION));
		
		MatOfKeyPoint mkp = null;
		if (annotationMemoryObject.has(TAG_INITIAL_RAW_KEY_POINTS)) {
			try {
				mkp = Protocol.jsonArrayToMatOfKeyPoint(annotationMemoryObject.getJSONArray(TAG_INITIAL_RAW_KEY_POINTS));
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		MatOfKeyPoint initialRawKeyPoints = (mkp != null) ? mkp : new MatOfKeyPoint();
		
		if (annotationMemoryObject.has(TAG_CURRENT_RAW_KEY_POINTS)) {
			try {
				mkp = Protocol.jsonArrayToMatOfKeyPoint(annotationMemoryObject.getJSONArray(TAG_CURRENT_RAW_KEY_POINTS));
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		MatOfKeyPoint currentRawKeyPoints = (mkp != null) ? mkp : new MatOfKeyPoint();
		
		if (annotationMemoryObject.has(TAG_INITIAL_KEY_POINTS)) {
			try {
				mkp = Protocol.jsonArrayToMatOfKeyPoint(annotationMemoryObject.getJSONArray(TAG_INITIAL_KEY_POINTS));
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		MatOfKeyPoint initialKeyPoints = (mkp != null) ? mkp : new MatOfKeyPoint();
		
		Mat m = null;
		if (annotationMemoryObject.has(TAG_INITIAL_DESCRIPTORS)) {
			try {
				m = Protocol.jsonObjectToMat(annotationMemoryObject.getJSONObject(TAG_INITIAL_DESCRIPTORS));
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		Mat initialDescriptors = (m != null) ? m : new Mat();
		
		Matches mtch = null;
		if (annotationMemoryObject.has(TAG_MATCHES)) {
			try {
				mtch = Protocol.jsonObjectToMatches(annotationMemoryObject.getJSONObject(TAG_MATCHES));
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		Matches matches = (mtch != null) ? mtch : new Matches();
		
		
		
		
		Mat homography = STARUtils.initHomography();
		if (annotationMemoryObject.has(TAG_CURRENT_HOMOGRAPHY)) {
			
			try {
				m = Protocol.jsonObjectToMat(annotationMemoryObject.getJSONObject(TAG_CURRENT_HOMOGRAPHY));	
			} catch (JSONException e) {
				Log.e(TAG, e.getMessage());
			}
			
			if (m != null) {
				homography = m;
			}
		}
		
		Annotation currentAnnotation = null;
		if (annotationMemoryObject.has(TAG_CURRENT_ANNOTATION)) {
			currentAnnotation = getAnnotationFromJSON(annotationMemoryObject.getJSONObject(TAG_CURRENT_ANNOTATION));	
		}
		
		AnnotationMemory annotationMemory = new AnnotationMemory(initialAnnotation, new FrameKeyPointData(initialRawKeyPoints, initialKeyPoints, initialDescriptors), matches);
		annotationMemory.setHomography(homography);
		annotationMemory.setCurrentAnnotation(currentAnnotation);
		annotationMemory.setCurrentRawKeyPoints(currentRawKeyPoints);
		
		return annotationMemory;
	}


	private void setCurrentAnnotation(Annotation currentAnnotation) {
		mCurrentAnnotation = currentAnnotation;
	}

	private static Annotation getAnnotationFromJSON(JSONObject annotationObject) throws JSONException {
		Log.d(TAG, "fromJSON()");
		String annotationType = annotationObject.getString(Annotation.TAG_ANNOTATION_TYPE);
		
		if (Annotation.TYPE_POINT.equals(annotationType)) {
			return PointAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_POLYLINE.equals(annotationType)) {
			return PolylineAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_POLYGON.equals(annotationType)) {
			return PolygonAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_TOOL.equals(annotationType)) {
			return ToolAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_MULTI_POINT.equals(annotationType)) {
			return MultiPointAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_ANIMATED_INCISION.equals(annotationType)) {
			return AnimatedIncisionAnnotation.fromJSON(annotationObject);
		} else {
			throw new IllegalStateException("unknown annotation type: " + annotationType);
		}
	}
	
	public Mat getCurrentHomography() {
		return mHomography;
	}
	
	public boolean isValid() {
		return mIsValid;
	}

	public MatOfKeyPoint getInitialRawKeyPoints() {
		return mInitialRawKeyPoints;
	}
	
	public MatOfKeyPoint getCurrentRawKeyPoints() {
		return mCurrentRawKeyPoints;
	}

	
	public void setCurrentRawKeyPoints(MatOfKeyPoint currentRawKeyPoints) {
		mCurrentRawKeyPoints = currentRawKeyPoints;
	}
	
	public Matches getMatches() {
		return mMatches;
	}

	public void setMatches(Matches matches) {
		this.mMatches = matches;
	}
	
	

}
