package edu.purdue.andersed.star.shaders;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.opengl.GLES30;
import android.opengl.GLUtils;
import android.util.Log;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.geometry.CameraPreviewQuad;

public class Texture2DVideo implements CameraViewBackground {
	
	private static final String TAG = "Texture2DVideo";
	
	private int mTextureID;
	
	private CameraPreviewQuad mGeometry;

	private int mShaderProgramID;

	private int mVertexHandle;
	private int mTextureCoordHandle;
	private int mTexSampler2DHandle;

	private Bitmap mFrameBitmap;
	private boolean mNeedToUpdateVideoTexture;
	
	private static final String PASS_THROUGH_VERTEX_SHADER =
		"attribute vec4 vertexPosition; \n" +
		"attribute vec2 vertexTexCoord; \n" +
		"varying vec2 texCoord; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_Position = vertexPosition; \n" +
		"    texCoord = vertexTexCoord; \n" +
		"} \n"
			;
		
	private static final String DIRECT_VIDEO_FRAGMENT_SHADER = 
		"precision mediump float; \n" + 
		"varying vec2 texCoord; \n" +
		"uniform sampler2D texSampler; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_FragColor = texture2D(texSampler, texCoord).bgra; \n" +
//		"    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n" +
		"} \n"
			;
	
	public Texture2DVideo(int textureID) {
		Log.d(TAG, "setting up Texture2DVideo with texture ID = " + textureID);

		mTextureID = textureID;
		
		Log.d(TAG, "creating geometry for camera preview quad");
		mGeometry = new CameraPreviewQuad();
		
		Log.d(TAG, "creating shader program");
		mShaderProgramID = STARUtils.createProgramFromShaderSrc(PASS_THROUGH_VERTEX_SHADER, DIRECT_VIDEO_FRAGMENT_SHADER);
		
		Log.d(TAG, "setting up handles");
		mVertexHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexPosition");
		STARUtils.checkGLError("glGetAttribLocation");
		mTextureCoordHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexTexCoord");
		STARUtils.checkGLError("glGetAttribLocation");
		mTexSampler2DHandle = GLES30.glGetAttribLocation(mShaderProgramID, "texSampler2D");
		STARUtils.checkGLError("glGetAttribLocation");
		
		
		
		//....
		
		/*
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, mTextureID);
		STARUtils.checkGLError("glBindTexture");
		
		Log.d(TAG, "loading from local bitmap to test");
		Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher);
		GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, bitmap, 0);
		STARUtils.checkGLError("texImage2D");
		bitmap.recycle();
		*/
	}

	@Override
	public void draw(GL10 gl) {

		if (mNeedToUpdateVideoTexture) {
			updateVideoTextureToBitmap(gl);
			mNeedToUpdateVideoTexture = false;
		}
		
		GLES30.glUseProgram(mShaderProgramID);
		STARUtils.checkGLError("glUseProgram");
		GLES30.glVertexAttribPointer(mVertexHandle, 3, GLES30.GL_FLOAT, false, 0, mGeometry.getVertices());
		STARUtils.checkGLError("glVertexAttribPointer");
		GLES30.glVertexAttribPointer(mTextureCoordHandle, 2, GLES30.GL_FLOAT, false, 0, mGeometry.getTexCoords());
		STARUtils.checkGLError("glVertexAttribPointer");
		GLES30.glEnableVertexAttribArray(mVertexHandle);
		STARUtils.checkGLError("glEnableVertexAttribArray");
		GLES30.glEnableVertexAttribArray(mTextureCoordHandle);
		STARUtils.checkGLError("glEnableVertexAttribArray");
		GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
		STARUtils.checkGLError("glActiveTexture");
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,
        		mTextureID);
        STARUtils.checkGLError("glBindTexture");
        GLES30.glUniform1i(mTexSampler2DHandle, 0);
        STARUtils.checkGLError("glUniform1i");
        GLES30.glDrawElements(GLES30.GL_TRIANGLES,
        		mGeometry.getNumObjectIndex(), GLES30.GL_UNSIGNED_SHORT,
        		mGeometry.getIndices());
        STARUtils.checkGLError("glDrawElements");
		GLES30.glDisableVertexAttribArray(mVertexHandle);
		STARUtils.checkGLError("glDisableVertexAttribArray");
		GLES30.glDisableVertexAttribArray(mTextureCoordHandle);
		STARUtils.checkGLError("glDisableVertexAttribArray");
		
	}
	
	private synchronized void updateVideoTextureToBitmap(GL10 gl) {
		//Log.d(TAG, "updateVideoTextureToBitmap");
		
		if (mFrameBitmap != null) {
			GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, mTextureID);
			STARUtils.checkGLError("glBindTexture");
			
			GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, mFrameBitmap, 0);
			STARUtils.checkGLError("texImage2D");
			
			//mFrameBitmap.recycle();
		} else {
			Log.e(TAG, "cannot update video texture -- frame bitmap is null");
		}
	}
	
	public synchronized void setFrameBitmap(Bitmap frameBitmap) {
		//Log.d(TAG, "setFrameBitmap");
		mFrameBitmap = frameBitmap;
		mNeedToUpdateVideoTexture = true;
	}
}
