LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE    := nonfree_prebuilt
LOCAL_SRC_FILES := libnonfree.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := opencv_java_prebuilt
LOCAL_SRC_FILES := libopencv_java.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= C:\Development\Android\OpenCV-2.4.9-android-sdk\sdk\native\jni\include
LOCAL_MODULE    := nonfree_jni
LOCAL_CFLAGS    := -Werror -O3 -ffast-math
LOCAL_LDLIBS    += -llog -ldl 
LOCAL_SHARED_LIBRARIES := nonfree_prebuilt opencv_java_prebuilt
LOCAL_SRC_FILES := nonfree_jni.cpp
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= C:\Development\Android\OpenCV-2.4.9-android-sdk\sdk\native\jni\include
LOCAL_MODULE    := point_overlay_jni
LOCAL_CFLAGS    := -Werror -O3 -ffast-math
LOCAL_LDLIBS    += -llog -ldl -lGLESv3 -lEGL -ljnigraphics -lz -landroid
LOCAL_SHARED_LIBRARIES := nonfree_prebuilt opencv_java_prebuilt
LOCAL_SRC_FILES := point_overlay_jni.cpp
include $(BUILD_SHARED_LIBRARY)




include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= C:\Development\Android\OpenCV-2.4.9-android-sdk\sdk\native\jni\include
LOCAL_MODULE    := encoder
LOCAL_CFLAGS    := -Werror -O3 -ffast-math -DHAVE_PTHREADS
LOCAL_LDLIBS    += -llog -ljnigraphics -ldl -lGLESv3 -lEGL  -lz -landroid 
LOCAL_SHARED_LIBRARIES := nonfree_prebuilt opencv_java_prebuilt libavformat libavcodec libswscale libavutil libavfilter libswresample
LOCAL_SRC_FILES := encoder.cpp
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_C_INCLUDES:= C:\Development\Android\OpenCV-2.4.9-android-sdk\sdk\native\jni\include
LOCAL_MODULE    := decoder
LOCAL_CFLAGS    := -Werror -O3 -ffast-math -DHAVE_PTHREADS
LOCAL_LDLIBS    += -llog -ljnigraphics -ldl -lGLESv3 -lEGL  -lz -landroid 
LOCAL_SHARED_LIBRARIES := nonfree_prebuilt opencv_java_prebuilt libavformat libavcodec libswscale libavutil libavfilter libswresample
LOCAL_SRC_FILES := decoder.cpp
include $(BUILD_SHARED_LIBRARY)
$(call import-module,ffmpeg-2.8.6/android/arm)