package edu.purdue.andersed.star;

import org.opencv.core.Point;

public class PointMatchingPair {

	private Point mP1;
	private Point mP2;
	private boolean mIsInlier;
	
	public PointMatchingPair(Point p1, Point p2, boolean isInlier) {
		mP1 = p1;
		mP2 = p2;
		mIsInlier = isInlier;
	}

	public Point getP1() {
		return mP1;
	}

	public Point getP2() {
		return mP2;
	}

	public boolean isInlier() {
		return mIsInlier;
	}
	
	
}
