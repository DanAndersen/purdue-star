package edu.purdue.andersed.star.frameprocessors;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.KeyPoint;
import org.opencv.imgproc.Imgproc;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import edu.purdue.andersed.star.FrameProcessorPerformanceLogging;
import edu.purdue.andersed.star.FrameTracing;
import edu.purdue.andersed.star.FrameTracingTag;
import edu.purdue.andersed.star.ImageWithTimestamp;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PointMatchingPair;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.annotations.Matches;
import edu.purdue.andersed.star.annotations.PolylineAnnotation;
import edu.purdue.andersed.star.settings.Pref;

public class StandardFrameProcessor extends FrameProcessor {
	
	private static final String TAG = "StandardFrameProcessor";
	
	private ProcessFrameAsyncTask mProcessFrameAsyncTask;

	// ==== feature detection ====
	private FeatureDetector mFeatureDetector;

	// ==== descriptor extraction ====
	private DescriptorExtractor mDescriptorExtractor;

	// ==== descriptor matching ====
	private DescriptorMatcher mDescriptorMatcher;

	private Mat mOutlierMask;
	
	private static final int NUM_POINTS_NEEDED_FOR_HOMOGRAPHY = 4;
	
	private List<SaveFrameAsFileAsyncTask> mSaveFrameAsyncTasks;
	
	private FrameTracing mFrameTracing;
	
	private MatOfKeyPoint mCurrentFrameRawKeyPoints;
	private MatOfKeyPoint mCurrentFrameKeyPoints;
	private Mat mCurrentFrameDescriptors;
	
	private Mat mCopiedWorkingImage;
	private Mat mCopiedWorkingImageToSave;
	private Mat mCopiedFullResolutionImage;
	
	private MatOfDMatch mMatches;
	
	private MatOfPoint2f mPrevPts;
	private MatOfPoint2f mNextPts;
	
	public StandardFrameProcessor(MainActivity delegate, PointOverlay pointOverlay) {
		super(delegate, pointOverlay);
	}
	
	@Override
	protected void initFrameProcessor() {
		Log.d(TAG, "initFrameProcessor");
		
		mSaveFrameAsyncTasks = new ArrayList<SaveFrameAsFileAsyncTask>();
		
		Log.d(TAG, "creating feature detector");
		mFeatureDetector = FeatureDetector.create(FeatureDetector.ORB);
		//mFeatureDetector = FeatureDetector.create(FeatureDetector.FAST);
		
		Log.d(TAG, "creating descriptor extractor");
		mDescriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
		//mDescriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.BRISK);
				
		Log.d(TAG, "creating descriptor matcher");
		//mDescriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);

		mDescriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
		
		mOutlierMask = new Mat();
		
		mCurrentFrameRawKeyPoints = new MatOfKeyPoint();
		mCurrentFrameKeyPoints = new MatOfKeyPoint();
		mCurrentFrameDescriptors = new Mat();
		
		mCopiedWorkingImage = new Mat();
		mCopiedWorkingImageToSave = new Mat();
		mCopiedFullResolutionImage = new Mat();
		
		mMatches = new MatOfDMatch();
		
		mPrevPts = new MatOfPoint2f();
		mNextPts = new MatOfPoint2f();
		
		resetFrameProcessor();
	}
	
	@Override
	protected void resetFrameProcessor() {
		Log.d(TAG, "resetFrameProcessor");
		
		mProcessFrameAsyncTask = new ProcessFrameAsyncTask();
				
		mFrameTracing = new FrameTracing();
		
		setReadyToProcessFrame(true);
	}
	
	public void processFrameIfReady(Mat workingImage, Mat fullResolutionImage) {
		//Log.d(TAG, "processFrameIfReady");
		
		if (isReadyToProcessFrame()) {
			mFrameTracing.init();
			
			fullResolutionImage.copyTo(mCopiedFullResolutionImage);
			workingImage.copyTo(mCopiedWorkingImage);
			
			long frameTimestamp = System.currentTimeMillis();
			
			ImageWithTimestamp imageWithTimestamp = new ImageWithTimestamp(mCopiedWorkingImage, mCopiedFullResolutionImage, frameTimestamp);
			
			setReadyToProcessFrame(false);
			
			mProcessFrameAsyncTask = new ProcessFrameAsyncTask();
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				// this is needed, or else doInBackground() doesn't get called
				mProcessFrameAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, imageWithTimestamp);	
			} else {
				mProcessFrameAsyncTask.execute(imageWithTimestamp);
			}
			
		}
		
	}
	
	private class SaveFrameAsFileAsyncTask extends AsyncTask<ImageWithTimestamp, Void, Void> {

		private static final String TAG = "SaveFrameAsFileAsyncTask";
		
		@Override
		protected Void doInBackground(ImageWithTimestamp... images) {
			
			Mat image = images[0].getWorkingImage();
			long timestamp = images[0].getTimestamp();
			
			STARUtils.saveMatToFile(image, "star_frame_" + timestamp + ".png");
			
			return null;
		}
		
	}
	
	private class ProcessFrameAsyncTask extends AsyncTask<ImageWithTimestamp, Void, Void> {

		private static final String TAG = "ProcessFrameAsyncTask";
		
		private ImageWithTimestamp mImageWithTimestamp;
		
		@Override
		protected Void doInBackground(ImageWithTimestamp... images) {
			//Log.d(TAG, "==================== doInBackground =====================");
			mFrameTracing.start(FrameTracingTag.total);
			
			int count = images.length;
			if (count > 0) {
				mImageWithTimestamp = images[0];
				
				Mat workingImage = mImageWithTimestamp.getWorkingImage();
				
				Imgproc.cvtColor(workingImage, workingImage, Imgproc.COLOR_BGR2GRAY);
				
				// code to do some blurring -- may help with tracking
				/*
				Mat workingImageCopy = new Mat();
				Imgproc.cvtColor(workingImage, workingImageCopy, Imgproc.COLOR_BGR2GRAY);
				Imgproc.bilateralFilter(workingImageCopy, workingImage, 9, 75, 75);
				*/
				
				//mPointOverlayDelegate.createBitmapFromMat(workingImage);
				
				
				findFeaturesForMat(workingImage);
				
				matchAndUpdateAnnotations(AnnotationState.getInstance().getAnnotations(), mCurrentFrameKeyPoints, mCurrentFrameDescriptors);
			
				// updating frame data in annotation state
				
				MatOfKeyPoint rawKeyPointsCopy = new MatOfKeyPoint();
				mCurrentFrameRawKeyPoints.copyTo(rawKeyPointsCopy);
				
				MatOfKeyPoint keyPointsCopy = new MatOfKeyPoint();
				mCurrentFrameKeyPoints.copyTo(keyPointsCopy);
				
				Mat descriptorsCopy = new Mat();
				mCurrentFrameDescriptors.copyTo(descriptorsCopy);
				
				AnnotationState.getInstance().updateCurrentFullResolutionFrame(mImageWithTimestamp.getFullResolutionImage());
				
				AnnotationState.getInstance().updateCurrentFrame(workingImage);
				AnnotationState.getInstance().updateFrameData(rawKeyPointsCopy, keyPointsCopy, descriptorsCopy);
				// updated frame data in annotation state
				
				if (Pref.getInstance().isSavingFrameToBeProcessed()) {
					drawKeypointsOnImageToBeSaved(mImageWithTimestamp.getWorkingImage());
					drawAnnotationsOnImageToBeSaved(mImageWithTimestamp.getWorkingImage());
					
					SaveFrameAsFileAsyncTask saveFrameTask = new SaveFrameAsFileAsyncTask();
					mSaveFrameAsyncTasks.add(saveFrameTask);
					
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						// this is needed, or else doInBackground() doesn't get called
						saveFrameTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mImageWithTimestamp);	
					} else {
						saveFrameTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mImageWithTimestamp);
					}
				}
			}
			
			mFrameTracing.end(FrameTracingTag.total);
			
			FrameProcessorPerformanceLogging.getInstance().writeLogForFrame(mFrameTracing);
			
			return null;
		}
		
		private void findFeaturesForMat(Mat image) {
			
			
			mFrameTracing.start(FrameTracingTag.feature_detection);
			mFeatureDetector.detect(image, mCurrentFrameKeyPoints);
			mFrameTracing.end(FrameTracingTag.feature_detection);
			
			mCurrentFrameKeyPoints.copyTo(mCurrentFrameRawKeyPoints);
			
			mFrameTracing.log(FrameTracingTag.num_features_after_feature_detection, mCurrentFrameKeyPoints.rows());
			
			mFrameTracing.start(FrameTracingTag.descriptor_extraction);
			mDescriptorExtractor.compute(image,mCurrentFrameKeyPoints, mCurrentFrameDescriptors);
			mFrameTracing.end(FrameTracingTag.descriptor_extraction);
			
			mFrameTracing.log(FrameTracingTag.num_features_after_descriptor_extraction, mCurrentFrameKeyPoints.rows());
			
			//Log.d(TAG, "NUM KEYPOINTS: " + mCurrentFrameKeyPoints.rows());
						
			mMainActivityDelegate.onNumKeyPointsUpdated(mCurrentFrameKeyPoints.rows());
			
			if (Pref.getInstance().isDrawingKeyPoints()) {
				List<Point> keyPointsToDraw = new ArrayList<Point>();
				List<KeyPoint> currentKeypointList = mCurrentFrameKeyPoints.toList();
				
				for (KeyPoint keypoint : currentKeypointList) {					
					Point pointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(keypoint.pt);
					
					keyPointsToDraw.add(pointInScreenSpace);					
				}
				mPointOverlayDelegate.setKeyPointsToDraw(keyPointsToDraw);
			}
			
			
		}
		
		private void drawAnnotationsOnImageToBeSaved(Mat workingImage) {
			Map<Integer, AnnotationMemory> annotationMemoryMap = AnnotationState.getInstance().getAnnotations();
			
			Iterator<Integer> iter = annotationMemoryMap.keySet().iterator();
			
			while (iter.hasNext()) {
				Integer id = iter.next();
				AnnotationMemory annotationMemory = annotationMemoryMap.get(id);
				
				if (annotationMemory != null && annotationMemory.isValid()) {
					
					if (annotationMemory.getCurrentHomography() != null) {
						drawAnnotationOnImageToBeSaved(annotationMemory.getCurrentAnnotation(), workingImage);	
					}
				}
			}
		}
		
		private void drawKeypointsOnImageToBeSaved(Mat workingImage) {
			List<KeyPoint> currentKeypointList = mCurrentFrameKeyPoints.toList();
			
			for (KeyPoint keypoint : currentKeypointList) {					
				Point keypointInOpenCVSpace = keypoint.pt;
				
				Core.circle(workingImage, keypointInOpenCVSpace, 2, new Scalar(0, 255, 0, 255));
			}
		}

		private void drawAnnotationOnImageToBeSaved(
				Annotation annotation, Mat workingImage) {
			if (annotation instanceof PolylineAnnotation) {
				PolylineAnnotation polylineAnnotation = (PolylineAnnotation) annotation;
				List<Point> pointsInOpenCVSpace = polylineAnnotation.getPointsMat().toList();
				for (int i = 0; i < pointsInOpenCVSpace.size() - 1; i++) {
					Point p = pointsInOpenCVSpace.get(i);
					Point nextP = pointsInOpenCVSpace.get(i+1);
					Core.line(workingImage, p, nextP, new Scalar(0, 0, 255), 2);
				}
			} else {
				Log.e(TAG, "other types of annotations not supported here yet");
			}
		}

		private void matchAndUpdateAnnotations(Map<Integer, AnnotationMemory> annotationMemoryMap, MatOfKeyPoint currentFrameKeyPoints, Mat currentFrameDescriptors) {
			
			//Log.d(TAG, "updating and drawing annotations");

			Iterator<Integer> iter = annotationMemoryMap.keySet().iterator();
			
			while (iter.hasNext()) {
				Integer id = iter.next();
				AnnotationMemory annotationMemory = annotationMemoryMap.get(id);
				
				if (annotationMemory != null && annotationMemory.isValid()) {
					
					doMatching(annotationMemory, currentFrameKeyPoints, currentFrameDescriptors);
					
					annotationMemory.updateCurrentAnnotationBasedOnHomography();
				} else {
					Log.d(TAG, "this annotation memory is invalid; remove it");
					annotationMemoryMap.remove(id);
				}
			}
			
		}
		
		private void doMatching(AnnotationMemory annotationMemory, MatOfKeyPoint currentFrameKeyPoints, Mat currentFrameDescriptors) {
			
			Mat currentHomography = annotationMemory.getCurrentHomography();
			
			MatOfKeyPoint copyRawKeyPoints = new MatOfKeyPoint();
			mCurrentFrameRawKeyPoints.copyTo(copyRawKeyPoints);
			annotationMemory.setCurrentRawKeyPoints(copyRawKeyPoints);
			
			MatOfKeyPoint annotationInitialKeyPoints = annotationMemory.getInitialKeyPoints();
			Mat annotationInitialDescriptors = annotationMemory.getInitialDescriptors();
			
			if (annotationInitialKeyPoints == null || annotationInitialKeyPoints.rows() == 0) {
				Log.e(TAG, "annotation doesn't have any key points!");
			}
			
			if (annotationInitialDescriptors == null || annotationInitialDescriptors.rows() == 0) {
				Log.e(TAG, "annotation doesn't have any descriptors!");
			}
			
			
			if (currentFrameKeyPoints.rows() > 0) {
				Mat homography = findHomographyViaDescriptorMatching(annotationMemory, annotationInitialKeyPoints, currentFrameKeyPoints, annotationInitialDescriptors, currentFrameDescriptors);
				
				
				if (Pref.getInstance().isUsingGoodHomographyCheck()) {
					// checking if good homography...
					if (!isGoodHomography(homography)) {
						// not a good homography
												
						// homography = null; // use this if we want to make the homography disappear
						homography = currentHomography;	// use this if we want to just have the previously good homography
					}
					// checked if good homography
				}
				
				annotationMemory.setHomography(homography);
			} else {
				Log.d(TAG, "there isn't both a preview and current frame yet, wait until next frame to match");
			}
		}

		private Mat findHomographyViaDescriptorMatching(AnnotationMemory annotationMemory, MatOfKeyPoint objectKeyPoints, MatOfKeyPoint sceneKeyPoints, Mat objectDescriptors, Mat sceneDescriptors) {
						
			mFrameTracing.start(FrameTracingTag.descriptor_matching);
			mDescriptorMatcher.match(objectDescriptors, sceneDescriptors, mMatches);
			mFrameTracing.end(FrameTracingTag.descriptor_matching);
			
			int numMatches = mMatches.rows();
			mFrameTracing.log(FrameTracingTag.num_matches, numMatches);
					
			mFrameTracing.start(FrameTracingTag.prepping_list_of_matches);
			DMatch[] matchesArray = mMatches.toArray();
			KeyPoint[] objectKeyPointsArray = objectKeyPoints.toArray();
			KeyPoint[] sceneKeyPointsArray = sceneKeyPoints.toArray();
			
			List<Point> prevPointsList = new ArrayList<Point>();
			List<Point> nextPointsList = new ArrayList<Point>();
			List<Float> matchDistancesList = new ArrayList<Float>();
			
			for (int i = 0; i < numMatches; i++) {
				if (matchesArray[i].queryIdx < objectKeyPointsArray.length && matchesArray[i].trainIdx < sceneKeyPointsArray.length) {
					matchDistancesList.add(matchesArray[i].distance);
					Point prevPoint = objectKeyPointsArray[matchesArray[i].queryIdx].pt;
					Point nextPoint = sceneKeyPointsArray[matchesArray[i].trainIdx].pt;
					prevPointsList.add(prevPoint);
					nextPointsList.add(nextPoint);
				}
			}
			
			// setting matches in annotation memory
			Matches matches = new Matches(prevPointsList, nextPointsList, matchDistancesList);
			annotationMemory.setMatches(matches);
			
			mPrevPts.fromList(prevPointsList);
			mNextPts.fromList(nextPointsList);
			mFrameTracing.end(FrameTracingTag.prepping_list_of_matches);
				
			Mat homography = findHomography(mPrevPts, mNextPts);
			
			return homography;
		}
		
		private Mat findHomography(MatOfPoint2f prevPts, MatOfPoint2f nextPts) {
			
			//Log.d(TAG, "starting findHomography");
			Mat homography = null;
			
			if (prevPts.rows() >= NUM_POINTS_NEEDED_FOR_HOMOGRAPHY) {
				//Log.d(TAG, "finding homography...");
				//double ransacReprojThreshold = 3.0;
				
				double ransacReprojThreshold = 3.0 * Pref.getInstance().getCvImageScaleFactor();
				
				mFrameTracing.start(FrameTracingTag.find_homography);
				homography = Calib3d.findHomography(prevPts, nextPts, Calib3d.RANSAC, ransacReprojThreshold, mOutlierMask);
				mFrameTracing.end(FrameTracingTag.find_homography);
			}
			
			int numInliers = 0;
			int numOutliers = 0;
			if (mOutlierMask != null) {
				for (int i = 0; i < prevPts.rows(); i++) {
					double[] outlierMaskEntry = mOutlierMask.get(i, 0);
					if (outlierMaskEntry != null && outlierMaskEntry.length > 0) {
						int inlierStatus = (int) outlierMaskEntry[0];
						if (inlierStatus == 1) {
							numInliers++;
						} else {
							numOutliers++;
						}
					}
				}
			}
			
			mFrameTracing.log(FrameTracingTag.num_inliers, numInliers);
			mFrameTracing.log(FrameTracingTag.num_outliers, numOutliers);
			
			if (Pref.getInstance().isDrawingPointMatching()) {
				List<Point> prevPtsList = prevPts.toList();
				List<Point> nextPtsList = nextPts.toList();
				
				List<PointMatchingPair> pointMatchingPairsToDraw = new ArrayList<PointMatchingPair>();
				
				for (int i = 0; i < prevPtsList.size(); i++) {
					Point prevPt = prevPtsList.get(i);
					Point nextPt = nextPtsList.get(i);
								
					Point prevPointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(prevPt);
					Point nextPointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(nextPt);
					
					boolean isInlier = false;
					if (mOutlierMask != null) {
						double[] outlierMaskEntry = mOutlierMask.get(i, 0);
						if (outlierMaskEntry != null && outlierMaskEntry.length > 0) {
							int inlierStatus = (int) outlierMaskEntry[0];
							isInlier = (inlierStatus == 1);
						}
					}
					
					pointMatchingPairsToDraw.add(new PointMatchingPair(prevPointInScreenSpace, nextPointInScreenSpace, isInlier));						
				}
				mPointOverlayDelegate.setPointMatchingPairsToDraw(pointMatchingPairsToDraw);
			}
			
			return homography;
		}
		
		private boolean isGoodHomography(Mat homography) {
			if (homography == null) {
				return false;
			}
			
			// https://github.com/MasteringOpenCV/code/issues/11
			double det = homography.get(0, 0)[0] * homography.get(1, 1)[0] - homography.get(1, 0)[0] * homography.get(0, 1)[0];
		    if (det < 0) {
		        return false;
		    }

		    
		    double N1 = Math.sqrt(homography.get(0, 0)[0] * homography.get(0, 0)[0] + homography.get(1, 0)[0] * homography.get(1, 0)[0]);
		    if (N1 > 4 || N1 < 0.1) {
		        return false;
		    }

		    double N2 = Math.sqrt(homography.get(0, 1)[0] * homography.get(0, 1)[0] + homography.get(1, 1)[0] * homography.get(1, 1)[0]);
		    if (N2 > 4 || N2 < 0.1) {
		        return false;
		    }

		    double N3 = Math.sqrt(homography.get(2, 0)[0] * homography.get(2, 0)[0] + homography.get(2, 1)[0] * homography.get(2, 1)[0]);
		    if (N3 > 0.002) {
		        return false;
		    }

		    return true;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			//Log.d(TAG, "onPostExecute");
						
			setReadyToProcessFrame(true);
		}
		
		@Override
		protected void onCancelled() {
			Log.d(TAG, "onCancelled");
			
			setReadyToProcessFrame(true);
		}

	}
	
}