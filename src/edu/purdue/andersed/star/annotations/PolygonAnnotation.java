package edu.purdue.andersed.star.annotations;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import android.util.Log;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class PolygonAnnotation extends Annotation {

	private static final String TAG = "PolygonAnnotation";
	
	private MatOfPoint2f mPointMat = new MatOfPoint2f();
	
	// Takes a String in the form:
	// 638 364
	// 748 388
	// 847 403
	// etc
	public PolygonAnnotation(String stringOfPoints) {
		//Log.d(TAG, "creating new polygon annotation from string \"" + stringOfPoints + "\"");
		
		String lines[] = stringOfPoints.split("\\r?\\n");
		
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			String coords[] = line.split("\\s+");
			
			int xCoord = Integer.parseInt(coords[0]);
			int yCoord = Integer.parseInt(coords[1]);
			
			addPointWithScreenCoordinates(new Point(xCoord, yCoord));
		}
	}
	
	public void addPointWithScreenCoordinates(Point pointInScreenSpace) {
		Point pointInOpenCVSpace = ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace);
		mPointMat.push_back(new MatOfPoint2f(pointInOpenCVSpace));
	}
	
	public PolygonAnnotation(MatOfPoint2f pointMat) {
		Log.d(TAG, "creating PolygonAnnotation from pointmat");
		mPointMat = pointMat;
	}

	public PolygonAnnotation() {
		Log.d(TAG, "creating empty PolygonAnnotation");
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_POLYGON;
	}
	
	public List<Point> getPointsInScreenSpace() {
		List<Point> pointsInOpenCVSpace = mPointMat.toList();
		
		List<Point> pointsInScreenSpace = new ArrayList<Point>();
		for (Point pointInOpenCVSpace : pointsInOpenCVSpace) {
			Point pointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(pointInOpenCVSpace);
			pointsInScreenSpace.add(pointInScreenSpace);
		}
		
		return pointsInScreenSpace;
	}
	
	@Override
	String getGeometryString() {
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		return pointsInScreenSpace.toString();
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		for (int i = 0; i < pointsInScreenSpace.size(); i++) {
			pointOverlay.drawPointWithColor(pointsInScreenSpace.get(i), color);
			
			if (i < pointsInScreenSpace.size() - 1) {
				pointOverlay.drawLineWithColorAndThickness(pointsInScreenSpace.get(i), pointsInScreenSpace.get(i+1), color, thickness);
			} else {
				pointOverlay.drawLineWithColorAndThickness(pointsInScreenSpace.get(i), pointsInScreenSpace.get(0), color, thickness);
			}
		}
	}
	
	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		drawWithColorAndThickness(pointOverlay, color, Pref.getInstance().getDefaultLineThickness());
	}
	
	@Override
	public JSONObject toJSON() throws JSONException {
		
		JSONObject polygonAnnotationObject = new JSONObject();
		
		polygonAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		polygonAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(getPointsMat()));
		
		return polygonAnnotationObject;
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		PolygonAnnotation polygonAnnotation = new PolygonAnnotation(pointMat);
		
		return polygonAnnotation;
	}

	@Override
	public MatOfPoint2f getPointsMat() {
		return mPointMat;
	}

	@Override
	public Annotation getTransformedAnnotationBasedOnHomography(Mat homography) {
		MatOfPoint2f transformedPointMat = new MatOfPoint2f();
		
		// transform is only valid for its own scale (in opencv coords not screen coords) -- scaling the point while working with it
		Core.perspectiveTransform(mPointMat, transformedPointMat, homography);	
		
		return new PolygonAnnotation(transformedPointMat);
	}

	
	
	public void setPointsInScreenSpace(List<Point> pointsInScreenSpace) {
		List<Point> pointsInOpenCVSpace = new ArrayList<Point>();
		for (Point pointInScreenSpace : pointsInScreenSpace) {
			pointsInOpenCVSpace.add(ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace));
		}
		
		mPointMat.fromList(pointsInOpenCVSpace);
	}

	@Override
	public void translate(Point point) {
		List<Point> pointsInScreenSpace = this.getPointsInScreenSpace();
		for (Point p : pointsInScreenSpace) {
			p.x += point.x;
			p.y += point.y;
		}
		this.setPointsInScreenSpace(pointsInScreenSpace);
	}
}
