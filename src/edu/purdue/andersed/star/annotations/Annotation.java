package edu.purdue.andersed.star.annotations;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import edu.purdue.andersed.star.PointOverlay;

public abstract class Annotation {

	private static final String TAG = "Annotation";
	
	public abstract void drawWithColor(PointOverlay pointOverlay, int color);

	abstract String getAnnotationType();
	
	abstract String getGeometryString();
	
	public abstract MatOfPoint2f getPointsMat();
	
	public static final String TAG_ANNOTATION_TYPE = "annotationType";
	public static final String TAG_ANNOTATION_POINTS = "annotationPoints";
	public static final String TAG_ANNOTATION_TOOL_TYPE = "toolType";
	public static final String TAG_ANNOTATION_ROTATION = "rotation";
	public static final String TAG_ANNOTATION_SCALE = "scale";
	public static final String TAG_ANNOTATION_SELECTABLE_COLOR = "selectableColor";
	
	public static final String TYPE_POINT = "point";
	public static final String TYPE_MULTI_POINT = "multi_point";
	public static final String TYPE_POLYLINE = "polyline";
	public static final String TYPE_POLYGON = "polygon";
	public static final String TYPE_TOOL = "tool";
	public static final String TYPE_ANIMATED_INCISION = "animated_incision";
	
	@Override
	public String toString() {
		return "{ Annotation, type = " + getAnnotationType() + ", geometry = " + getGeometryString() + " }";
	}

	public abstract Annotation getTransformedAnnotationBasedOnHomography(Mat homography);


	public abstract JSONObject toJSON() throws JSONException;

	public abstract void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness);

	public abstract void translate(Point point);
}
