package edu.purdue.andersed.star;

import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;

import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;

public class SaveAnnotationReferenceFrameAsyncTask extends SaveAnnotatedFrameToFileAsyncTask {
	
	@Override
	void doDrawing(MatOfKeyPoint currentFrameKeyPoints, AnnotationMemory annotationMemory, Mat imageOriginal) {

		Mat image = new Mat();
		imageOriginal.copyTo(image);
		
		Annotation initialAnnotation = annotationMemory.getInitialAnnotation();
		
		drawAnnotation(initialAnnotation, image, ANNOTATION_POINT_COLOR);
		
		saveImage(image, "reference_annotation");
		
		drawBoundingRect(annotationMemory, image);
				
		drawRawFeatures(annotationMemory.getInitialRawKeyPoints(), image, REFERENCE_FEATURE_CROSS_LINE_COLOR);
		
		saveImage(image, "features");
		
		drawDescriptors(annotationMemory.getInitialKeyPoints(), image, REFERENCE_DESCRIPTOR_RECT_COLOR);
		
		saveImage(image, "descriptors");
				
	}
}
