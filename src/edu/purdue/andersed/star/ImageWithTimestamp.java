package edu.purdue.andersed.star;

import org.opencv.core.Mat;

public class ImageWithTimestamp {

	private Mat mWorkingImage;
	private Mat mFullResolutionImage;
	
	private long mTimestamp;
	
	public Mat getWorkingImage() {
		return mWorkingImage;
	}

	public Mat getFullResolutionImage() {
		return mFullResolutionImage;
	}
	
	public long getTimestamp() {
		return mTimestamp;
	}

	public ImageWithTimestamp(Mat workingImage, Mat fullResolutionImage, long timestamp) {
		super();
		this.mWorkingImage = workingImage;
		this.mFullResolutionImage = fullResolutionImage;
		this.mTimestamp = timestamp;
	}
	
	
}
