package edu.purdue.andersed.streaming;

public interface EncodeListener {

	public void onFrameEncoded(byte[] encodedBytes);
}
