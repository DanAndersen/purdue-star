package edu.purdue.andersed.star.videosource;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
import android.view.Surface;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.recordings.RecordedVideoFile;
import edu.purdue.andersed.star.settings.Pref;

public class FileVideoSource extends VideoSource implements OnCompletionListener {

	private static final String TAG = "FileVideoSource";
	
	private MainActivity mDelegate;
	
	private MediaPlayer mMediaPlayer;
	private Resources mResources;
	
	private RecordedVideoFile mRecordedVideoFile;
			
	private AssetFileDescriptor mAfd;
	
	public FileVideoSource(MainActivity delegate) {
		mDelegate = delegate;
		
		mRecordedVideoFile = Pref.getInstance().getRecordedVideoFile();
	}
	
	@Override
	public void initAndStartVideoSource(SurfaceTexture surfaceTexture) {
		Log.d(TAG, "initAndStartVideoSource");
		mMediaPlayer = new MediaPlayer();
		mResources = mDelegate.getResources();
		
		Log.d(TAG, "opening resource");
		mAfd = mResources.openRawResourceFd(mRecordedVideoFile.getRawVideoId());
		
		try {
			Log.d(TAG, "setting data source");
			mMediaPlayer.setDataSource(mAfd.getFileDescriptor(), mAfd.getStartOffset(), mAfd.getLength());
			RecordedVideoErrorLogging.getInstance().resetLogging(mRecordedVideoFile);
			
			Surface surface = new Surface(surfaceTexture);
			mMediaPlayer.setSurface(surface);
			mMediaPlayer.setScreenOnWhilePlaying(true);
			mMediaPlayer.setOnCompletionListener(this);
			surface.release();

			mMediaPlayer.prepare();
			
			//mMediaPlayer.setLooping(true);
			
			mMediaPlayer.start();
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void stopVideoSource() {
		Log.d(TAG, "stopVideoSource");
		
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();	
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		//Log.d(TAG, "file video has completed playing, print out error log and reset error log");

		//RecordedVideoErrorLogging.getInstance().printErrorLog();
		
		//RecordedVideoErrorLogging.getInstance().resetLogging(mRecordedVideoFile);
		
		mMediaPlayer.start();
	}

	public void logErrorForFrame(double error) {
		RecordedVideoErrorLogging.getInstance().addLogForFrame(mMediaPlayer.getCurrentPosition() * 30 / 1000, error);
	}
}
