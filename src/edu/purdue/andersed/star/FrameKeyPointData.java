package edu.purdue.andersed.star;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;

public class FrameKeyPointData {

	private MatOfKeyPoint mCurrentFrameRawKeyPoints;
	private MatOfKeyPoint mCurrentFrameKeyPoints;
	private Mat mCurrentFrameDescriptors;
	
	public FrameKeyPointData(MatOfKeyPoint rawKeyPoints, MatOfKeyPoint keyPoints, Mat descriptors) {
		mCurrentFrameRawKeyPoints = rawKeyPoints;
		mCurrentFrameKeyPoints = keyPoints;
		mCurrentFrameDescriptors = descriptors;
	}

	public MatOfKeyPoint getCurrentFrameKeyPoints() {
		return mCurrentFrameKeyPoints;
	}

	public Mat getCurrentFrameDescriptors() {
		return mCurrentFrameDescriptors;
	}

	public MatOfKeyPoint getCurrentFrameRawKeyPoints() {
		return mCurrentFrameRawKeyPoints;
	}
	
	
	
}
