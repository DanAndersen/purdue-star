package edu.purdue.andersed.star.ipconnect;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.R;
import edu.purdue.andersed.star.UserType;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.settings.SettingsActivity;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class STARConnectActivity extends Activity {

	private static final String TAG = "STARConnectActivity";

	private Button mStartAsMentorButton;
	private Button mStartAsTraineeButton;
	private TextView mStatusTextView;
	private EditText mMentorIPAddressEditText;
	
	
	
	private String mLocalIP;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Pref.getInstance().initialize(this);
		Log.d(TAG, "setting up default values for prefs");
		PreferenceManager.setDefaultValues(this, R.xml.preferences, true);
		
		setContentView(R.layout.connect);

		mStatusTextView = (TextView) findViewById(R.id.status_textview);
		mMentorIPAddressEditText = (EditText) findViewById(R.id.mentor_ip_address_edittext);
		
		mLocalIP = getLocalIP();
		
		
		
		
		
		

		
		
		mStartAsMentorButton = (Button) findViewById(R.id.start_as_mentor_button);
		mStartAsMentorButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "pressed start as mentor button");

				appendToStatusText("starting as mentor...");
				
				PeerConnection.getInstance().setMentorIP(mLocalIP);
				
				connectAsMentor();
			}
		});

		mStartAsTraineeButton = (Button) findViewById(R.id.start_as_trainee_button);
		mStartAsTraineeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "pressed start as trainee button");

				mStatusTextView.setText("starting as trainee...");
				
				String mentorIPAddress = mMentorIPAddressEditText.getText().toString();
				
				try {
					InetAddress mentorInetAddress = InetAddress.getByName(mentorIPAddress);
					
					PeerConnection.getInstance().setMentorIP(mentorInetAddress.getHostAddress());
					
					connectAsTrainee();
					
				} catch (UnknownHostException e) {
					appendToStatusText("error on mentor IP address: " + e.getMessage());
				}
			}
		});	
		
		initInstructions();
	}
	
	
	private void connectAsMentor() {
		Log.d(TAG, "connectAsMentor()");
		
		connectAs(UserType.MENTOR);
	}
	
	private void connectAsTrainee() {
		Log.d(TAG, "connectAsTrainee()");
		
		connectAs(UserType.TRAINEE);
	}
	
	private void connectAs(UserType userType) {
		Log.d(TAG, "connectAs()");
		
		PeerConnection.getInstance().setOwnUserType(userType);
		
		Log.d(TAG, "todo: start ar");
		startAR();
		appendToStatusText("todo: start ar");
	}
	
	private void startAR() {
		Log.d(TAG, "startAR()");
		
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
	
	private void initInstructions() {
		appendToStatusText("Trainee should connect to mentor using mentor's local IP address.");
		appendToStatusText("Local IP address of this device:");
		
		appendToStatusText(mLocalIP);
		
		
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ip_connect_menu, menu);
        return true;
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.atn_star_settings:
            	Log.d(TAG, "pressed STAR settings button");
            	
            	Intent intent = new Intent(this, SettingsActivity.class);
        		startActivity(intent);
        		return true;
            case R.id.atn_star_reset_prefs:
            	Log.d(TAG, "pressed reset settings buttons");
            	
            	AlertDialog.Builder builder = new AlertDialog.Builder(this);
            	builder.setMessage(R.string.dialog_reset_defaults).setPositiveButton(R.string.reset_prefs_confirm, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d(TAG, "resetting prefs");
						
						PreferenceManager.getDefaultSharedPreferences(STARConnectActivity.this).edit().clear().commit();
		            	PreferenceManager.setDefaultValues(STARConnectActivity.this, R.xml.preferences, true);
					}
				}).setNegativeButton(R.string.reset_prefs_cancel, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// do nothing
					}
				});
            	builder.show();
            	
            	
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	
	
	
	
	
	
	

	@Override
	public void onResume() {
		super.onResume();

		
	}

	private void appendToStatusText(String string) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(mStatusTextView.getText());
		sb.append("\n");
		sb.append(string);

		mStatusTextView.setText(sb.toString());
	}
	
	private String getLocalIP() {
		String localIP = null;
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces.nextElement();
				appendToStatusText("network interface display name: " + networkInterface.getDisplayName());
				Enumeration<InetAddress> enumInetAddress = networkInterface.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress.nextElement();
					appendToStatusText("inetAddress: " + inetAddress.getHostAddress());
					
					if (inetAddress.isSiteLocalAddress()) {
						localIP = inetAddress.getHostAddress();
						appendToStatusText("got a local ip: " + localIP);
					}

				}

			}

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			appendToStatusText("Something Wrong! " + e.toString());
		}
		return localIP;
	}
	
	private void logIpAddress() {
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces.nextElement();
				Enumeration<InetAddress> enumInetAddress = networkInterface.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress.nextElement();

					if (inetAddress.isSiteLocalAddress()) {
						appendToStatusText("SiteLocalAddress: " + inetAddress.getHostAddress());
					}

				}

			}

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			appendToStatusText("Something Wrong! " + e.toString());
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		
	}
}
