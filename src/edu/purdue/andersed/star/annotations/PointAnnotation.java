package edu.purdue.andersed.star.annotations;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class PointAnnotation extends Annotation {

	private static final String TAG = "PointAnnotation";
	
	protected MatOfPoint2f mPointMat = new MatOfPoint2f();
		
	public PointAnnotation(MatOfPoint2f pointMat) {
		mPointMat = pointMat;
	}
	
	public PointAnnotation(Point pointInScreenSpace) {
		setPointInScreenSpace(pointInScreenSpace);
	}
	
	public void setPointInScreenSpace(Point pointInScreenSpace) {
		Point pointInOpenCVSpace = ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace);
		
		mPointMat.fromArray(pointInOpenCVSpace);
	}
	
	public PointAnnotation(double screenX, double screenY) {
		this(new Point(screenX, screenY));
	}

	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		pointOverlay.drawPointWithColor(getPointInScreenSpace(), color);
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		drawWithColor(pointOverlay, color);
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_POINT;
	}

	@Override
	String getGeometryString() {
		Point point = getPointInScreenSpace();
		if (point != null) {
			return point.toString();	
		} else {
			return "";
		}
		
	}
	
	public Point getPointInScreenSpace() {
		Point pointInOpenCVSpace = mPointMat.toArray()[0];
		return ScreenState.getInstance().openCVSpaceToScreenSpace(pointInOpenCVSpace);
	}

	@Override
	public Annotation getTransformedAnnotationBasedOnHomography(Mat homography) {

		MatOfPoint2f transformedPointMat = new MatOfPoint2f();
		
		// transform is only valid for its own scale (in opencv coords not screen coords) -- scaling the point while working with it
		Core.perspectiveTransform(mPointMat, transformedPointMat, homography);	
		
		return new PointAnnotation(transformedPointMat);
	}

	@Override
	public MatOfPoint2f getPointsMat() {
		return mPointMat;
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject pointAnnotationObject = new JSONObject();
		
		pointAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		pointAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(getPointsMat()));
		
		//Log.d(TAG, "result: " + pointAnnotationObject.toString());
		return pointAnnotationObject;
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		PointAnnotation pointAnnotation = new PointAnnotation(pointMat);
		
		return pointAnnotation;
	}

	@Override
	public void translate(Point point) {
		Point p = this.getPointInScreenSpace();
		p.x += point.x;
		p.y += point.y;
		this.setPointInScreenSpace(p);
	}

}
