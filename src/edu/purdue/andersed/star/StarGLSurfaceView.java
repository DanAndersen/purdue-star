package edu.purdue.andersed.star;

import android.opengl.GLSurfaceView;
import android.util.Log;

public class StarGLSurfaceView extends GLSurfaceView {

	private static final String TAG = "StarGLSurfaceView";
	
	private STARRenderer mRenderer;
	
	public StarGLSurfaceView(MainActivity mainActivity) {
		super(mainActivity);
		
		Log.d(TAG, "creating OpenGL ES context");
		setEGLContextClientVersion(2);
		
		setEGLConfigChooser(false);
		
		Log.d(TAG, "creating renderer");
		mRenderer = new STARRenderer(mainActivity);
		setRenderer(mRenderer);
		//setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}

	public STARRenderer getRenderer() {
		return mRenderer;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		mRenderer = null;
	}
	
}
