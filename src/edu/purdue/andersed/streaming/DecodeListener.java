package edu.purdue.andersed.streaming;

import org.opencv.core.Mat;

public interface DecodeListener {

	public void onReceivedDecodedFrame(Mat decodedRGBFrame);
}
