package edu.purdue.andersed.star;


public class Constants {
				
	public static final int BYTES_PER_PIXEL = 4;
	
	public static final int ICON_ROTATION_TEXT = 0;
	public static final int ICON_ROTATION_HAND = 0;
	public static final int ICON_ROTATION_TOOL = 45;
}
