package edu.purdue.andersed.star.commands;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateAnnotationCommand extends Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1007975793339983569L;

	public static final String TAG = "CreateAnnotationCommand";
	
	public static final String KEY_ID = "id";
	public static final String KEY_ANNOTATION_MEMORY = "annotation_memory";
	
	private Integer mId;
	private String mAnnotationMemoryJSONObjectString;
	
	public CreateAnnotationCommand(Integer id, JSONObject annotationMemoryJSONObject) {
		mId = id;
		mAnnotationMemoryJSONObjectString = annotationMemoryJSONObject.toString();
	}
	
	@Override
	public JSONObject toJSON() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put(KEY_COMMAND, TAG);
		obj.put(KEY_ID, mId);
		obj.put(KEY_ANNOTATION_MEMORY, new JSONObject(mAnnotationMemoryJSONObjectString));
		return obj;
	}
	
	public Integer getId() {
		return mId;
	}
	
	public JSONObject getAnnotationMemoryJSONObject() {
		try {
			return new JSONObject(mAnnotationMemoryJSONObjectString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static CreateAnnotationCommand fromJSON(JSONObject commandJsonObject) throws JSONException {
		
		Integer id = commandJsonObject.getInt(KEY_ID);
		JSONObject annotationMemoryObject = commandJsonObject.getJSONObject(KEY_ANNOTATION_MEMORY);
		
		return new CreateAnnotationCommand(id, annotationMemoryObject);
	}

}
