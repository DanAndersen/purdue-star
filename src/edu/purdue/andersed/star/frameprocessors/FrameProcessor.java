package edu.purdue.andersed.star.frameprocessors;

import org.opencv.core.Mat;

import android.util.Log;
import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PointOverlay;

public abstract class FrameProcessor {
	
	private static final String TAG = "FrameProcessor";
	
	private boolean mIsReadyToProcessFrame;

	protected MainActivity mMainActivityDelegate;
	protected PointOverlay mPointOverlayDelegate;
	
	public FrameProcessor(MainActivity delegate, PointOverlay pointOverlay) {
		Log.d(TAG, "initializing FrameProcessor");
		mMainActivityDelegate = delegate;
		mPointOverlayDelegate = pointOverlay;
		
		initFrameProcessor();
		
		resetFrameProcessor();
	}
	
	protected abstract void initFrameProcessor();
	
	protected abstract void resetFrameProcessor();
	
	protected void setReadyToProcessFrame(boolean isReady) {
		mIsReadyToProcessFrame = isReady;
	}
	
	public boolean isReadyToProcessFrame() {
		return mIsReadyToProcessFrame;
	}
	
	public abstract void processFrameIfReady(Mat workingImage, Mat fullResolutionImage);
	
}


