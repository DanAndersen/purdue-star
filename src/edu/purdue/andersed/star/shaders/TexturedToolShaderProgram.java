package edu.purdue.andersed.star.shaders;

import java.util.EnumMap;

import org.opencv.core.Point;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES30;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.annotations.ToolAnnotation;
import edu.purdue.andersed.star.annotations.ToolTemplate;
import edu.purdue.andersed.star.annotations.ToolType;
import edu.purdue.andersed.star.geometry.Quad;
import edu.purdue.andersed.star.settings.Pref;

public class TexturedToolShaderProgram {

	private static final String TAG = "TexturedToolShaderProgram";
	
	private static final String VERTEX_SHADER =
		"attribute vec4 vertexPosition; \n" +
		"uniform mat4 uMVPMatrix; \n" +
		"attribute vec2 vertexTexCoord; \n" +
		"varying vec2 texCoord; \n" +
		"void main() \n" +
		"{ \n" +
		"    gl_Position = uMVPMatrix * vertexPosition; \n" +

		"    texCoord = vertexTexCoord; \n" +
		"} \n"
			;
	
	private static final String FRAGMENT_SHADER = 
			"precision mediump float; \n" + 
			"varying vec2 texCoord; \n" +
			"uniform sampler2D texSampler; \n" +
			"uniform float toolTransparency; \n" + 
			"void main() \n" +
			"{ \n" +
			"    vec4 textureColor = texture2D(texSampler, texCoord);" +
			"    gl_FragColor = vec4(textureColor.rgb, textureColor.a - toolTransparency); \n" +
			"} \n"
				;
	
	private int mShaderProgramID;

	private int mVertexHandle;
	
	private int mMVPMatrixHandle;
	
	private int mTextureCoordHandle;
	private int mTexSampler2DHandle;
	private int mToolTransparencyHandle;
	
	private EnumMap<ToolType, Integer> mDefaultTextureIDs = new EnumMap<ToolType, Integer>(ToolType.class);
	private EnumMap<ToolType, Integer> mSelectedTextureIDs = new EnumMap<ToolType, Integer>(ToolType.class);
	private EnumMap<ToolType, Quad> mGeometries = new EnumMap<ToolType, Quad>(ToolType.class);
	
	private float MAX_TOOL_SCALE = 1.0f;	// percentage of screen that should be taken up by geometry
	
	public TexturedToolShaderProgram(Context context) {	
		setupShaderProgram(context);
	}

	private void setupShaderProgram(Context context) {
		Log.d(TAG, "creating shader program");
		
		Log.d(TAG, "creating geometry");
		
		mShaderProgramID = STARUtils.createProgramFromShaderSrc(VERTEX_SHADER, FRAGMENT_SHADER);
		STARUtils.checkGLError("createProgramFromShaderSrc");
		
		Log.d(TAG, "setting up handles");
		mVertexHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexPosition");
		STARUtils.checkGLError("glGetAttribLocation");
		
		mMVPMatrixHandle = GLES30.glGetUniformLocation(mShaderProgramID, "uMVPMatrix");
		STARUtils.checkGLError("glGetUniformLocation");
		mToolTransparencyHandle = GLES30.glGetUniformLocation(mShaderProgramID, "toolTransparency");
		STARUtils.checkGLError("glGetUniformLocation");
						
		mTextureCoordHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexTexCoord");
		STARUtils.checkGLError("glGetAttribLocation");
		mTexSampler2DHandle = GLES30.glGetAttribLocation(mShaderProgramID, "texSampler2D");
		STARUtils.checkGLError("glGetAttribLocation");
		
		Log.d(TAG, "setting up the geometries and textures for each tool type");
		for (ToolType tt : ToolType.values()) {
			setupTextureAndGeometryForToolTemplate(context, tt);
		}
	}
	
	private void setupTextureAndGeometryForToolTemplate(Context context, ToolType toolType) {
		ToolTemplate toolTemplate = toolType.getToolTemplate();
		
		final int[] textureHandle = new int[2];
		 
	    GLES30.glGenTextures(2, textureHandle, 0);
	 
	    if (textureHandle[0] != 0)
	    {
	        final BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inScaled = false;   // No pre-scaling
	 
	        // Read in the resource
	        final Bitmap defaultBitmap = BitmapFactory.decodeResource(context.getResources(), toolTemplate.getDefaultDrawableResourceId(), options);
	        final Bitmap selectedBitmap = BitmapFactory.decodeResource(context.getResources(), toolTemplate.getSelectedDrawableResourceId(), options);
	 
	        
	        Log.d(TAG, "setting up geometry");
	        
	        float width = defaultBitmap.getWidth();
	        float height = defaultBitmap.getHeight();
	        
	        Quad quad = new Quad(MAX_TOOL_SCALE, width, height, toolTemplate.getAnchorPixelsFromCenterX(), toolTemplate.getAnchorPixelsFromCenterY());
	        
	        mGeometries.put(toolType, quad);
	        
	        // Bind to the texture in OpenGL
	        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, textureHandle[0]);
	 
	        // Set filtering
	        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_NEAREST);
	        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_NEAREST);
	 
	        // Load the bitmap into the bound texture.
	        GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, defaultBitmap, 0);
	 
	        // Recycle the bitmap, since its data has been loaded into OpenGL.
	        defaultBitmap.recycle();
	        
	        // Bind to the texture in OpenGL
	        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, textureHandle[1]);
	 
	        // Set filtering
	        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_NEAREST);
	        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_NEAREST);
	 
	        // Load the bitmap into the bound texture.
	        GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, selectedBitmap, 0);
	 
	        // Recycle the bitmap, since its data has been loaded into OpenGL.
	        selectedBitmap.recycle();
	    }
	 
	    if (textureHandle[0] == 0)
	    {
	        throw new RuntimeException("Error loading texture.");
	    }
	 
	    if (textureHandle[1] == 0)
	    {
	        throw new RuntimeException("Error loading texture.");
	    }
	    
	    mDefaultTextureIDs.put(toolType, textureHandle[0]);
	    mSelectedTextureIDs.put(toolType, textureHandle[1]);
	}
	
	public void drawTool(ToolAnnotation toolAnnotation, boolean isSelected) {
		
		ToolType toolType = toolAnnotation.getToolType();
		
		Point anchorPoint = toolAnnotation.getPointInScreenSpace();
		
		int textureID = isSelected ? mSelectedTextureIDs.get(toolType) : mDefaultTextureIDs.get(toolType); 
		
		Quad geometry = mGeometries.get(toolType);
		
		GLES30.glUseProgram(mShaderProgramID);
		STARUtils.checkGLError("glUseProgram");
				
		GLES30.glVertexAttribPointer(mVertexHandle, 3, GLES30.GL_FLOAT, false, 0, geometry.getVertices());
		STARUtils.checkGLError("glVertexAttribPointer");
		GLES30.glVertexAttribPointer(mTextureCoordHandle, 2, GLES30.GL_FLOAT, false, 0, geometry.getTexCoords());
		STARUtils.checkGLError("glVertexAttribPointer");
		GLES30.glEnableVertexAttribArray(mVertexHandle);
		STARUtils.checkGLError("glEnableVertexAttribArray");
		GLES30.glEnableVertexAttribArray(mTextureCoordHandle);
		STARUtils.checkGLError("glEnableVertexAttribArray");
		GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
		STARUtils.checkGLError("glActiveTexture");
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,
        		textureID);
        STARUtils.checkGLError("glBindTexture");
        GLES30.glUniform1i(mTexSampler2DHandle, 0);
        STARUtils.checkGLError("glUniform1i");
        GLES30.glUniform1f(mToolTransparencyHandle, Pref.getInstance().getToolTransparency());
        
        float[] mvpMatrix = new float[16];
        
        double degrees = toolAnnotation.getDegrees();
        double scale = toolAnnotation.getScale();
        
        //Log.d(TAG, "degrees: " + degrees);
        //Log.d(TAG, "scale: " + scale);
        
        Matrix.setIdentityM(mvpMatrix, 0);
        
        Matrix.translateM(mvpMatrix, 0, 
        		(float)(2 * (anchorPoint.x - ScreenState.getInstance().getHalfScreenWidth()) / (float) ScreenState.getInstance().getScreenWidth()), 
        		(float)(-2 * (anchorPoint.y - ScreenState.getInstance().getHalfScreenHeight()) / (float) ScreenState.getInstance().getScreenHeight()), 
        		0.0f);
        
        Matrix.scaleM(mvpMatrix, 0, 1.0f, ScreenState.getInstance().getAspectRatio(), 1.0f);

        Matrix.scaleM(mvpMatrix, 0, (float) scale, (float) scale, 1.0f);
        
        Matrix.rotateM(mvpMatrix, 0, (float) degrees, 0.0f, 0.0f, 1.0f);
                
        Matrix.translateM(mvpMatrix, 0, geometry.getScaledOffsetFromCenterX(), geometry.getScaledOffsetFromCenterY(), 0.0f);

        GLES30.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        STARUtils.checkGLError("glUniformMatrix4fv");        
        
        GLES30.glDrawElements(GLES30.GL_TRIANGLES,
        		geometry.getNumObjectIndex(), GLES30.GL_UNSIGNED_SHORT,
        		geometry.getIndices());
        STARUtils.checkGLError("glDrawElements");
		GLES30.glDisableVertexAttribArray(mVertexHandle);
		STARUtils.checkGLError("glDisableVertexAttribArray");
		GLES30.glDisableVertexAttribArray(mTextureCoordHandle);
		STARUtils.checkGLError("glDisableVertexAttribArray");
		
	}
}
